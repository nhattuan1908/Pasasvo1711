//
//  Queue.swift
//  SensorsLog
//
//  Created by Unitec on 12/7/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation

/*
 First-in first-out queue (FIFO)
 New elements are added to the end of the queue. Dequeuing pulls elements from
 the front of the queue.
 Enqueuing and dequeuing are O(1) operations.
 */
public class Queue<T> {
    
    
    fileprivate var array = [T?]()
    fileprivate var head = 0
    
    //private let blocksDispatchQueue = DispatchQueue.global()
    
    
    public var isEmpty: Bool {
        return count == 0
    }
    
    public var count: Int {
        return array.count - head
    }
    

    public func enqueue(_ element: T) {
        if (array.count >= 1000){
            return
        }
        
        
        
        //blocksDispatchQueue.async {
            self.array.append(element)
        //}

        
        
    }
    
    public func dequeue() -> T? {
        
        guard head < array.count, let element = array[head] else { return nil }

        array[head] = nil
        head += 1
        
        let percentage = Double(head)/Double(array.count)
        if array.count > 50 && percentage > 0.25 {
            
            self.array.removeFirst(self.head)
            
            head = 0
        }

        return element

    }
    
    public var front: T? {
        if isEmpty {
            return nil
        } else {
            return array[head]
        }
    }
}
