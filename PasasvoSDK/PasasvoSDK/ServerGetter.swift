//
//  ServerGetter.swift
//  SensorsLog
//
//  Created by Unitec on 12/10/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
open class ServerBase {
    static var dataProperties = DataProperties()
    
    public static func initParam(){
        if PasasvoConfig.drivingId == "" {
            // Load phat chac an
            PasasvoConfig.load()
        }
        
        
        dataProperties.carId = PasasvoConfig.carId
        dataProperties.deviceId = PasasvoConfig.deviceId
        dataProperties.userId = PasasvoConfig.userId
        dataProperties.drivingId = PasasvoConfig.drivingId
    }
    
}

open class ServerGetter : ServerBase{
    
    // If sending, do nothing
    private static var sending: Bool = false
    private static var sendingWaitCount = 0
    
    static var hashmapDangerous = [String]()
    
    static public func getSafeDriving(delay: Int = 15, completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?, _ result: SafeDriving?) -> (Void)) -> Bool
    {
        
        initParam()
        var l = 0.1
        if delay > 0 {
            l = Double(delay)
        }
        
        Timer.after(TimeInterval(l)){
            DataManageAPI.safeDrivingGet(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingId: PasasvoConfig.drivingId) { (data, error) in
                if(error != nil)
                {
                    completeHanlder((error?.0)!, error.debugDescription, nil)
                }
                else
                {
                    if let d = data?.first?.safeDriving?.first {
                        completeHanlder(200, nil, d)
                    }else{
                        completeHanlder(400, "Server return different format data.", nil)
                    }
                    
                }
            }
        }
        

        return true
    }
    
    
    static public func getDangerousDriving(completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?, _ result: DangerousDriving?) -> (Void)) -> Bool
    {
        
        initParam()
        
        if sending{
            sendingWaitCount += 1
            if (sendingWaitCount >= 10){
                sendingWaitCount = 0
                sending = false
            }
            return false
        }
        
        sending = true
        
        DataManageAPI.dangerousDrivingGet(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingId: PasasvoConfig.drivingId, completion: { (data, error) in
            
            sending = false
            
            if(error == nil)
            {
                if (data != nil){
                    for d in data! {
                        if d.dangerousDriving != nil {
                            for e in d.dangerousDriving!{
                                if hashmapDangerous.contains(e.measurementDatetime!) == false{
                                    hashmapDangerous.append(e.measurementDatetime!)
                                    completeHanlder(200, nil, e)
                                }
                            }
                        }
                        
                    }
                    
                }
                
            }
            else
            {
                completeHanlder((error?.0)!, error.debugDescription, nil)
            }
        })
        
        return true
    }
    
    static public func getHistoriesSummary(completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?, _ result: [DrivingDataGetReturn]?) -> (Void)) -> Void
    {
        initParam()
        
        DataManageAPI.drivingDataGet(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, completion: { (data, error) in
            if(error != nil)
            {
                completeHanlder((error?.0)!, error.debugDescription, nil)
            }
            else
            {
                if let d = data?.last?.drivingData {
                    
                    completeHanlder(200, nil, data)
                }
                else
                {
                    completeHanlder(400, "Server return different format data.", nil)
                }
            }
        })
    }
    
    static public func getHistory(drivingId: String?, completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?, _ result: [DrivingDataGetReturn]?) -> (Void)) -> Void
    {
        
        initParam()
            DataManageAPI.drivingDataDetailGet(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingId: drivingId!, completion: { (data, error) in
                if(error != nil)
                {
                    completeHanlder((error?.0)!, error.debugDescription, nil)
                }
                else
                {
                    if let d = data?.first?.drivingData {
                        completeHanlder(200, nil, data)
                    }else{
                    completeHanlder(400, "Server return different format data.", nil)
                    }
                }
            })
    }
}
