//
//  PoolDatabase.swift
//  SensorsLog
//
//  Created by Unitec on 12/12/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation

import GRDB

var db = PoolDatabase.getInstance()

class PoolDatabase {
    var dbQueue: DatabasePool
    var lastIdSensor: Int = 0
    var lastIdOBD: Int = 0
    var startIdSensor: Int = 0
    var startIdObd: Int = 0
    
    static public func getInstance()->PoolDatabase?{
        do{
            let ret = try PoolDatabase()
            return ret
        }catch{
            print(error)
            return nil
        }
        
    }
    
    public func getSensorLastId() -> Int{
        return self.lastIdSensor
    }
    
    public func getOBDLastId() -> Int{
        return self.lastIdOBD
    }
    
    
    public init() throws {
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
            ).first!
        
        dbQueue = try DatabasePool(path: "\(path)/database.sqlite")
        initDatabase()
    }
    
    func resetDatabase(){
        do{
            try dbQueue.write(){ db in
                try db.execute( "DELETE FROM sensordata")
                try db.execute( "DELETE FROM obddata")
            }
            startIdSensor = 0
            startIdObd = 0
            
        }catch{
            print(error)
        }
        
    }
    
    func getConfigValue(key: String) -> String?{
        var ret: String? = nil
        do{
            try dbQueue.read { db  in
                if let row = try Row.fetchOne(db, "SELECT * FROM config WHERE key = ?", arguments: [key]) {
                    ret = row.value(named: "value")
                }
            }
        }catch{
            print(error)
        }
        return ret
    }
    
    func saveConfigValue(key: String, value: String){
        do{
            let test = getConfigValue(key: key)
            if test == nil{
                try dbQueue.write { db  in
                    try db.execute( "INSERT INTO config (key, value) values (?, ?)", arguments: [key, value])
                }
            }else{
                try dbQueue.write { db  in
                    try db.execute( "UPDATE config set value = ? WHERE key = ?", arguments: [value, key])
                }
            }
            
        }catch{
            print(error)
        }
    }
    
    func initDatabase() {
        
        
        if (isDatabaseInited()){
            return
        }
        
        do{
            try dbQueue.write { db in
                try db.execute( "CREATE TABLE sensordata (id INTEGER PRIMARY KEY,acc TEXT,gyro TEXT, gps TEXT)")
                try db.execute( "CREATE TABLE obddata (id INTEGER PRIMARY KEY,data TEXT NOT NULL)")
                
                try db.execute( "CREATE TABLE config (id INTEGER PRIMARY KEY, key TEXT NOT NULL, value TEXT NOT NULL)")
            }
        }catch{
            print(error)
        }
    }
    
    func isDatabaseInited() -> Bool {
        var ret = false
        do{
            try dbQueue.read { db  in
                let _ = try Row.fetchCursor(db, "SELECT * FROM sensordata")
                ret = true
            }
        }catch{
            print(error)
        }
        return ret
    }
    
    func countSensorData() -> Int64 {
        var ret: Int64 = 0
        do{
            try dbQueue.read { db  in
                let row = try Row.fetchOne(db, "SELECT count(*) as datacount FROM sensordata")
                let test = row?.value(named: "datacount")
                if (test != nil){
                    if let j = Int64(exactly: test as! Int64) {
                        ret = Int64(j)
                    }
                }
                
            }
        }catch{
            print(error)
        }
        return ret
    }
    
    func maxSensorDataId() -> Int64 {
        var ret: Int64 = 0
        do{
            try dbQueue.read { db  in
                let row = try Row.fetchOne(db, "SELECT max(id) as id FROM sensordata")
                let test = row?.value(named: "id")
                if (test != nil){
                    ret = test as! Int64
                }
                
            }
        }catch{
            print(error)
        }
        return ret
    }
    
    
    func countObdData() -> Int {
        var ret = 0
        do{
            try dbQueue.read { db  in
                let row = try Row.fetchOne(db, "SELECT count(*) as datacount FROM obddata")
                let test = row?.value(named: "datacount")
                if (test != nil){
                    if let j = Int32(exactly: test as! Int64) {
                        ret = Int(j)
                    }
                }
                
            }
        }catch{
            print(error)
        }
        return ret
    }
    
    func maxObdDataId() -> Int64 {
        var ret: Int64 = 0
        do{
            try dbQueue.read { db  in
                let row = try Row.fetchOne(db, "SELECT max(id) as id FROM obddata")
                let test = row?.value(named: "id")
                if (test != nil){
                    ret = test as! Int64
                }
                
            }
        }catch{
            print(error)
        }
        return ret
    }
    
    
    
    public func executeBatch(queries: [String]){
        do{
            try dbQueue.write { db  in
                for query in queries{
                    try db.execute(query)
                }
            }
        }catch{
            print(error)
        }
    }
    
    public func createQuerySensor(acc: SensorData?, gyro: SensorData?, gps: SensorData?) -> String? {
        var ret: String? = nil
        
        do {
            var accData: String = "NULL"
            var gyroData: String = "NULL"
            var gpsData: String = "NULL"
            
            
            if (acc != nil) {
                let jsonData = acc?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                accData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            if (gyro != nil) {
                let jsonData = gyro?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                gyroData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            if (gps != nil) {
                let jsonData = gps?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                gpsData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            ret = "INSERT INTO sensordata (acc, gyro, gps) VALUES (\(accData),\(gyroData),\(gpsData))"
        } catch {
            print(error)
        }
        return ret
    }
    
    public func addSensor(acc: SensorData?, gyro: SensorData?, gps: SensorData?) {
        
        //let jsonData = try? JSONSerialization.data(withJSONObject: sensor.encodeToJSON(), options: [])
        do{
            var accData: String = "NULL"
            var gyroData: String = "NULL"
            var gpsData: String = "NULL"
            
            
            if (acc != nil) {
                let jsonData = acc?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                accData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            if (gyro != nil) {
                let jsonData = gyro?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                gyroData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            if (gps != nil) {
                let jsonData = gps?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                gpsData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            //String(describing: convertedString)
            try dbQueue.write { db in
                try db.execute( "INSERT INTO sensordata (acc, gyro, gps) VALUES (\(accData),\(gyroData),\(gpsData))")
            }
        }catch{
            print(error)
        }
    }
    
    public func addOBD(obd: ObdData?) {
        do{
            var obdData: String = "NULL"
            
            if (obd != nil) {
                let jsonData = obd?.encodeToJSON()
                let data =  try JSONSerialization.data(withJSONObject: jsonData!, options: .prettyPrinted )
                obdData = "'" + String(data: data, encoding: .utf8)! + "'"
            }
            
            try dbQueue.write { db in
                try db.execute( "INSERT INTO obddata (data) VALUES (\(obdData))")
            }
        }catch{
            print(error)
        }
    }
    
    
    public func deleteSensor(toId: Int = 0){
        var id = toId
        if (id == 0){
            id = self.lastIdSensor
            // Khoi xoa the xem nhanh khong
            self.startIdSensor = self.lastIdSensor
            
            return
        }
        
        do{
            try dbQueue.write { db  in
                try db.execute( "DELETE FROM sensordata WHERE id <= \(id)")
            }
        }catch{
            print(error)
        }
    }
    
    
    public func deleteFirstSensors(max: Double){
        if max == 0.0 {
            return
        }
        let maxCount = Int64(max * 1024 * 1024 / 100)
        
        do{
            let count = countSensorData()
            if (count <= maxCount){
                return
            }
            
            let deleteCount = maxCount - count
            
            try dbQueue.write { db  in
                try db.execute( "DELETE FROM sensordata WHERE id IN ( SELECT id FROM sensordata ORDER BY id LIMIT \(deleteCount))")
            }
        }catch{
            print(error)
        }
    }
    
    public func deleteOBD(toId: Int = 0){
        var id = toId
        if (id == 0){
            id = self.lastIdOBD
            
            // Khoi xoa the xem nhanh khong
            self.startIdObd = self.lastIdOBD
            
            return

        }
        
        do{
            try dbQueue.write { db  in
                try db.execute( "DELETE FROM obddata WHERE id <= \(id)")
            }
        }catch{
            print(error)
        }
    }
    
    public func getSensor<T: SensorData>(to: Int = 0, maxId: Int64 = 0) -> [T] {
        var ret = [T]()
        
        do{
            try dbQueue.read { db  in
                var count = 0
                var query = "SELECT * FROM sensordata WHERE id >\(startIdSensor) ORDER BY id"
                if maxId > 0{
                    query = "SELECT * FROM sensordata WHERE id >\(startIdSensor)  AND id <= \(maxId) ORDER BY id "
                }
                
                let rows = try Row.fetchCursor(db, query)
                
                
                while let row = try rows.next() {
                    if (to > 0 ){
                        count = count + 1
                        if count >= to {
                            break
                        }
                        
                    }
                    
                    for key in ["acc","gyro","gps"] {
                        let stringData: String? = row.value(named: key)
                        
                        
                        if let data = stringData?.data(using: .utf8) {
                            let parsedData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                            
                            let object = Decoders.decode(clazz: T.self, source: parsedData as AnyObject, instance: nil)
                            
                            ret.append(object)
                        }
                    }
                    
                    
                    self.lastIdSensor = row.value(named: "id")
                    
                    
                }
            }
        }catch{
            print(error)
        }
        
        return ret
    }
    
    public func getOBD<T: ObdData>(to: Int) -> [T] {
        var ret = [T]()
        
        do{
            try dbQueue.read { db  in
                var count = 0
                let rows = try Row.fetchCursor(db, "SELECT * FROM obddata WHERE  id >\(startIdObd) ORDER BY id")
                
                while let row = try rows.next() {
                    count = count + 1
                    if (count >= to){
                        break
                    }
                    
                    for key in ["data"] {
                        let stringData: String? = row.value(named: key)
                        
                        
                        if let data = stringData?.data(using: .utf8) {
                            let parsedData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                            
                            let object = Decoders.decode(clazz: T.self, source: parsedData as AnyObject, instance: nil)
                            
                            ret.append(object)
                        }
                    }
                    
                    
                    self.lastIdOBD = row.value(named: "id")
                    
                    
                }
            }
        }catch{
            print(error)
        }
        
        return ret
    }
    
}
