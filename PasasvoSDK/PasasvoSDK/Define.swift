//
//  CommonConfig.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/01.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit

open class AnswerType
{
    public static var yes = 1
    public static var no = 0
    
}


public enum CurrentHTTPMethod : String
{
    case GET = "GET"
    case POST = "POST"
    
}


public enum SystemCallbackType: Int
{
    case OBDDisconnected = 1
    case MaxDrivingTimeOver = 101
    case SensorData = 201
}


public enum BDOState: Int
{
    case Connected = 1
    case Disconnected = 2
}

public enum LogLevel: Int
{
    case Debug = 1
    case Info = 2
    case Error = 4
    case Warning = 8
    
}

open class BDOStateMessage
{
    static var Connected = NSLocalizedString("bdo_connected", comment: "")
    static var Disconnected = NSLocalizedString("bdo_disconnected", comment: "")
}


open class CommandModel
{
    var uuid : String?
    //  端末特定ID
    var command : Int?
    //  コマンド。1:測定開始、2:測定終了、10:イベント回答
    var miles : Double?
    //  走ったマイル
    var eventid : Int?
    //  イベントID
    var answer : Int?
    //  イベント回答
    var timestamp : Int?
    
}

public enum CommandCode : Int
{
    case Start = 1
    case Stop = 2
    case SendAnswer = 10
}

public enum SensorType: Int32
{
    case Acc = 1
    case GPS = 2
    case Gyro = 3
    
}


open class CanOBD2Mode
{
    static var CanModeList = [(name: "None",code: 0), (name:"Dummy",code: 1),(name: "Can",code:2)]
    static var None = CanModeList[0].code
    static var Dump = CanModeList[1].code
    static var RealData = CanModeList[2].code
    static var CanModeListName = [CanModeList[0].name, CanModeList[1].name, CanModeList[2].name]
    static public func getCanModeByName(name: String) -> Int?
    {
        for canMode in CanModeList
        {
            if canMode.name.uppercased() == name.uppercased()
            {
                return canMode.code
            }
        }
        return -1
    }
    
    static public func getCanModeByCode(code: Int) -> String?
    {
        for canCode in CanModeList
        {
            if canCode.code == code
            {
                return canCode.name
            }
        }
        return ""
    }
}


open class DangerousTrigger
{
    public static var FromUser = 1
    public static var FromServer = 2
}

open class DatasModel
{
    var accelerator : [[Int]]?
    var gyroscope : [[Int]]?
    var gps: [[Int]]?
    var rpm: Int!
    var fuelpress: Int!
}

open class ResponseMessage
{
    static var DeviceAlreadyRegistered200 = NSLocalizedString("device_already_registered_get_device", comment: "")
    static var DeviceIsNotRegistered400 = NSLocalizedString("device_is_not_registered_get_device", comment: "")
    static var SuccessRegister200 = NSLocalizedString("success_register", comment: "")
    static var SubmitDataSuccessWithEvent200 = NSLocalizedString("submit_data_success_with_event", comment: "")
    static var CommandSucess200 = NSLocalizedString("command_success", comment: "")
    static var DeviceAlreadyRegistered400 = NSLocalizedString("device_already_registered", comment: "")
    static var SubmitDataSuccessWithoutEvent400 = NSLocalizedString("submit_data_success_without_event", comment: "")
    static var ArgumentWrong401 = NSLocalizedString("argument_wrong", comment: "")
    static var AuthenticateFailed409 = NSLocalizedString("authenticate_failed", comment: "")
    static var SystemError410 = NSLocalizedString("system_error", comment: "")
    
}



public enum StartMetricCode: Int
{
    case Start = 1
    case SendData = 2
}

public enum ResponseCode : Int
{
    case Code200 = 200
    case Code400 = 400
    case Code401 = 401
    case Code409 = 409
    case Code410 = 410
}






