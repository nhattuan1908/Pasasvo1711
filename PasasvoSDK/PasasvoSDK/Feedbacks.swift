//
// Feedbacks.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


open class Feedbacks: JSONEncodable {
    
    public var measurementDatetime: String?
    public var trigger: Int32?
    public var dangerousDrivingId: String?
    public var dangerousDrivingType: Int32?
    
    public init() {}
    
    public init(measurementDatetime:String? , trigger: Int32?, dangerousDrivingId: String?, dangerousDrivingType: Int32?)
    {
        self.measurementDatetime = measurementDatetime
        self.trigger = trigger
        self.dangerousDrivingId = dangerousDrivingId
        self.dangerousDrivingType = dangerousDrivingType
    }
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> Any {
        var nillableDictionary = [String:Any?]()
        nillableDictionary["measurement_datetime"] = self.measurementDatetime
        nillableDictionary["trigger"] = self.trigger?.encodeToJSON()
        nillableDictionary["dangerous_driving_id"] = self.dangerousDrivingId
        nillableDictionary["dangerous_driving_type"] = self.dangerousDrivingType?.encodeToJSON()
        
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}

