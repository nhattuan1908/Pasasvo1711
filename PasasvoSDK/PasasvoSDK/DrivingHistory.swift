//
// DrivingHistory.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


open class DrivingHistory: JSONEncodable {

    public var drivingStatus: Int32?
    public var safeDrivingLevel: Int32?
    public var drivingStartDatetime: String?
    public var drivingEndDatetime: String?
    public var measurement_DateTime: String?
    public var drivingDistance: Int32?
    public var drivingTime: Int32?
    public var averageSpeed: Int32?
    public var memo: String?

    public init() {}

    // MARK: JSONEncodable
    open func encodeToJSON() -> Any {
        var nillableDictionary = [String:Any?]()
        nillableDictionary["driving_status"] = self.drivingStatus
        nillableDictionary["safe_driving_level"] = self.safeDrivingLevel
        nillableDictionary["driving_start_datetime"] = self.drivingStartDatetime
        nillableDictionary["driving_end_datetime"] = self.drivingEndDatetime
        nillableDictionary["measurement_datetime"] = self.measurement_DateTime
        nillableDictionary["driving_distance"] = self.drivingDistance
        nillableDictionary["driving_time"] = self.drivingTime
        nillableDictionary["average_speed"] = self.averageSpeed
        nillableDictionary["memo"] = self.memo

        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
