//
//  Profile.swift
//  PasasvoSDK
//
//  Created by unitec-MAC on 12/12/17.
//  Copyright © 2017 Unitec. All rights reserved.
//

import Foundation

open class User {
    
    static public func registerUser(user: UserModel , callback: @escaping (_ responseCode: Int?, _ userId: String?, _ errorMessage: String?) -> (Void)) {
        let param = UserReturn()
        param.users = [user]
        
        UserManageAPI.userPost(userParam: param) { (result, error) in
            if error == nil
            {
                callback(200, (result?.users?.first?.userId), error.debugDescription)
            }
            else
            {
                callback((error?.0),nil, error.debugDescription)
            }
        }
    }
    
    static public func deleteUser(userId: String, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.userDelete(userId: userId, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    
    static public func updateUser(userId: String , user: UserModel, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = UserReturn()
        param.users = [user]
        
        UserManageAPI.userPut(userId: userId, userParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func getUser(userId: String, callback: @escaping (_ responseCode: Int?, _ user: UserModel?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.userGet(userId: userId, completion: { (data, error) in
            if error == nil
            {
                callback(200, (data?.users?.first), error.debugDescription)
            }
            else
            {
                callback((error?.0), nil, error.debugDescription)
            }
        })
    }
    
    static public func getUserList(callback: @escaping (_ responseCode: Int?, _ userlist: UserReturn?, _ errorMessage: String) -> (Void)) {
        UserManageAPI.userListGet(completion: { (data, error) in
            if error == nil
            {
                callback(200, data!, error.debugDescription)
            }
            else
            {
                callback((error?.0)!, nil , error.debugDescription)
            }
        })
    }
    
    
    static public func registerCar(userId: String,carId: String, car: Car, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = CarReturn()
        param.cars = [car]
        
        let a = Util.toUUIDString(value: carId)
        
        UserManageAPI.carPost(userId: userId, carId: a, carParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func deleteCar(userId: String, carId: String, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.carDelete(userId: userId, carId: carId, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    
    static public func updateCar(userId: String, carId: String, car: Car, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = CarReturn()
        param.cars = [car]
        
        UserManageAPI.carPut(userId: userId, carId: carId, carParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func getCar(userId: String, carId: String, callback: @escaping (_ responseCode: Int?, _ car: Car?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.carGet(userId: userId, carId: carId, completion: { (data, error) in
            if error == nil
            {
                callback(200, (data?.cars?.first), error.debugDescription)
            }
            else
            {
                callback((error?.0), nil, error.debugDescription)
            }
        })
    }
    
    static public func getCarList(userId: String, callback: @escaping (_ responseCode: Int?, _ carlist: CarReturn?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.carsGet(userId: userId, completion: { (data, error) in
            if error == nil
            {
                callback(200, data, error.debugDescription)
            }
            else
            {
                callback((error?.0),nil, error.debugDescription)
            }
        })    }
    
    static public func deleteCarList(userId: String, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.carsDelete(userId: userId, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }

    
    static public func registerDevice(userId: String, deviceId: String, device: Device, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = DeviceReturn()
        param.devices = [device]
        let a = Util.toUUIDString(value: deviceId)
        
        
        UserManageAPI.devicePost(userId: userId, deviceId: a, deviceParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func deleteDevice(userId: String, deviceId: String, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.deviceDelete(userId: userId, deviceId: deviceId, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    
    static public func updateDevice(userId: String, deviceId: String, deviceData: Device, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = DeviceReturn()
        param.devices = [deviceData]
        
        UserManageAPI.devicePut(userId: userId, deviceId: deviceId, deviceParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func getDevice(userId: String, deviceId: String, callback: @escaping (_ responseCode: Int?, _ device: Device?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.deviceGet(userId: userId, deviceId: deviceId, completion: { (data, error) in
            if error == nil
            {
                callback(200, (data?.devices?.first), error.debugDescription)
            }
            else
            {
                callback((error?.0),nil, error.debugDescription)
            }
        })
    }
    
    static public func getDeviceList(userId: String, callback: @escaping (_ responseCode: Int?, _ devicelist: DeviceReturn?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.devicesGet(userId: userId, completion: { (data, error) in
            if error == nil
            {
                callback(200, data, error.debugDescription)
            }
            else
            {
                callback((error?.0) ,nil, error.debugDescription)
            }
        })
    }
    
    static public func deleteDeviceList(userId: String, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.devicesDelete(userId: userId, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }

    
    static public func registerProfile(userId: String, profile: Profile, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = ProfileParam()
        param.profiles = [profile]
        
        UserManageAPI.profilePost(userId: userId, userParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func deleteProfile(userId: String,callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.profileDelete(userId: userId, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    
    static public func updateProfile(userId: String,profile: Profile, callback: @escaping (_ responseCode: Int?, _ errorMessage: String?) -> (Void)) {
        let param = ProfileParam()
        param.profiles = [profile]
        
        UserManageAPI.profilePut(userId: userId, profileParam: param, completion: { (error) in
            if error != nil
            {
                callback((error?.0), error.debugDescription)
            }
            else
            {
                callback(200, nil)
            }
        })
    }
    
    static public func getProfile(userId: String,callback: @escaping (_ responseCode: Int?, _ profile: Profile?, _ errorMessage: String?) -> (Void)) {
        UserManageAPI.profileGet(userId: userId, completion: { (data, error) in
            if error == nil
            {
                callback(200, (data?.profiles?.first), error.debugDescription)
            }
            else
            {
                callback((error?.0) ,nil, error.debugDescription)
            }
        })

    }
    
    static public func getHistoryDriving(drivingId: String, callback: @escaping(_ result: [DrivingDataGetReturn]) -> (Void)) {
        ServerGetter.getHistory(drivingId: drivingId, completeHanlder: { (code, message, result) -> (Void) in
                PasasvoConfig.callbackHistoryDriving(result!)
        })
    }
    
    static public func login(login: Login , callback: @escaping (_ responseCode: Int?, _ userId: String?, _ errorMessage: String?) -> (Void)) {
        
        let param = LoginReturn()
        param.login = [login]
        
        UserManageAPI.login(loginParam: param) { (result, error) in
            if result != nil
            {
                callback(200, (result?.login?.first?.userId), error.debugDescription)
            }
            else
            {
                callback((error?.0),nil, error.debugDescription)
            }
        }
        
    }
}
