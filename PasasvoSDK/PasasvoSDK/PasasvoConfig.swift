//
//  PasasvoConfig.swift
//  SensorsLog
//
//  Created by Unitec on 12/10/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation

open class PasasvoConfig {
    
    public static var version = ""
    public static var drivingId = ""
    
    
    public static var intervalCan = 3
    public static var intervalSensor = 20

    public static var intervalCalcSpeed = 3
    public static var intervalServerPolling = 3
    public static var intervalDrivingData = 3
    public static var intervalDataUpload : Float = 3.0
    
    public static var testMode = false
    public static var memo = "IOS"
    public static var https = false
    
    public static var serverUrl = "elb-frontend-1085886777.ap-northeast-1.elb.amazonaws.com/api/v1"
    
    public static var sensorOption = [Int]()
    
    public static var obdOption = CanOBD2Mode.None
    
    public static var userId = ""
    public static var carId = ""
    public static var deviceId = ""
    
    public static var maxDrivingTime = 0.0
    public static var maxSensorMemory = 0.0
    
    

    
    static func load(){
        let value = db?.getConfigValue(key: "driving_id")
        if value != nil{
            drivingId = value!
        }
    }
    
    static func save(){
        db?.saveConfigValue(key: "driving_id", value: drivingId)
    }
    
    static var callbackSystem:  ((_ code: Int, _ responseCode: Int, _ errorMessage: String?) -> (Void))? = {(_ code: Int, _ responseCode: Int, _ errorMessage: String?) in
        
    }
    
    static var callbackDistance:  ((_ speed: Double,_ distance: Double, _ drivingTime: Double) -> (Void))? = { (_ speed: Double,_ distance: Double, _ drivingTime: Double) in
        
    }
    
    static var callbackDangerous:  ((_ responseCode: Int?, _ data: DangerousDriving?, _ errorMessage: String?) -> (Void))? = {(_ responseCode: Int?, _ data: DangerousDriving?, _ errorMessage: String?) in
        
    }
    
    static var callbackHistorySummary:((_ dataheader: DrivingHistorySummaryHeader?, _ data: [DrivingHistorySummaryBody]?) -> (Void)) = {(_ dataheader: DrivingHistorySummaryHeader?, _data: [DrivingHistorySummaryBody]? ) in
        
    }
    
    static var callbackHistoryDriving:  ((_ data: [DrivingDataGetReturn]) -> (Void)) = {(_ data: [DrivingDataGetReturn]) in
        
    }
    
    static public func setSystemCallback(callback: @escaping (_ code: Int, _ responseCode: Int, _ errorMessage: String?) -> (Void)) {
        PasasvoConfig.callbackSystem = callback
    }
    
    static public func setDistanceCallback(callback: @escaping (_ speed: Double, _ distance:Double, _ drivingTime: Double ) -> (Void)) {
        PasasvoConfig.callbackDistance = callback
    }

    
    static public func setDangerousCallback(callback: @escaping (_ responseCode: Int?, _ data: DangerousDriving?, _ errorMessage: String? ) -> (Void)) {
        PasasvoConfig.callbackDangerous = callback
    }
    

    static public func setHistorySummaryCallback(callback: @escaping (_ dataheader: DrivingHistorySummaryHeader?, _ data: [DrivingHistorySummaryBody]?) -> (Void)) {
        PasasvoConfig.callbackHistorySummary = callback
    }
    
    static public func setHistoryDrivingCallback(callback: @escaping (_ data: [DrivingDataGetReturn] ) -> (Void)) {
        PasasvoConfig.callbackHistoryDriving = callback
    }

    
    
    static public func getFormattedServerUrl() -> String{
        
        if(serverUrl.characters.last == "/")
        {
            PasasvoConfig.serverUrl = PasasvoConfig.serverUrl.substring(to: PasasvoConfig.serverUrl.index(before: PasasvoConfig.serverUrl.endIndex))

        }
        
        
        if (PasasvoConfig.https)
        {
            return "https://\(PasasvoConfig.serverUrl)"
        }
        else
        {
            return "http://\(PasasvoConfig.serverUrl)"
        }
    }
    static public func setOBDOption(value: Int) {
        PasasvoConfig.obdOption = value
    }
    
    static public func getOBDOption() -> Int {
        return PasasvoConfig.obdOption
    }
    //
    static public func setSensorOption(gyro: Int, acc: Int, gps: Int) {
        PasasvoConfig.sensorOption.append(gyro)
        PasasvoConfig.sensorOption.append(acc)
        PasasvoConfig.sensorOption.append(gps)
    }
    
    static public func getSensorOption() -> [Int] {
        return PasasvoConfig.sensorOption
    }
    //
    static public func setSecurities(value: Bool) {
        PasasvoConfig.https = value
    }
    
    static public func getSecurities() -> Bool {
        return PasasvoConfig.https
    }
    //
    static public func setTestMode(value: Bool) {
        PasasvoConfig.testMode = value
    }
    
    static public func getTestMode() -> Bool {
        return PasasvoConfig.testMode
    }
    //
    static public func setServerAddress(value: String){
        PasasvoConfig.serverUrl = value
    }
    
    static public func getServerAddress()  -> String  {
        return PasasvoConfig.serverUrl
    }
    
    //
    static public func setMaxMemory(value: Double) {
        PasasvoConfig.maxSensorMemory = value
    }
    
    static public func getMaxMemory() -> Double{
        return PasasvoConfig.maxSensorMemory
    }
    
    //
    static public func setInterval(sensor: Int, upload: Int, speedCalc: Int, polling: Int, drivingData: Int) {
        PasasvoConfig.sensorOption.append(sensor)
        PasasvoConfig.sensorOption.append(upload)
        PasasvoConfig.sensorOption.append(speedCalc)
        PasasvoConfig.sensorOption.append(polling)
        PasasvoConfig.sensorOption.append(drivingData)
    }
    
    static public func getInterval() -> [Int] {
        return PasasvoConfig.sensorOption
    }
    //
    static public func setMemo(value: String) {
        PasasvoConfig.memo = value
    }
    
    static public func getMemo() -> String {
        return PasasvoConfig.memo
    }
    //
    static public func setMaxTime(value: Double) {
        PasasvoConfig.maxDrivingTime = value
    }
    
    static public func getMaxTime() -> Double {
        return PasasvoConfig.maxDrivingTime
    }
    //
    static public func setCurrentCar(value: String) {
        PasasvoConfig.carId = value
    }
    
    static public func getCurrentCar() -> String {
        return PasasvoConfig.carId
    }
    static public func setCurrentUser(value: String) {
        PasasvoConfig.userId = value
    }
    
    static public func getCurrentUser() -> String {
        return PasasvoConfig.userId
    }
    
    static public func setCurrentDevice(value: String) {
        PasasvoConfig.deviceId = value
    }
    
    static public func getCurrentDevice() -> String {
        return PasasvoConfig.deviceId
    }
    
    static public func setVersion(value: String) {
        PasasvoConfig.version = value
    }
    
    static public func getVersion() -> String {
        return PasasvoConfig.version
    }
}



