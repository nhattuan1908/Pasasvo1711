//
//  ServerSender.swift
//  SensorsLog
//
//  Created by Unitec on 12/9/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation

open class ServerSender: ServerBase {
    
    // If sending, do nothing
    private static var sending: Bool = false
    private static var sendingWaitCount = 0
    
    static func sendDriving(drivingData: DrivingData , noBlock: Bool = true, completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?) -> (Void)) -> Bool
    {
        
        initParam()
        
        let param = DrivingDataParam()
        param.dataProperties = dataProperties
        param.drivingData = [drivingData]
        
        
        if sending && !noBlock {

            sendingWaitCount += 1
            if (sendingWaitCount >= 50){
                sendingWaitCount = 0
                sending = false
            }
            return false

        }
            
        if (!noBlock){
            sending = true
        }
        
        let drivingStatus = drivingData.drivingStatus
        
        if drivingStatus != 1 {
            drivingData.drivingId = PasasvoConfig.drivingId
        }else{
            param.dataProperties?.drivingId = nil
        }
        
        DataManageAPI.drivingDataPost(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingDataParam: param) { (data,error) in
            
            if (!noBlock){
                sending = false
            }
            
            if error == nil
            {
                if drivingStatus == 1{
                    let body = data?.body
                    if(body != nil)
                    {
                        PasasvoConfig.drivingId = (body?.drivingId)!
                        
                        // Permanance driving ID
                        PasasvoConfig.save()
                        
                        if PasasvoConfig.drivingId == ""{
                            completeHanlder(500, "Driving ID not return")
                        }else{
                            completeHanlder(200, nil)
                        }
                        
                    }else{
                        completeHanlder((data?.statusCode)!, data.debugDescription)
                    }
                }else{
                    // Fire event
                    completeHanlder((data?.statusCode)!, data.debugDescription)
                }

            }else{
                // Fire event
                    completeHanlder((error?.0)!, error.debugDescription)
            }
            
            
        }
        
        return true
    }
    
    
    static public func sendFeedback(feedback: Feedbacks , completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?) -> (Void)) -> Void
    {
        
        initParam()
        let param = FeedbacksParam()
        param.dataProperties = dataProperties
        param.feedbacks = [feedback]
        
        if feedback.dangerousDrivingId == nil{
            feedback.dangerousDrivingId = UUID().uuidString
        }
        
        DataManageAPI.feedbacksPost(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingId: PasasvoConfig.drivingId , feedbacksParam: param) { (data, error) in
            if error == nil
            {
                // Fire event
                completeHanlder((data?.statusCode)!, data.debugDescription)
                
            }else{
                // Fire event
                
                    completeHanlder((error?.0)!, error.debugDescription)

            }
            
        }
    }
    
    
    static public func sendSensorData(completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?) -> (Void)) -> Bool
    {
       
        if sending{
            sendingWaitCount += 1
            if (sendingWaitCount >= 200){
                sendingWaitCount = 0
                sending = false
            }
            return false
        }
        

        initParam()
        
        let sensorDataSend = SensorDataParam()
        sensorDataSend.sensorData = [SensorData]()
        
        //let maxId = db?.maxSensorDataId()
        
        //if maxId! < 1{
            // Have nothing to sen
        //    return false
        //}
        
        // Xoa truoc neu memory bi tran
        db?.deleteFirstSensors(max: PasasvoConfig.maxSensorMemory)
        
        let l = db?.getSensor(to: 500)

        
        if ((l?.count)! < 1) {
            return false
        }
        
        for item in l!{
            sensorDataSend.sensorData?.append(item)
        }
        
        
        sensorDataSend.dataProperties = dataProperties
    
        if ((sensorDataSend.sensorData?.count)! > 0 )
        {
            // Lock sending
            ServerSender.sending = true
            DataManageAPI.sensorDataPost(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingId: PasasvoConfig.drivingId, sensorDataParam: sensorDataSend) { (error) in
                // Unlock sending
                ServerSender.sending = false
                if error == nil
                {
                    // Delete if send success
                    db?.deleteSensor()
                    // Fire event
                    completeHanlder(200, nil)
                    PasasvoConfig.callbackSystem!(SystemCallbackType.SensorData.rawValue, 200, nil)
                    
                }else{
                    // Fire event
                        completeHanlder((error?.0)!, error.debugDescription)

                    PasasvoConfig.callbackSystem!(SystemCallbackType.SensorData.rawValue, (error?.0)!, error.debugDescription)
                }
                
                
            }
            
        }
        return true
    }

    
    static public func sendCanData(completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?) -> (Void)) -> Bool
    {
        
        if sending{
            sendingWaitCount += 1
            if (sendingWaitCount >= 200){
                sendingWaitCount = 0
                sending = false
            }
            return false
        }
        
        
        initParam()
        
        let obdDataSend = OdbDataParam()
        obdDataSend.obdData = [ObdData]()
        
        obdDataSend.obdAdapter = DataCollector.obdApdater
        obdDataSend.dataProperties = dataProperties
        
        let l = db?.getOBD(to: 1000)
        
        
        if ((l?.count)! < 1) {
            return false
        }
        
        for item in l!{
            obdDataSend.obdData?.append(item)
        }
        

        if ((obdDataSend.obdData?.count)! > 0 )
        {
            // Lock sending
            ServerSender.sending = true
            DataManageAPI.canDataPost(userId: dataProperties.userId!, deviceId: dataProperties.deviceId!, carId: dataProperties.carId!, drivingId: dataProperties.drivingId!, odbDataParam: obdDataSend) { (error) in
                // Unlock sending
                ServerSender.sending = false
                if error == nil
                {
                    // Delete if send success
                    db?.deleteOBD()
                    // Fire event
                    completeHanlder(200, nil)
                    
                }else{
                    // Fire event
                        completeHanlder((error?.0)!, error.debugDescription)

                }
                
                
            }
            
        }
        return true
    }

}
