//
//  DataCollector.swift
//  Pasasvo
//
//  Created by Unitec on 12/9/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit
import CoreMotion
import CoreLocation
import Alamofire
import OBD2

open class DataCollector {
    public static var currentDistance: Double = 0
    public static var currentSpeed: Double  = 0
    public static var averageSpeed: Double = 0
    public static var drivingTime: Double = 0
    static var startedTime: Double = 0
    static var lastLocation: CLLocation?
    static var sensorPool = [String]()
    
    private static var timerGDC: DispatchSourceTimer?
    
    static let obd: OBD2 = OBD2()
    
    private static var obdPids = [Int]()
    
    private static var obdTimer: Timer!
    private static var gpsTimer: Timer!
    private static var dangerousTimer: Timer!
    private static var drivingTimer: Timer!
    private static var dataTimer: Timer!
    
    
    private static var obdConnected = false
    static var obdApdater:ObdAdapter = ObdAdapter()
    private static var obdRequestedPidCount = -1
    private static var obdTimerCount = -1
    private static var notifyOBD2Data = ObdData()
    
    private static var motionManager = CMMotionManager()
    private static var locationManager: LocationMangager!
        
    private static func initOBDPids() {
        if obdPids.count > 0{
            return
        }
        
        for i in 0...32
        {
            obdPids.append(i)
        }
        

        
    }
    
    
    private static func connectObd(){

        print("Start connect")
        
        if obdConnected {
            return
        }
        
        
        obd.connect { (success, error) in
            print ("connect event!")
            if let error = error {
                print("OBD connection failed with \(error)")
            } else {
                obdConnected = true
                print("Connect success")
                obd.request(command: Command.Custom.string("ST SN")) { (descr) in
                    if let response = descr?.getRawResponse() {
                        obdApdater.serialNumber = response
                        print ("obdApdater.serialNumber = \(String(describing: obdApdater.serialNumber))")
                    }
                }
                
                requestObdData()
                
            }
        }
    }
    
    private static func initObdAdapter(){
        if obdConnected {
            return
        }
        
        obdApdater.serialNumber = "123456789012"
        
        obd.stateChanged = { (state) in
            if state == ScanState.connected //state == ScanState.openingConnection || 
            {
                obdConnected = true
            }else{
                obdConnected = false
            }
        }
    }
    
    static private func storeObdData(){
        print ("OBD data set prepared. Ready to send.")
        
        // Send can data
        // Only adapter inited
        if DataCollector.obdApdater.serialNumber != "123456789012" {
            db?.addOBD(obd: notifyOBD2Data)
        }
        
        // Reset
        notifyOBD2Data = ObdData()
        notifyOBD2Data.measurementDatetime = Util.getTodayString()
    }
    
    static private func requestObdData(){
        // No connection
        if (!obdConnected){
            return
        }
        
        if obdRequestedPidCount < 0{
            obdRequestedPidCount = 0
        }
        
        if obdRequestedPidCount >= obdPids.count{
            storeObdData()
            obdRequestedPidCount = 0
            
        }
        
        let command = Command.Mode01.pid(number: obdPids[obdRequestedPidCount])
        obd.request(command: command) { (descriptor) in
            let i = obdRequestedPidCount
            
            print("obd.request \(obdRequestedPidCount) = \(String(describing: descriptor))")
            
            if let valueAsci = descriptor?.asciValue {
                if(i == 1)  { notifyOBD2Data.pid01 = valueAsci }
                if(i == 2)  { notifyOBD2Data.pid02 = valueAsci }
                if(i == 3)  { notifyOBD2Data.pid03 = valueAsci }
                if(i == 18) { notifyOBD2Data.pid12 = valueAsci }
                if(i == 19) { notifyOBD2Data.pid13 = valueAsci }
                if(i == 20) { notifyOBD2Data.pid14 = valueAsci }
                if(i == 21) { notifyOBD2Data.pid15 = valueAsci }
                if(i == 22) { notifyOBD2Data.pid16 = valueAsci }
                if(i == 23) { notifyOBD2Data.pid17 = valueAsci }
                if(i == 24) { notifyOBD2Data.pid18 = valueAsci }
                if(i == 25) { notifyOBD2Data.pid19 = valueAsci }
                if(i == 26) { notifyOBD2Data.pid1a = valueAsci }
                if(i == 27) { notifyOBD2Data.pid1b = valueAsci }
                if(i == 28) { notifyOBD2Data.pid1c = valueAsci }
                if(i == 29) { notifyOBD2Data.pid1d = valueAsci }
                if(i == 30) { notifyOBD2Data.pid1e = valueAsci }
            }

            if let valueGot = descriptor?.valueMetrics  {
                let value = Double(valueGot)
                if (i == 4)  { notifyOBD2Data.pid04 = String(value) }
                if (i == 5)  { notifyOBD2Data.pid05 = String(value) }
                if (i == 6)  { notifyOBD2Data.pid06 = String(value) }
                if (i == 7)  { notifyOBD2Data.pid07 = String(value) }
                if (i == 8)  { notifyOBD2Data.pid08 = String(value) }
                if (i == 9)  { notifyOBD2Data.pid09 = String(value) }
                if (i == 10) { notifyOBD2Data.pid0a = String(value) }
                if (i == 11) { notifyOBD2Data.pid0b = String(value) }
                if (i == 12) { notifyOBD2Data.pid0c = String(value) }
                if (i == 13) { notifyOBD2Data.pid0d = String(value) }
                if (i == 14) { notifyOBD2Data.pid0e = String(value) }
                if (i == 15) { notifyOBD2Data.pid0f = String(value) }
                if (i == 16) { notifyOBD2Data.pid10 = String(value) }
                if (i == 17) { notifyOBD2Data.pid11 = String(value) }
                if (i == 31) { notifyOBD2Data.pid1f = String(value) }
            }
            obdRequestedPidCount += 1
            requestObdData()
        }
    }
    
    static public func startOBDCollector(interval:Int, dummy: Bool = false)
    {
        initObdAdapter()
        initOBDPids()
        
        if dummy == false{
            connectObd()
        }
        
        obdTimerCount = 0
        
        obdRequestedPidCount = 0
        
        
        notifyOBD2Data.measurementDatetime = Util.getTodayString()
        let timeInterval = TimeInterval(Float(interval))
        
        
        obdTimer = Timer.every(timeInterval, {
            
            if obdTimerCount >= 3 {
                
                obdTimerCount = 0
                
                
                // Send dummy
                if dummy {
                    let obdData = createDummyObdData()
                    db?.addOBD(obd: obdData)
                    return
                }else{
                    // Co can phai send data o day?
                }
            }
            
            obdTimerCount += 1
            
        })
    }
    
    
    
    static public func stopOBDCollector()
    {
        if obdTimer != nil{
            obdTimer.invalidate()
            obdTimer = nil
        }
        
        obd.disconnect()
        obdConnected = false
        
        
    }

    
    static func startSensorCollector(timeInterval: Int, sensorMode: [Int])
    {

        
        let interval = Double(timeInterval)/1000.0
        motionManager.gyroUpdateInterval = TimeInterval(interval)
        motionManager.accelerometerUpdateInterval = TimeInterval(interval)
        
        if (sensorMode.index(of: (Int(SensorType.Gyro.rawValue))) != nil)
        {
            motionManager.startGyroUpdates()
        }
        
        if (sensorMode.index(of: (Int(SensorType.Acc.rawValue))) != nil)
        {
            motionManager.startAccelerometerUpdates()
        }
        
        let queueGDC = DispatchQueue(label: "co.jp.unitec.sensor.timer", attributes: .concurrent)
    
        timerGDC?.cancel()
        
        timerGDC = DispatchSource.makeTimerSource(queue: queueGDC)
        
        timerGDC?.scheduleRepeating(deadline: .now(), interval: .milliseconds(timeInterval), leeway: .microseconds(1))
        
        timerGDC?.setEventHandler {
            
            if (interval==0){
                return
            }
            
            let currentTimestamp = NSDate().timeIntervalSince1970
            // NSLog("Sensor time = \(currentTimestamp)")
            
            var acc: SensorData? = nil
            var gyro: SensorData? = nil
            var gps: SensorData? = nil
            
            let timestamp = Util.getTodayString(timestamp: currentTimestamp)
            
            if (sensorMode.index(of: (Int(SensorType.Gyro.rawValue))) != nil)
            {
                var x = 0.0
                var y = 0.0
                var z = 0.0
                
                if let data = motionManager.gyroData {
                    x = data.rotationRate.x
                    y = data.rotationRate.y
                    z = data.rotationRate.z
                    
                }
                let sensor = SensorData()
                sensor.measurementDatetime = timestamp
                sensor.sensorType = SensorType.Gyro.rawValue
                sensor.sensorValue1 = String(x)
                sensor.sensorValue2 = String(y)
                sensor.sensorValue3 = String(z)
                gyro = sensor
            }
            
            if (sensorMode.index(of: (Int(SensorType.Acc.rawValue))) != nil)
            {
                var x = 0.0
                var y = 0.0
                var z = 0.0
                
                
                if let data = motionManager.accelerometerData {
                    x = data.acceleration.x*(-9.8)
                    y = data.acceleration.y*(-9.8)
                    z = data.acceleration.z*(-9.8)
                }
                let sensor = SensorData()
                sensor.measurementDatetime = timestamp
                sensor.sensorType = SensorType.Acc.rawValue
                sensor.sensorValue1 = String(x)
                sensor.sensorValue2 = String(y)
                sensor.sensorValue3 = String(z)
                acc = sensor
                
                
            }
            
            
            // For store GPS data
            if (sensorMode.index(of: (Int(SensorType.GPS.rawValue))) != nil)
            {
                let sensor = SensorData()
                sensor.sensorType = SensorType.GPS.rawValue
                sensor.sensorValue1 = String(LocationMangager.currentLocation.coordinate.latitude)
                sensor.sensorValue2 = String(LocationMangager.currentLocation.coordinate.longitude)
                sensor.measurementDatetime = timestamp

                gps = sensor
                
            }
            
            if (acc != nil || gyro != nil || gps != nil ){
                let queue = DispatchQueue(label: "co.jp.unitec.sendsensor")
                queue.async {
                    db?.addSensor(acc: acc, gyro: gyro, gps: gps)
                }
                
                
                
//                
//                let query = db?.createQuerySensor(acc: acc, gyro: gyro, gps: gps)
//                sensorPool.append(query!)
//                
//                if (sensorPool.count > 20) {
//                    
//                    let queue = DispatchQueue(label: "co.jp.unitec.sendsensor")
//                    let poolForWrite = sensorPool
//                    sensorPool = [String]()
//                    
//                    queue.async {
//                        
//                        db?.executeBatch(queries: poolForWrite)
//                        
//                    }
//                }
            }
            
        }
        timerGDC?.resume()
        
        
    }
    
    static func stopSensorCollector()
    {
        motionManager.stopAccelerometerUpdates()
        motionManager.stopGyroUpdates()
        timerGDC?.cancel()
        timerGDC = nil
    }
    
    
    static private func createDummyObdData() -> ObdData{
        let notifyOBD2Data = ObdData()
        notifyOBD2Data.pid01 = "MIL = On; DTCs = 121; Spark ignition monitors supported"
        notifyOBD2Data.pid02 = "01"
        notifyOBD2Data.pid03 = "Open loop due to insufficient engine temperature"
        notifyOBD2Data.pid04 = "50"
        notifyOBD2Data.pid05 = "30"
        notifyOBD2Data.pid06 = "30"
        notifyOBD2Data.pid07 = "40"
        notifyOBD2Data.pid08 = "50"
        notifyOBD2Data.pid09 = "60"
        notifyOBD2Data.pid0a = "200"
        notifyOBD2Data.pid0b = "100"
        notifyOBD2Data.pid0c = "200"
        notifyOBD2Data.pid0d = "100"
        notifyOBD2Data.pid0e = "10"
        notifyOBD2Data.pid0f = "10"
        notifyOBD2Data.pid10 = "100"
        notifyOBD2Data.pid11 = "100"
        notifyOBD2Data.pid12 = "Upstream"
        notifyOBD2Data.pid13 = "01"
        notifyOBD2Data.pid14 = "1.1; -10"
        notifyOBD2Data.pid15 = "1.1; -10"
        notifyOBD2Data.pid16 = "1.1; -10"
        notifyOBD2Data.pid17 = "1.1; -10"
        notifyOBD2Data.pid18 = "1.1; -10"
        notifyOBD2Data.pid19 = "1.1; -10"
        notifyOBD2Data.pid1a = "1.1; -10"
        notifyOBD2Data.pid1b = "1.1; -10"
        notifyOBD2Data.pid1c = "OBD-II"
        notifyOBD2Data.pid1d = "01"
        notifyOBD2Data.pid1e = "Activite"
        notifyOBD2Data.pid1f = "30"
        notifyOBD2Data.measurementDatetime = Util.getTodayString()
        return notifyOBD2Data
    }
    
    
    static func startGPSLocation(interval: Int)
    {
        locationManager =  LocationMangager.locationManagerInstance
        locationManager.startLocation()
        startedTime = NSDate().timeIntervalSince1970

        
        currentDistance = 0
        currentSpeed  = 0
        averageSpeed = 0
        drivingTime = 0

        
        gpsTimer = Timer.every(TimeInterval(interval), {
            calculateDistance()
            
            // Fire event
            PasasvoConfig.callbackDistance!(DataCollector.currentSpeed, DataCollector.currentDistance, DataCollector.drivingTime)
            
            // Max time check
            if PasasvoConfig.maxDrivingTime != 0.0{
                if PasasvoConfig.maxDrivingTime * 60 < DataCollector.drivingTime {
                    // Tell client stop
                    Pasasvo.stopDataCollect(completeHanlder: { (code, message) -> (Void) in
                        PasasvoConfig.callbackSystem!(SystemCallbackType.MaxDrivingTimeOver.rawValue, code, message)
                        
                    })
                }
            }
        })
        
    }
    
    static func stopGPSLocation()
    {
    
        if (locationManager != nil)
        {
            locationManager.stopLocation()
        }
        
        if gpsTimer != nil{
            gpsTimer.invalidate()
            gpsTimer = nil
        }
    }
    
    
    static func startGetDangerousTimer(interval: Int){
        dangerousTimer = Timer.every(TimeInterval(interval), {
            _ = ServerGetter.getDangerousDriving(completeHanlder: { (code, message, result) in
                if result != nil {
                    PasasvoConfig.callbackDangerous!(code, result!, message)
                }
                else
                {
                    PasasvoConfig.callbackDangerous!(code, nil, message)
                }
            })
        })
    }
    
    static func startSendDrivingTimer(interval: Int){
        drivingTimer = Timer.every(TimeInterval(interval), {
            let param = DrivingData()
            param.drivingStatus = 2
            param.memo = PasasvoConfig.memo
            param.measurementDatetime = param.drivingEndDatetime
            param.drivingId = PasasvoConfig.drivingId
            param.averageSpeed = Int32(DataCollector.averageSpeed)
            param.drivingTime = Int32(DataCollector.drivingTime)
            param.drivingDistance = Int32 (DataCollector.currentDistance)
            
            _ = ServerSender.sendDriving(drivingData: param, completeHanlder: { (code, result) in
                
            })
        })
    }
    
    
    static func startSendDataTimer(interval: Int){
        
        var timeInterval = interval
        if timeInterval == 0 {
            // Bat buoc phai lon hon 1 giay
            timeInterval = 1
        }
        
        dataTimer = Timer.every(TimeInterval(timeInterval), {
            _ = ServerSender.sendSensorData(){ (_, _) in
                
                if PasasvoConfig.obdOption != CanOBD2Mode.None{
                    _ = ServerSender.sendCanData() { (_,_) in
                        
                    }
                }
                
            }
        })
    }
    
    
    static func stopGetDangerousTimer(){
        if dangerousTimer != nil{
            dangerousTimer.invalidate()
            dangerousTimer = nil
        }
    }
    
    static func stopSendDrivingTimer(){
        if drivingTimer != nil{
            drivingTimer.invalidate()
            drivingTimer = nil
        }
    }
    
    static func stopSendDataTimer(){
        if dataTimer != nil{
            dataTimer.invalidate()
            dataTimer = nil
        }
    }
    
    /// Calculate distance base on LocationMangager.lastLocation and LocationMangager.currentLocation
    static func calculateDistance()
    {

        if (DataCollector.lastLocation == nil)
        {
            DataCollector.lastLocation = LocationMangager.lastLocation
            return
        }

        
        let currentLocation = LocationMangager.currentLocation
        let currentTimeStamp = NSDate().timeIntervalSince1970
        
        let distance = currentLocation.distance(from: lastLocation!)
        DataCollector.lastLocation = currentLocation
        
        
        DataCollector.currentDistance += distance * 0.000621371192
        if distance == 0
        {
            DataCollector.currentSpeed = 0
            
        }
        else
        {
            DataCollector.currentSpeed = abs(currentLocation.speed*2.23693629)
        }
        
        LocationMangager.lastLocation = currentLocation
        LocationMangager.lastTimeInterval = currentTimeStamp
        DataCollector.drivingTime = (currentTimeStamp - DataCollector.startedTime)
        
        if DataCollector.drivingTime > 0.0 {
            DataCollector.averageSpeed = round (DataCollector.currentDistance * 3600 / DataCollector.drivingTime )
        }
        
        DataCollector.currentDistance = round(DataCollector.currentDistance * 100) / 100
        DataCollector.currentSpeed = round(DataCollector.currentSpeed)
        DataCollector.drivingTime = round(DataCollector.drivingTime * 100) / 100
        
    }

}

