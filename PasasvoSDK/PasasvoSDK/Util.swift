//
//  Util.swift
//  SensorsLog
//
//  Created by Unitec on 12/12/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation

class Util{
    
    static var timeZone: String = getTimeZone()
    static var dateFormatter = getDateFormatter()
    
    private static func getTimeZone() -> String {
        let timeZoneOffset = NSTimeZone.system.secondsFromGMT()/3600
        return String(format: "%02d:00", Int(timeZoneOffset))
    }
    
    private static func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd¥'T¥'HH:mm:ss.SSS+Z"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS+\(Util.timeZone)"
        return dateFormatter
    }
    
    
    static func getTodayString(timestamp: Double = NSDate().timeIntervalSince1970) -> String{
        let d = Date(timeIntervalSince1970: timestamp)
        return dateFormatter.string(from: d)
    }

    static func toUUIDString(value: String) -> String {
        let data = value.data(using: .utf8)
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: (data?.count)!)
        let stream = OutputStream(toBuffer: buffer, capacity: (data?.count)!)
        
        stream.open()
        data?.withUnsafeBytes({ (p: UnsafePointer<UInt8>) -> Void in
            stream.write(p, maxLength: (data?.count)!)
        })

        
        stream.close()

        
        return NSUUID.init(uuidBytes: buffer).uuidString
        
    }

    
}


open class Log
{
    //    public static let log = SwiftyBeaver.self
    //    static func getLogInstance() -> SwiftyBeaver.Type
    //    {
    //        let console = ConsoleDestination()  // log to Xcode Console
    //        let file = FileDestination()
    //        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
    //        let path = dir.appendingPathComponent("Log.log")
    //            file.logFileURL = path
    //        }
    //        Log.log.addDestination(file)
    //        Log.log.addDestination(console)
    //        return Log.log
    //
    //    }
    public static var level: Int = LogLevel.Debug.rawValue
    
    static private var logFilePath = getLogFilePath()
    
    static var instance: Log?
    
    public static func getInstance() -> Log{
        if instance != nil {
            return instance!
        }
        
        instance = Log()
        
        return instance!
    }
    
    
    static private func getLogFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy'.log'"
        let fileName = formatter.string(from: Date())
        //let filePath = NSHomeDirectory() + "/Documents/" + fileName
        let filePath = (documentsDirectory as NSString).appendingPathComponent(fileName)
        return filePath
    
    }
    
    
    static public func toJsonString(dic: [String:Any]?) -> String{
        if dic == nil{
            return "nil"
        }
        
        let max = 500
        
        
     
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dic!,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            
            if (theJSONText?.lengthOfBytes(using: .utf8))! > max{
                return (theJSONText?.substring(to: (theJSONText?.index((theJSONText?.startIndex)!, offsetBy: max ))!))!
            }else{
                return theJSONText!
            }

        }
        
        return "nil"
    }
    
    static public func toQueryString(dic: [String:Any]) -> String{
        var components: [(String, String)] = []
        
        for key in dic.keys.sorted(by: <) {
            let value = dic[key]!
            components += getComponents(fromKey: key, value: value)
        }
        return components.map { "\($0)=\($1)" }.joined(separator: "&")
    }
    
    private static func getComponents(fromKey key: String, value: Any) -> [(String, String)] {
        var components: [(String, String)] = []
        
        if let dictionary = value as? [String: Any] {
            for (nestedKey, value) in dictionary {
                components += getComponents(fromKey: "\(key)[\(nestedKey)]", value: value)
            }
        } else if let array = value as? [Any] {
            for value in array {
                components += getComponents(fromKey: "\(key)[]", value: value)
            }
        } else if let value = value as? NSNumber {
            components.append((key, "\(value)"))
        } else if let bool = value as? Bool {
            components.append((key, (bool ? "1" : "0")))
        } else {
            components.append((key, "\(value)"))
        }
        
        return components
    }
    
    
    static func log(o: Any, level: LogLevel){
        if level.rawValue & Log.level == 0{
            return
        }
        
        var levelText = ""
        switch level {
        case LogLevel.Debug:
            levelText = "[DEBUG]"
        case LogLevel.Info:
            levelText = "[INFO]"
        case LogLevel.Warning:
            levelText = "[WARNING]"
        case LogLevel.Error:
            levelText = "[ERROR]"
        }
        
        // Log to file
        let content = "\(Date()): \(levelText) \(String(describing: o))"
        
        print(content)
        
        
        writeToFile(content: content, filePath: logFilePath)
    }
    
    
    static func writeToFile(content: String, filePath: String) {
        
        let contentToAppend = content + "\n"
        
        
        //Check if file exists
        if let fileHandle = FileHandle(forWritingAtPath: filePath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
        }
        else {
            //Create new file
            do {
                try contentToAppend.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                print("Error creating \(filePath)")
            }
        }
    }
    
    static public func debug(o: Any) {

        log(o: o, level: LogLevel.Debug)
    }
    
    static public func info(o: Any) {
        log(o: o, level: LogLevel.Info)
    }
    
    
    static public func error(o: Any) {
        log(o: o, level: LogLevel.Error)
    }
    
    static public func warning(o: Any) {
        log(o: o, level: LogLevel.Warning)
    }
    
    
    public static func debugPrint(_ s:String) {
        NSLog(s)
        return
        
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: Date())
        let fileName = "\(dateString).log"
        let logFilePath = (documentsDirectory as NSString).appendingPathComponent(fileName)
        var dump = ""
        if FileManager.default.fileExists(atPath: logFilePath) {
            dump =  try! String(contentsOfFile: logFilePath, encoding: String.Encoding.utf8)
        }
        // Write to the file
        NSLog("\(dump)\n\(Date()):\(s)")
        
        do {
            
            try  "\(dump)\n\(Date()):\(s)".write(toFile: logFilePath, atomically: true, encoding: String.Encoding.utf8)
            
        } catch let error as NSError {
            print("Failed writing to log file: \(logFilePath), Error: " + error.localizedDescription)
        }
    }
}
