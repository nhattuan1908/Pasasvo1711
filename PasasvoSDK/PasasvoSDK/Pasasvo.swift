//
//  Pasasvo.swift
//  Pasasvo.swift
//
//  Created by Unitec on 12/9/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation

open class Pasasvo {
    
    static private var dataCollecting = false
    
    static public func startDataCollect(completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?) -> (Void)) -> Void{
        
        if dataCollecting {
            // Do nothing if not yet stop
            return
        }
        
        // Get driving data first
        let param = DrivingData()
        param.drivingStatus = 1
        param.memo = PasasvoConfig.memo
        param.drivingStartDatetime = Util.getTodayString()
        param.measurementDatetime = param.drivingStartDatetime
        ServerGetter.hashmapDangerous.removeAll()
        
        
        _ = ServerSender.sendDriving(drivingData: param, noBlock: true){ (code, messaage) in
            if code == 200 || code == 201 {
                
                // Reset data base luon
                db?.resetDatabase()
                dataCollecting = true
                
                if PasasvoConfig.obdOption != CanOBD2Mode.None {
                    DataCollector.startOBDCollector(interval: PasasvoConfig.intervalCan, dummy: PasasvoConfig.obdOption == CanOBD2Mode.Dump)
                }

                // GPS
                DataCollector.startGPSLocation(interval: PasasvoConfig.intervalCalcSpeed)
                DataCollector.startSendDrivingTimer(interval: PasasvoConfig.intervalDrivingData)
                DataCollector.startGetDangerousTimer(interval: PasasvoConfig.intervalServerPolling)
                DataCollector.startSendDataTimer(interval: Int(PasasvoConfig.intervalDataUpload))
                
                

                
                // OK fire event for app
                completeHanlder(200, nil)

                
                // Do it at last
                // Khong chon sensor khoi start luon
                if (PasasvoConfig.sensorOption.count != 0 ){
                    DataCollector.startSensorCollector(timeInterval: PasasvoConfig.intervalSensor, sensorMode: PasasvoConfig.sensorOption)
                }
                
                
            }else{
                completeHanlder(code, messaage)
            }
        }

    }

    static public func stopDataCollect(completeHanlder: @escaping (_ responseCode: Int, _ errorMessage: String?) -> (Void)) -> Void {
        
        // Stop some thing first
        DataCollector.stopSendDataTimer()
        DataCollector.stopGPSLocation()
        DataCollector.stopOBDCollector()
        DataCollector.stopSensorCollector()
        DataCollector.stopSendDrivingTimer()
        DataCollector.stopGetDangerousTimer()
        
        dataCollecting = false
        
        let param = DrivingData()
        param.drivingStatus = 3
        param.memo = PasasvoConfig.memo
        param.drivingEndDatetime = Util.getTodayString()
        param.measurementDatetime = param.drivingEndDatetime
        param.drivingId = PasasvoConfig.drivingId
        param.averageSpeed = Int32(DataCollector.averageSpeed)
        param.drivingTime = Int32(DataCollector.drivingTime)
        param.drivingDistance = Int32 (DataCollector.currentDistance)
        
        
        // Get driving data
        _ = ServerSender.sendDriving(drivingData: param, noBlock: true){ (code, message) in
            
            completeHanlder(code, message)
        }

    }
    
    static public func deleteSensorData(){
        if let maxId = db?.maxSensorDataId(){
            db?.deleteSensor(toId: Int(maxId))

        }
    }
    
    static public func hasSensorData()->Bool {
        if let count = db?.countSensorData(){
            return (count > 0)
        }
        return false
        
    }
    
    
    static public func deleteCanData(){
        if let maxId = db?.maxObdDataId(){
            db?.deleteSensor(toId: Int(maxId))
            
        }
    }
    
    static public func hasCanData()->Bool {
        if let count = db?.countObdData(){
            return (count > 0)
        }
        return false
        
    }
    
    static public func isDataCollecting() -> Bool {
        return dataCollecting
    }
    
    static public func getCurrentDistance()->Double{
        return DataCollector.currentDistance
    }
    
    static public func getCurrentSpeed()->Double{
        return DataCollector.currentSpeed
    }
    
    static public func getAvegareSpeed()->Double{
        return DataCollector.averageSpeed
    }
    
    
    static public func getDrivingTime()->Double{
        return DataCollector.drivingTime
    }
}
