//
//  Descriptor.swift
//  OBD2Swift
//
//  Created by Max Vitruk on 5/25/17.
//  Copyright © 2017 Lemberg. All rights reserved.
//

import Foundation

public class Mode01Descriptor : DescriptorProtocol {
    public var response : Response
    var descriptor : SensorDescriptor
    
    required public init(describe response : Response) {
        self.response = response
        let pid = response.pid
        self.mode = response.mode
        
        guard pid >= 0x0 && pid <= 0x4E else {
            assertionFailure("Unsuported pid group")
            self.descriptor = SensorDescriptorTable[0]
            return
        }
        self.descriptor = SensorDescriptorTable[Int(pid)]
    }
    
    public var mode : Mode
    public var pid : UInt8 {
        return response.pid
    }
    
    public var valueMetrics : Float? {
        guard let data = response.data else {return nil}
        guard !isAsciiEncoded else {return nil}
        guard let exec = descriptor.calcFunction else {return nil}
        
        return exec(data)
    }
    
    public var valueImperial : Float? {
        guard let value = valueMetrics else {return nil}
        guard let exec = descriptor.convertFunction else {return nil}
        return exec(value)
    }
    
    public var unitsImperial : Float? {
        guard let value = valueMetrics else {return nil}
        guard let exec = descriptor.convertFunction else {return nil}
        return exec(value)
    }
    
    public var asciValue : String? {
        guard let data = response.data else {return nil}
        guard isAsciiEncoded else {return nil}
        return calculateStringForData(data: data)
    }
    
    public var description : String {
        return descriptor.description
    }
    
    public var shortDescription :String {
        return descriptor.shortDescription
    }
    
    public var unitImperial : String {
        return descriptor.imperialUnit
    }
    
    public var unitMetric : String {
        return descriptor.metricUnit
    }
    
    public var isAsciiEncoded : Bool {
        return IS_ALPHA_VALUE(pid: pid)
    }
    
    public func stringRepresentation(metric : Bool, rounded : Bool = false) -> String {
        if isAsciiEncoded {
            return asciValue ?? ""
        }else{
            guard let value = self.value(metric: metric) else {return ""}
            let units = self.units(metric: metric)
            
            if rounded {
                return "\(Int.init(value)) \(units)"
            }else{
                return "\(value) \(units)"
            }
        }
    }
    
    
    public func units(metric : Bool) -> String {
        return metric ? descriptor.metricUnit : descriptor.imperialUnit
    }
    
    public func value(metric : Bool) -> Float? {
        return metric ? valueMetrics : valueImperial
    }
    
    public func minValue(metric : Bool) -> Int {
        if isAsciiEncoded {
            return Int.min
        }
        return metric ? descriptor.minMetricValue : descriptor.minImperialValue
    }
    
    public func maxValue(metric : Bool) -> Int {
        if isAsciiEncoded {
            return Int.max
        }
        
        return metric ? descriptor.maxMetricValue : descriptor.maxImperialValue
    }
    
    //TODO: - Add multidescriptor to descriptor tables
    //    public func isMultiValue() -> Bool {
    //        return IS_MULTI_VALUE_SENSOR(pid: pid)
    //    }
    
    //MARK: - String Calculation Methods
    
    private func calculateStringForData(data : Data) -> String? {
        switch pid {
        case 0x01:
            return calculateMonitors(data)
        case 0x02:
            return calculateFreezeDTC(data)
        case 0x03:
            return calculateFuelSystemStatus(data)
        case 0x12:
            return calculateSecondaryAirStatus(data)
        case 0x13:
            return calculateOxygenSensorsPresent(data)
        case 0x14:
            return calculateOxygenSensorsData(data)
        case 0x15:
            return calculateOxygenSensorsData2(data)
        case 0x16:
            return calculateOxygenSensorsData3(data)
        case 0x17:
            return calculateOxygenSensorsData4(data)
        case 0x18:
            return calculateOxygenSensorsData5(data)
        case 0x19:
            return calculateOxygenSensorsData6(data)
        case 0x1A:
            return calculateOxygenSensorsData7(data)
        case 0x1B:
            return calculateOxygenSensorsData8(data)
        case 0x1C:
            return calculateDesignRequirements(data)
        case 0x1D:
            return calculateOxygenSensorsPresentB(data)
        case 0x1E:
            return calculateAuxiliaryInputStatus(data)
        default:
            return nil
        }
    }
    private func calculateMonitors(_ data : Data) -> String? {
        var returnString:String = ""
        var dataA = data[0]
        var dataB = data[1]
        if(dataA & 0x80 != 0)
        {
            returnString = "MIL = On"
        }
        else
        {
            returnString = "MIL = Off"
        }
        
        var dtcNumber = String(dataA & 0x7F)
        returnString = "\(returnString); DTCs = \(dtcNumber)"
        
        if(dataB & 0x04 != 0)
        {
            returnString = "\(returnString); Compression ignition monitors supported"
        }
        else
        {
            returnString = "\(returnString); Spark ignition monitors supported"
        }
        return returnString
    }
    
    private func calculateFreezeDTC(_ data : Data) -> String? {
        var returnString: String = ""
        
        returnString = data.map{String(format: "%02X", $0)}.joined()
        return returnString
    }
    
    private func calculateAuxiliaryInputStatus(_ data : Data) -> String? {
        var dataA = data[0]
        dataA = dataA & ~0x7F // only bit 0 is valid
        
        if dataA & 0x01 != 0 {
            return "Activite"
        }else if dataA & 0x02 != 0 {
            return "Inactivite"
        }else {
            return nil
        }
    }
    
    private func calculateDesignRequirements(_ data : Data) -> String? {
        var returnString : String?
        let dataA = data[0]
        
        switch dataA {
        case 0x01:
            returnString	= "OBD-II"
            break
        case 0x02:
            returnString	= "OBD"
            break
        case 0x03:
            returnString	= "OBD and OBD-II"
            break
        case 0x04:
            returnString	= "OBD-I"
            break
        case 0x05:
            returnString	= "Not OBD compliant"
            break
        case 0x06:
            returnString	= "EOBD"
            break
        case 0x07:
            returnString	= "EOBD and OBD-II"
            break
        case 0x08:
            returnString	= "EOBD and OBD"
            break
        case 0x09:
            returnString	= "EOBD, OBD and OBD II"
            break
        case 0x0A:
            returnString	= "JOBD"
            break
        case 0x0B:
            returnString	= "JOBD and OBD II"
            break
        case 0x0C:
            returnString	= "JOBD and EOBD"
            break
        case 0x0D:
            returnString	= "JOBD, EOBD, and OBD II"
            break
        case 0x11:
            returnString = "Engine Manufacturer Diagnostics (EMD)"
            break
        case 0x12:
            returnString = "Engine Manufacturer Diagnostics Enhanced (EMD+)"
            break
        case 0x13:
            returnString = "Heavy Duty On-Board Diagnostics (Child/Partial) (HD OBD-C)"
            break
        case 0x14:
            returnString = "Heavy Duty On-Board Diagnostics (HD OBD)"
            break
        case 0x15:
            returnString = "World Wide Harmonized OBD (WWH OBD)"
            break
        case 0x17:
            returnString = "Heavy Duty Euro OBD Stage I without NOx control (HD EOBD-I)"
            break
        case 0x18:
            returnString = "Heavy Duty Euro OBD Stage I with NOx control (HD EOBD-I N)"
            break
        case 0x19:
            returnString = "Heavy Duty Euro OBD Stage II without NOx control (HD EOBD-II)"
            break
        case 0x1A:
            returnString = "Heavy Duty Euro OBD Stage II with NOx control (HD EOBD-II N)"
            break
        case 0x1C:
            returnString = "Brazil OBD Phase 1 (OBDBr-1)"
            break
        case 0x1D:
            returnString = "Brazil OBD Phase 2 (OBDBr-2)"
            break
        case 0x1E:
            returnString = "Korean OBD (KOBD)"
            break
        case 0x1F:
            returnString = "India OBD I (IOBD I)"
            break
        case 0x20:
            returnString = "India OBD II (IOBD II)"
            break
        case 0x21:
            returnString = "Heavy Duty Euro OBD Stage VI (HD EOBD-IV)"
            break
        case 0xFB:
            returnString = "Not available for assignment"
            break
        case 0xFC:
            returnString = "Not available for assignment"
            break
        case 0xFD:
            returnString = "Not available for assignment"
            break
        case 0xFE:
            returnString = "Not available for assignment"
            break
        case 0xFF:
            returnString = "Not available for assignment"
            break
        default:
            returnString	= "Reserved"
            break
        }
        
        return returnString
    }
    
    private func calculateOxygenSensorsPresent(_ data : Data) -> String {
        var returnString : String = ""
        
        returnString = data.map{String(format: "%02X", $0)}.joined()
        
        return returnString
    }
    
    private func calculateOxygenSensorsPresentB(_ data : Data) -> String {
        var returnString : String = ""
        returnString = data.map{String(format: "%02X", $0)}.joined()
        return returnString
    }
    
    private func calculateOxygenSensorsData(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData2(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData3(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData4(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData5(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData6(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData7(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    private func calculateOxygenSensorsData8(_ data : Data) -> String {
        var returnString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        var numberA = String(Double(dataA) / 200)
        var numberB = String((Double(dataB) / 1.28) - 100)
        returnString = "\(numberA); \(numberB)"
        return returnString
    }
    
    private func calculateFuelSystemStatus(_ data : Data) -> String {
        var rvString : String = ""
        let dataA = data[0]
        let dataB = data[1]
        
        switch dataA {
        case 0x01:
            rvString		= "Open loop due to insufficient engine temperature"
            break
        case 0x02:
            rvString		= "Closed loop, using oxygen sensor feedback to determine fuel mix"
            break
        case 0x04:
            rvString		= "Open loop due to engine load OR fuel cut due to deceleration"
            break;
        case 0x08:
            rvString		= "Open loop due to system failure"
            break
        case 0x10:
            rvString		= "Closed loop, using at least one oxygen sensor but there is a fault in the feedback system"
            break
        default:
            rvString    = "Invalid"
            break
        }
        
        switch dataB {
        case 0x01:
            rvString		= "\(rvString); Open loop due to insufficient engine temperature"
            break
        case 0x02:
            rvString		= "\(rvString); Closed loop, using oxygen sensor feedback to determine fuel mix"
            break
        case 0x04:
            rvString		= "\(rvString); Open loop due to engine load OR fuel cut due to deceleration"
            break;
        case 0x08:
            rvString		= "\(rvString); Open loop due to system failure"
            break
        case 0x10:
            rvString		= "\(rvString); Closed loop, using at least one oxygen sensor but there is a fault in the feedback system"
            break
        default:
            rvString    = "Invalid"
            break
        }
        
        return rvString
    }
    
    private func calculateSecondaryAirStatus(_ data : Data) -> String {
        var rvString : String = ""
        let dataA = data[0]
        
        switch dataA {
        case 0x01:
            rvString		= "Upstream"
            break
        case 0x02:
            rvString		= "Downstream of catalytic converter"
            break
        case 0x04:
            rvString		= "From the outside atmosphere or off"
            break
        case 0x08:
            rvString        = "Pump commanded on for diagnostics"
        default:
            rvString        = "Invalid"
            break
        }
        
        return rvString
    }
}
