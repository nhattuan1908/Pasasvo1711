//
//  MeasureTableViewCell.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class MeasureTableViewCell: UITableViewCell{
    
    @IBOutlet weak var textViewMeasure: UITextView!
    
    @IBOutlet weak var datetime: UILabel!
    @IBOutlet weak var action: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }


}
