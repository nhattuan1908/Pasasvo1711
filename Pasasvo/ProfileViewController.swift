//
//  NewLogsViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/16/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import PasasvoSDK

class ProfileViewController : FormViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setButtonBack()
        self.navigationItem.title  = NSLocalizedString("API Test", comment: "")
        var rules = RuleSet<String>()
        rules.add(rule: RuleRequired())
        //self.save()
        
        form +++
            Section("")
            <<< SegmentedRow<String>("Segment")
            {
                $0.options = ["Profile", "Cars", "Devices", "Users"]
                $0.value = "Profile"
            }
    }
    
//    func saveSDK()
//    {
//        
//        let error = form.validate()
//        if (error.count > 0)
//        {
//            self.showToast(message: NSLocalizedString("enter_all_settings_required", comment: ""), textColor: UIColor.white)
//            return
//        }
//        
//        let userIdRow: TextRow? = form.rowBy(tag: "UserId")
//        let carIdRow: TextRow? = form.rowBy(tag: "CarId")
//        let deviceIdRow: TextRow? = form.rowBy(tag: "DeviceId")
//        let drivingIdRow: TextRow? = form.rowBy(tag: "DrivingId")
//        let loginIdRow: TextRow? = form.rowBy(tag: "LoginId")
//        let passwordRow: TextRow? = form.rowBy(tag: "Password")
//        
//        UserDefaults.standard.setUserId(value: userIdRow?.value)
//        UserDefaults.standard.setCarId(value: carIdRow?.value)
//        UserDefaults.standard.setDeviceId(value: deviceIdRow?.value)
//        UserDefaults.standard.setDrivingId(value: drivingIdRow?.value)
//        UserDefaults.standard.setLoginId(value: loginIdRow?.value)
//        UserDefaults.standard.setPassword(value: passwordRow?.value)
//        
//        PasasvoConfig.userId = UserDefaults.standard.getUserId()
//        PasasvoConfig.carId = UserDefaults.standard.getCarId()
//        PasasvoConfig.deviceId = UserDefaults.standard.getDeviceId()
//        PasasvoConfig.drivingId = UserDefaults.standard.getDrivingId()
//        
//    }
    
    func setButtonBack() {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(SDKTestViewController.backSettings), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
    }
    
    func backSettings()
    {
        self.performSegue(withIdentifier: "show_back_to_setting_view", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
