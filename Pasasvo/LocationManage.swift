//
//  LocationManage.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import CoreMotion
import CoreLocation

import PasasvoSDK


class LocationMangager: NSObject,CLLocationManagerDelegate {

    public static var currentLocation = CLLocation()
    public static var currentTimeInterval: Double?
    public static var lastLocation : CLLocation?
    public static var lastTimeInterval: Double?
    public static var gpsData = [SensorData]()
    
    public static var getLastLocation = true

    var localManagement = CLLocationManager()
    
    private override init() {
        super.init()
        self.localManagement.requestWhenInUseAuthorization()
        self.localManagement.allowsBackgroundLocationUpdates = true
        self.localManagement.pausesLocationUpdatesAutomatically = false
        self.localManagement.delegate = self
    }

    static let locationManagerInstance = LocationMangager()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        LocationMangager.currentLocation =  manager.location!
        LocationMangager.currentTimeInterval = NSDate().timeIntervalSince1970
        if (LocationMangager.getLastLocation)
        {
            LocationMangager.lastTimeInterval = NSDate().timeIntervalSince1970
            LocationMangager.lastLocation = manager.location!
            LocationMangager.getLastLocation = false
        }
    }
    
    func startLocation()
    {
        localManagement.startUpdatingLocation()
    }
    
    func stopLocation()
    {
        self.localManagement.stopUpdatingLocation()

    }
    

}
