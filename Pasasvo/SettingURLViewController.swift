//
//  SettingURLViewController.swift
//  Pasasvo
//
//  Created by Tran Anh on 2017/12/22.
//  Copyright © 2017 unitec-MAC. All rights reserved.
//

import UIKit
import Eureka
import PasasvoSDK

class SettingURLViewController: FormViewController {
    
    var cancelBtn: UIBarButtonItem!
    var saveBtn: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        cancelBtn = UIBarButtonItem(title: NSLocalizedString("cancel", comment: ""), style: .done, target: self, action: #selector(self.cancelSetting))
        self.navigationItem.leftBarButtonItem = cancelBtn
        saveBtn = UIBarButtonItem(title: NSLocalizedString("save", comment: ""), style: .done, target: self, action: #selector(self.saveSetting))
        self.navigationItem.rightBarButtonItem = saveBtn
        
        form +++ Section("")
            <<< TextRow("service_domain") {
                $0.title = NSLocalizedString("service_domain", comment: "")
                $0.value = PasasvoConfig.serverUrl
                $0.placeholder = "192.168.0.99:8080"
                }
                .cellUpdate { cell, row in
                    cell.textField.keyboardType = .URL
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                }
        // Do any additional setup after loading the view.
    }
    
    func saveSetting()
    {
        let serviceRow: TextRow = form.rowBy(tag: "service_domain")!
        UserDefaults.standard.setServerUrl(value: serviceRow.value!)
        PasasvoConfig.serverUrl = serviceRow.value!
        self.performSegue(withIdentifier: "back_to_login_view", sender: self)
    }
    func cancelSetting()
    {
        self.performSegue(withIdentifier: "back_to_login_view", sender: self)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
