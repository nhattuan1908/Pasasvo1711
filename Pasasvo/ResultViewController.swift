//
//  ResultViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/15/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import PasasvoSDK

class ResultViewController: UIViewController {
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var DrivingDiagnosis: UILabel!
    @IBOutlet weak var DrivingDiagnosisValue: UILabel!
    @IBOutlet weak var EcoDriving: UILabel!
    @IBOutlet weak var EcoDrivingValue: UILabel!
    @IBOutlet weak var MileageEarned: UILabel!
    @IBOutlet weak var MileageEarnedValue: UILabel!
    @IBOutlet weak var TotalMiles: UILabel!
    @IBOutlet weak var TotalMilesValue: UILabel!
    @IBOutlet weak var AverageSpeed: UILabel!
    @IBOutlet weak var AverageSpeedValue: UILabel!
    @IBOutlet weak var Address: UILabel!
    
    
    @IBOutlet weak var tableView: UITableView!
    var detailLogs: NSArray?
    
    
    @IBAction func unwindToResultView(segue:UIStoryboardSegue) { }
    
    
    @IBAction func LogsBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "show_log_segue", sender: self)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        setViewLoad()
        setButtonBack()
        setIconCar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.detailLogs = Helper.getLogsDatas(groupBy: [LogTableFieldName.dangerousType])
        self.tableView.reloadData()
    }
    
    
    func setViewLoad()
    {
        
        if (flag == true)
        {
            self.showActivityIndicatory()
        _ = ServerGetter.getSafeDriving(delay: 15) { (code, message, result) in
            
            self.stopActivityIndicatory()
            if code == 200 || code == 201 {
                self.DrivingDiagnosisValue.text = AppConfig.ResultType[(result?.safeDrivingLevel)!]
                UserDefaults.standard.setDrivingDiagnosisValue(value: AppConfig.ResultType[(result?.safeDrivingLevel)!]!)
                self.dateTime.text = result?.measurementDatetime
                UserDefaults.standard.setDateTime(value: (result?.measurementDatetime)!)
                let total:Int32! = result?.totalDrivingDistance
                self.TotalMilesValue.text = String(total) + " mile"
                UserDefaults.standard.setTotalMilesValue(value: String(total))
            }
            else
            {
                if (message?.range(of: "The Internet connection appears to be offline") != nil)
                {
                    self.showAlert(title: "Error", message: "\(code):  Network error")
                    return
                }
                else if (message?.range(of: "The request timed out.") != nil)
                {
                    self.showAlert(title: "Error", message: "\(code):  Server time out")
                    return
                }
                else
                {
                    self.showAlert(title: "Error", message: "\(code) \(String(describing: message))")
                    return
                }
            }
        }
    }
        if (flag == false)
        {
            self.DrivingDiagnosisValue.text = UserDefaults.standard.getDrivingDiagnosisValue()
            self.dateTime.text = UserDefaults.standard.getDateTime()
            self.TotalMilesValue.text = UserDefaults.standard.getTotalMilesValue()
        }

        if (self.DrivingDiagnosisValue.text == nil || self.dateTime.text == "" || self.TotalMilesValue.text == nil)
        {
           
            self.DrivingDiagnosisValue.text = " "
            self.dateTime.text = " "
            self.TotalMilesValue.text = " "
        }
        self.DrivingDiagnosis.text = NSLocalizedString("DrivingDiagnosis", comment: "")
        self.DrivingDiagnosisValue.textColor = UIColor.red
        self.EcoDriving.text = NSLocalizedString("EcoDriving", comment: "")
        self.EcoDrivingValue.textColor = UIColor.red
        self.MileageEarned.text = NSLocalizedString("MileageEarned", comment: "")
        self.MileageEarnedValue.text = String(DataCollector.currentDistance) + " mile"
        self.TotalMiles.text = NSLocalizedString("TotalMiles", comment: "")
        self.AverageSpeedValue.text = String(Int(DataCollector.averageSpeed)) + " mile/h"
        self.AverageSpeed.text = NSLocalizedString("AverageSpeed", comment: "")
        self.Address.text = NSLocalizedString("Address", comment: "")
        self.Address.isHidden = true
    }
    
    func callMethod()
    {
        self.performSegue(withIdentifier: "back_to_show_main_view", sender: self)
    }
    
    func setButtonBack() {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(ResultViewController.callMethod), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    
    func setIconCar() {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "car"), for: UIControlState.normal)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
}


extension ResultViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailLogTableViewCell", for: indexPath) as! DetailLogTableViewCell
        let detailDict = detailLogs?[indexPath.row] as? [String: Int]
        if(detailDict?["dangerousType"] == 99)
        {
            cell.dangerousCount.isHidden = true
            cell.dangerousTitle.isHidden = true
        }
        else
        {
            cell.dangerousCount.text = String(describing: (detailDict?["count"])!) // detailLogs[indexPath.row]["count"]
            cell.dangerousTitle.text = AppConfig.DangerousType[(detailDict?["dangerousType"])!]
            //return cell
        }
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(UserDefaults.standard.getTestModeSettings())
        {
            return detailLogs!.count
        }
        else
        {
            return 0
        }
    }
    
}
