//
//  SearchOptionTableViewCell.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/15.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class SearchOptionTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        for var view in self.subviews
        {
            if view.isKind(of: UIScrollView.self)
            {
                (view as! UIScrollView).delaysContentTouches = false
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
