//
//  MachineCheckViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/15/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class MachineCheckViewController: UIViewController {
    @IBOutlet weak var DateTime: UILabel!
    @IBOutlet weak var Warning: UILabel!
    @IBOutlet weak var YesBtn: UIButton!
    @IBOutlet weak var NoBtn: UIButton!
    @IBOutlet weak var CancelBtn: UIButton!
    @IBAction func BacktoLogs(_ sender: UIButton) {
        self.performSegue(withIdentifier: "show_back_to_logs_view", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        DateTime.text = "2017/11/26 11:11:11"
        Warning.textColor = ConstantColors.UserCheckBackground
        Warning.text = NSLocalizedString("Warning", comment: "")
        setButtonConfirm(button: YesBtn,tittle: "YES")
        setButtonConfirm(button: NoBtn,tittle: "NO")
        setButtonCancel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setButtonConfirm(button: UIButton, tittle: String)
    {
        button.layer.cornerRadius = 7
        button.backgroundColor = ConstantColors.UserConfirmBackgroud
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle(tittle, for: .normal)
    }
    
    func setButtonCancel()
    {
        CancelBtn.layer.cornerRadius = CancelBtn.frame.height/2
        CancelBtn.backgroundColor = ConstantColors.CommonButtonBackGround
        CancelBtn.setTitleColor(UIColor.white, for: .normal)
        CancelBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
    }
}
