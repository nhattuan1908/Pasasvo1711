//
//  MenuViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/14/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import PasasvoSDK


class MenuViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var carLogoImg: UIImageView!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var LoginContent: UITextField!
    @IBOutlet weak var PasswordContent: UITextField!
    
    
    var companyTitle: UILabel!
    var appNameTitle: UILabel!
    @IBAction func unwindToLoginView(segue:UIStoryboardSegue) { }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        PasswordContent.isSecureTextEntry = true
        self.PasswordContent.delegate = self;
        self.LoginContent.delegate = self;
        
        // Setting
        PasasvoConfig.carId = UserDefaults.standard.getCarId()
        PasasvoConfig.deviceId = UserDefaults.standard.getDeviceId()
        PasasvoConfig.userId = UserDefaults.standard.getUserId()
        
        PasasvoConfig.intervalSensor = UserDefaults.standard.getSensorDataSettings()
        PasasvoConfig.intervalDrivingData = UserDefaults.standard.getDrivingDataSettings()
        PasasvoConfig.intervalDataUpload = UserDefaults.standard.getUploadIntervalSettings()
        PasasvoConfig.intervalCalcSpeed = UserDefaults.standard.getSpeedDataSettings()
        PasasvoConfig.testMode = UserDefaults.standard.getTestModeSettings()
        PasasvoConfig.memo = UserDefaults.standard.getMemo()
        PasasvoConfig.serverUrl = UserDefaults.standard.getServerUrl()
        PasasvoConfig.obdOption = CanOBD2Mode.getCanModeByName(name: UserDefaults.standard.getOBD2Settings())!
        PasasvoConfig.sensorOption = []
        PasasvoConfig.maxDrivingTime = Double(UserDefaults.standard.getMaxDrivingTime())
        
        if UserDefaults.standard.getGyroEnableSettings(){
            PasasvoConfig.sensorOption.append(Int(SensorType.Gyro.rawValue))
        }
        if UserDefaults.standard.getAccelerationEnableSettings(){
            PasasvoConfig.sensorOption.append(Int(SensorType.Acc.rawValue))
        }
        if UserDefaults.standard.getGpsEnableSettings(){
            PasasvoConfig.sensorOption.append(Int(SensorType.GPS.rawValue))
        }
        PasasvoConfig.intervalServerPolling = UserDefaults.standard.getDangerousDataSettings()
        if(UserDefaults.standard.getLoginId() != "" && UserDefaults.standard.getPassword() != "" && UserDefaults.standard.getUserId() != "")
        {
            self.performSegue(withIdentifier: "show_main_segue", sender: self)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.handleView()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func SignUp(_ sender: UIButton) {
        UserDefaults.standard.setLoginId(value: LoginContent.text)
        UserDefaults.standard.setPassword(value: PasswordContent.text)
        loginTest.loginId = LoginContent.text
        loginTest.password = PasswordContent.text
        //loginTest.userId = "123"
        self.showActivityIndicatory()
        User.login(login: loginTest, callback: { (response, data, error) -> (Void) in
            if(response != 200)
            {
                UserDefaults.standard.setUserId(value: "")
                UserDefaults.standard.setPassword(value: "")
                if (error?.range(of: "The Internet connection appears to be offline") != nil)
                {
                    self.showAlert(title: "Error", message: "\(String(describing: response)):  Network error")
                    return
                }
                else if (error?.range(of: "The request timed out.") != nil)
                {
                    self.showAlert(title: "Error", message: "\(String(describing: response)):  Server time out")
                    return
                }
                else
                {
                    self.showAlert(title: "Error", message: "\(String(describing: response)) \(String(describing: error))")
                    return
                }
            }
            else
            {
                UserDefaults.standard.setUserId(value: data)
            }
            let value = UserDefaults.standard.getUserId()
            for i in 0...UserLocal.userIdList.count - 1
            {
                if(value == UserLocal.userIdList[i])
                {
                    UserDefaults.standard.setCarId(value: UserLocal.carIdList[i])
                    UserDefaults.standard.setDeviceId(value: UserLocal.deviceIdList[i])
                    UserDefaults.standard.setCurrentUserName(value: UserLocal.userNameList[i])
                    PasasvoConfig.setCurrentCar(value: UserLocal.carIdList[i])
                    PasasvoConfig.setCurrentUser(value: UserLocal.userIdList[i])
                    PasasvoConfig.setCurrentDevice(value: UserLocal.deviceIdList[i])
                    
                    self.performSegue(withIdentifier: "show_main_segue", sender: self)
                    return
                }
            }
            self.showAlert(title: "Error", message: "UserId \(value) not found")
            UserDefaults.standard.setUserId(value: "")
            UserDefaults.standard.setPassword(value: "")
            self.stopActivityIndicatory()
        })
        
    }
    
    func setButtonSignup()
    {
        self.signupBtn.layer.cornerRadius = self.signupBtn.frame.height/3
        self.signupBtn.backgroundColor = ConstantColors.CommonButtonBackGround
        self.signupBtn.setTitleColor(UIColor.white, for: .normal)
        self.signupBtn.setTitle(NSLocalizedString("login", comment:""), for: .normal)
        self.signupBtn.titleLabel?.font = ConstantFonts.CommitFont
    }
    
    private func handleView()
    {
        setButtonSignup()
        let viewHeight = self.view.frame.height
        let viewWitdh = self.view.frame.width
        let constantHeight = CGFloat(2210)
        let constantWidth = CGFloat(1240)
        
        let leftSpace = CGFloat(170)
        self.companyTitle = LabelWithAdaptiveTextHeight(frame: CGRect(x: leftSpace*viewWitdh/constantWidth, y: CGFloat(400)*viewHeight/constantHeight, width: 900*viewWitdh/constantWidth, height: CGFloat(150)*viewHeight/constantHeight))
        let font = UIFont(name: "ArialRoundedMTBold", size: CGFloat(10))
        self.companyTitle.text = NSLocalizedString("company_title", comment: "")
        self.companyTitle.font = font
        self.companyTitle.textAlignment = .center
        self.appNameTitle = LabelWithAdaptiveTextHeight(frame: CGRect(x: leftSpace*viewWitdh/constantWidth, y: CGFloat(550)*viewHeight/constantHeight, width: 900*viewWitdh/constantWidth, height: CGFloat(150)*viewHeight/constantHeight))
        self.appNameTitle.text = NSLocalizedString("appname_title", comment: "")
        self.appNameTitle.font = font
        self.appNameTitle.textAlignment = .center
        
        _ = CGFloat(150*viewHeight)/constantHeight
        _ = CGFloat(170*viewWitdh)/constantWidth
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint(item: self.carLogoImg, attribute: .top, relatedBy: .equal, toItem:  self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: CGFloat(620*viewHeight)/constantHeight),
                NSLayoutConstraint(item: self.carLogoImg, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(400*viewWitdh)/constantWidth),

                
                ])
       self.view.addSubview(self.companyTitle)
       self.view.addSubview(self.appNameTitle)
    }
}
