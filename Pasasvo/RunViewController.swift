//
//  RunViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/15/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import PasasvoSDK

var flag:Bool = false

class RunViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var OperationEnd: UIButton!
    @IBOutlet weak var Content: UILabel!
    @IBOutlet weak var Speed: UILabel!
    @IBOutlet weak var Mileage: UILabel!
    @IBOutlet weak var MileageValue: UILabel!
    @IBOutlet weak var SpeedValue: UILabel!
    @IBOutlet weak var EventLog: UILabel!
    @IBOutlet weak var ServerCommand: UILabel!
    @IBOutlet weak var carLogoImg: UIImageView!
    @IBOutlet weak var textView: UITextView!
    
    
    var startTime: Timer!
    var endTime: Timer!
    var getDangerousTimer: Timer!
    @IBAction func StopOperation(_ sender: UIButton) {
        flag = true
        Pasasvo.stopDataCollect { (responseCode, errorMessage) -> (Void) in
            self.stopActivityIndicatory()
            if (responseCode == 201 || responseCode == 200)
            {
                self.performSegue(withIdentifier: "show_result_segue", sender: self)
            }
            else
            {
                if (errorMessage?.range(of: "The Internet connection appears to be offline") != nil)
                {
                    self.showAlert(title: "Error", message: "\(responseCode):  Network error")
                    return
                }
                else if (errorMessage?.range(of: "The request timed out.") != nil)
                {
                    self.showAlert(title: "Error", message: "\(responseCode):  Server time out")
                    return
                }
                else
                {
                    self.showAlert(title: "Error", message: "\(responseCode) \(String(describing: errorMessage))")
                    return
                }

            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         textView.delegate = self
        
        PasasvoConfig.setDistanceCallback(){(s, d, t) in
            self.SpeedValue.text = "\(Int(s)) mile/h"
            self.MileageValue.text = "\(d) mile"
        }
        if(UserDefaults.standard.getRealtimeLogSettings() == true)
        {
            self.textView.isHidden = false
        }
        else
        {
            self.textView.isHidden = true
        }
        
        PasasvoConfig.setSystemCallback { (code, result, error) -> (Void) in
            
        }
        
        PasasvoConfig.setSystemCallback { (code, result, error) -> (Void) in
            if (code == 201)
            {
                let logsSensor = Helper.getTimeString() + " Sensor: " + " \(result)"
                self.setvalue(value: logsSensor)
            }
            if (code == 101)
            {
                if (result == 200 || result == 201)
                {
                    self.performSegue(withIdentifier: "show_result_segue", sender: self)
                    self.showAlert(title: "Notification", message: "Maxtime time out")
                }
                else
                {
                    if (error?.range(of: "The Internet connection appears to be offline") != nil)
                    {
                        self.showAlert(title: "Error", message: "\(code):  Network error")
                        return
                    }
                    else if (error?.range(of: "The request timed out.") != nil)
                    {
                        self.showAlert(title: "Error", message: "\(code):  Server time out")
                        return
                    }
                    else
                    {
                        self.showAlert(title: "Error", message: "\(code) \(String(describing: error))")
                        return
                    }
                }
            }
            
        }
        
        PasasvoConfig.setDangerousCallback(){ (code, result, error) in
            self.Content.isHidden = false
            self.Content.textColor = UIColor.red
            if (code == 200 || code == 201)
            {
                if(result != nil)
                {
                    self.Content.text = AppConfig.DangerousType[Int((result?.dangerousDrivingType!)!)]
                    let logsData = Helper.getTimeString() + " Dangerous: " + "\(String(describing: code))"
                    self.setvalue(value: logsData)

                }
                else
                {
                    let logsData = Helper.getTimeString() + " Dangerous: " + ": No received data"
                    self.setvalue(value: logsData)
                }
            }
            else
            {
                self.Content.text = ""
                let errorData = Helper.getTimeString() + " Dangerous: " + ": \(String(describing: code))"
                self.setvalue(value: errorData)
            }
            self.getDangerousTimer = Timer.after(3, {
                self.Content.text = ""
            })
        }
        
        Content.isHidden = true
        Speed.text = NSLocalizedString("Speed", comment: "")
        Mileage.text = NSLocalizedString("Mileage", comment: "")
        SpeedValue.text = "0 mile/h"
        MileageValue.text = "0 mile"
        EventLog.text = NSLocalizedString("Content", comment: "")
        EventLog.isHidden = true
        setButton()
        ServerCommand.isHidden = true
        
        handleView()
        
    }
    func scroll()
    {
        let range = NSMakeRange(self.textView.text.characters.count - 1, 0)
        self.textView.scrollRangeToVisible(range)
    }
    func setvalue(value: String){
        self.textView.text = self.textView.text + "\n" + value
        self.scroll()
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            self.textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func handleView()
    {
        let viewHeight = self.view.frame.height
        let viewWitdh = self.view.frame.width
        let constantHeight = CGFloat(2210)
        let constantWidth = CGFloat(1240)
        
        self.OperationEnd.titleLabel?.font = ConstantFonts.CommitFont
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint(item: self.carLogoImg, attribute: .top, relatedBy: .equal, toItem:  self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: CGFloat(50*viewHeight)/constantHeight),
                NSLayoutConstraint(item: self.carLogoImg, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(450*viewWitdh)/constantWidth),
                NSLayoutConstraint(item: self.OperationEnd, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(100*viewHeight)/constantHeight) ,
                NSLayoutConstraint(item: self.OperationEnd, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(170*viewWitdh)/constantWidth),
                ])
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //getDangerous()
    }
    
    func setButton()
    {
        OperationEnd.layer.cornerRadius = OperationEnd.frame.height/2
        OperationEnd.backgroundColor = ConstantColors.CommonButtonBackGround
        OperationEnd.setTitleColor(UIColor.white, for: .normal)
        OperationEnd.setTitle(NSLocalizedString("OperationEnd", comment: ""), for: .normal)
    }
    
    func setServerCommand()
    {
        Content.isHidden = true
        Speed.isHidden = true
        SpeedValue.isHidden = true
        Mileage.isHidden = true
        MileageValue.isHidden = true
        EventLog.isHidden = true
        ServerCommand.isHidden = false
        ServerCommand.textColor = UIColor.blue
        ServerCommand.text = NSLocalizedString("ServerCommand", comment: "")
    }
    func StopTimer()
    {
        Content.isHidden = false
        Speed.isHidden = false
        SpeedValue.isHidden = false
        Mileage.isHidden = false
        MileageValue.isHidden = false
        EventLog.isHidden = false
        ServerCommand.isHidden = true
        startTime.invalidate()
    }
}
