//
//  Profile.swift
//  SensorsLog
//
//  Created by Unitec on 12/5/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit

class UserLocal {
    static var userIdList = ["3a971ebc-e4e0-4c7c-8824-6ca9bf0f75a8", "4c410110-99c9-49b1-a325-7446df25c78d", "bd361967-67de-4220-87c8-43e11eab6d30","87ee8f63-3b96-44c1-8606-05f9e2d77a7b", "37a82c04-3953-4c73-8156-104622b6bb67", "336b0a27-10d8-4e24-bb45-33d15caaf754", "2aab222e-e110-49b4-9185-e634868c3f25","d7d53c36-d199-4b21-800c-1874ccbc4077", "b32a4dfb-1723-4398-afe1-59c41953f60f", "02284d1b-e026-4167-823d-06e53950beb3"]
    static var deviceIdList = ["3a398a51-0237-43f3-a283-ce6c9dc587bb", "a033bbef-595c-49ae-8773-d267c38da3d4", "c221b600-64cc-4582-923c-2664f2a2f600", "6da2c9c3-ba9d-4b15-9868-dfb308c14af3", "04c8f525-a45f-4a79-be8e-68ae29efc428", "3708e7d6-3958-4025-99f2-57ebc25ed5b1", "da8c5941-9fd1-4ecc-8ca1-25862f1f11ae", "400b08fb-05f4-426b-ae3c-e01572dc9801", "34dcccd9-fb18-4341-ad57-9397c6c881ae", "3bdd48d1-f162-46ec-9641-e273fe8c7104"]
    static var carIdList = ["ed95576f-2fb3-4c22-9ead-0bd40559600b", "cf8019b6-b6e6-490d-82b5-9c30ac379a67", "b1d4d024-cf1d-4b8a-b9a6-8045ad1f75f5", "fce6c3b5-7564-43a3-a809-76172a7eeea5", "7c72ae0f-2a14-46b5-ac7a-67add785dd32", "0689c00f-6969-45df-afb4-0655f2ce91de", "43ba84ed-2f1c-4f9f-9a1e-03ef6849bf64", "226a9cca-34f2-4cc0-93d5-48184d0ccc9e", "7f325ead-e7f8-4581-ab1b-7819d25828c3", "a99e6cd1-f3d2-4db0-8ab8-78fedb9eff97"]
    static var userNameList = ["PASA_mori", "4c410110-99c9-49b1-a325-7446df25c78d", "bd361967-67de-4220-87c8-43e11eab6d30","87ee8f63-3b96-44c1-8606-05f9e2d77a7b", "37a82c04-3953-4c73-8156-104622b6bb67", "336b0a27-10d8-4e24-bb45-33d15caaf754", "2aab222e-e110-49b4-9185-e634868c3f25","d7d53c36-d199-4b21-800c-1874ccbc4077", "AISC_shoda", "AISC_imoto"]
    
    static func getIndex(UserName: String) -> Int
    {
        for i in 0...self.userNameList.count
        {
        if(UserName == nil)
            {
                return 0
            }
        if (UserName == self.userNameList[i])
        {
            return i
        }
        }
        return 0
    }
}
