//
//  ViewController+Extension.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/01.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit
import PasasvoSDK

extension UIViewController
{
    func showErrorAlert(message:String, okHandler:(()->Void)? = nil){
        let alertViewController = AlertVC(title: NSLocalizedString("error_alert", comment: ""), message: message, preferredStyle: .alert)
        
        // Add alert actions
        let okAction = UIAlertAction(title: NSLocalizedString("close_alert", comment: ""), style: .default) { (action) in
            okHandler?()
        }
        
        alertViewController.addAction(okAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func showAlert(title:String, message:String, okHandler:(()->Void)? = nil){
        self.showAlert(title: title, message: message, okTitle: NSLocalizedString("OK", comment: ""), okHandler: okHandler)
    }
    
    func showAlert(title:String, message:String, okTitle: String, okHandler:(()->Void)? = nil){
        let alertViewController = AlertVC(title: title, message: message, preferredStyle: .alert)
        
        // Set a title and message
        alertViewController.title = title
        alertViewController.message = message
        
        // Add alert actions
        let okAction = UIAlertAction(
            title: okTitle,
            style: .default,
            handler: { (action) in
                okHandler?()
        }
        )
        alertViewController.addAction(okAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }

    func showToast(message : String, textColor: UIColor) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 20, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.text = message
        toastLabel.textColor = textColor
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    

    
    func showActivityIndicatory() {
        let container: UIView = UIView()
        container.tag = 100
        container.frame = self.view.frame
        container.center = self.view.center
        container.backgroundColor = UIColor.black
        container.alpha = CGFloat(0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = self.view.center
        loadingView.backgroundColor = UIColor.white
        loadingView.alpha = CGFloat(0.4)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 400)
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        actInd.center =  CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        actInd.color = UIColor.black
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        self.view.addSubview(container)
        actInd.startAnimating()
    }
    
    func stopActivityIndicatory()
    {
        if (self.view.viewWithTag(100) != nil)
        {
            self.view.viewWithTag(100)?.removeFromSuperview()
        }
    }
    
    
    
        func hideKeyboardWhenTappedAround()
        {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
        }
        
    
        func dismissKeyboard()
        {
            view.endEditing(true)
        }
        
        func getCurrentDateTimeMilisecond() -> String
        {
            let d = Date()
            let df = DateFormatter()
            df.dateFormat = "y-MM-dd H:m:ss.SSSS"
            return df.string(from: d)
        }
    

        func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0
                {
                    self.view.frame.origin.y -= keyboardSize.height*3/5
                }
            }
        }
    
        func keyboardWillHide(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y != 0
                {
                    self.view.frame.origin.y += keyboardSize.height*3/5
                }
            }
        }



}



class AlertVC: UIAlertController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
