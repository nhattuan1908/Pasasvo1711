//
//  StoreFile.swift
//  SensorsLog
//
//  Created by Unitec on 2017/08/01.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import PasasvoSDK

class Helper {
    
    static func writeFileInTextFormat(fileName: String!, dataStr: String) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(fileName)
            
            if FileManager.default.fileExists(atPath: path.path)
            {
                let data = ("\n" + dataStr).data(using: String.Encoding.utf8, allowLossyConversion: false)
                var error: Error?
                if let fileHandle = FileHandle(forWritingAtPath: path.path)
                {
                    fileHandle.seekToEndOfFile()
                    fileHandle.write(data!)
                    fileHandle.closeFile()
                }
            }
            else
            {
                let data = dataStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    try dataStr.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                }
                catch {/* error handling here */}
            }
            
        }
        
    }
    
    
    static func toByteArray<T>(_ value: T) -> [UInt8] {
        let totalBytes = MemoryLayout<T>.size
        var value = value
        return withUnsafePointer(to: &value) { valuePtr in
            return valuePtr.withMemoryRebound(to: UInt8.self, capacity: totalBytes) { reboundPtr in
                return Array(UnsafeBufferPointer(start: reboundPtr, count: totalBytes))
            }
        }
    }
    
    static func readFileInTextFormat(fileName:String) -> String
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(fileName)
            
            do {
                let text = try String(contentsOf: path, encoding: String.Encoding.utf8)
                return text
            }
            catch {/* error handling here */
                return ""
            }
        }
        return ""
    }
    static func writeFileInBinaryFormat(fileName: String!, data: [Float32]) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(fileName)
            if FileManager.default.fileExists(atPath: path.path)
            {
                var b = [UInt8]()
                for elm in data
                {
                    b.append(contentsOf: Helper.toByteArray(elm))
                }
                
                
                let pointer = UnsafeBufferPointer(start: b, count: b.count)
                let data = Data(buffer: pointer)
                if let fileHandle = FileHandle(forWritingAtPath: path.path)
                {
                    fileHandle.seekToEndOfFile()
                    fileHandle.write(data)
                    fileHandle.closeFile()
                }
                
                
            }
            else
            {
                var b = [UInt8]()
                for elm in data
                {
                    b.append(contentsOf: Helper.toByteArray(elm))
                }
                
                
                let pointer = UnsafeBufferPointer(start: b, count: b.count)
                let data = Data(buffer: pointer)
                do {
                    try data.write(to: path)
                }
                catch {/* error handling here */}
            }
        }
        
    }
    
    
    static func getTodayString() -> String{
        let timezoneOffset = NSTimeZone.system.secondsFromGMT()/3600
        let timestamps = NSDate().timeIntervalSince1970  // + (timezoneOffset)*3600
        let datae = Date(timeIntervalSince1970: timestamps)
        let df = DateFormatter()
        
        df.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS+0\(timezoneOffset):00"
        return df.string(from: datae).replacingOccurrences(of: " ", with: "T")
        
    }
    static func getTimeString() -> String{
        let timezoneOffset = NSTimeZone.system.secondsFromGMT()/3600
        let timestamps = NSDate().timeIntervalSince1970  // + (timezoneOffset)*3600
        let datae = Date(timeIntervalSince1970: timestamps)
        let df = DateFormatter()
        
        df.dateFormat = "HH:mm:ss.SSS+0\(timezoneOffset):00"
        return df.string(from: datae).replacingOccurrences(of: " ", with: "T")
        
    }
    
    
    
    static func parseTimestampsToDateTimeMilisecond(timestamps : Double?) -> String
    {
        if timestamps == nil
        {
            return "0000-00-00"
        }
        let bootTime = NSDate(timeIntervalSinceNow: -ProcessInfo.processInfo.systemUptime) as Date
        let datae = Date(timeInterval: TimeInterval(timestamps!), since: bootTime)
        // let d = Date()
        let df = DateFormatter()
        let timezoneOffset = NSTimeZone.system.secondsFromGMT()/3600
        df.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS+0\(timezoneOffset):00"
        return df.string(from: datae)
    }
    
    static func parseDateTime(dateTimeString: String) -> String
    {
        // Chua biet lam rang nen viet dai de mai test da
        let index = dateTimeString.index(dateTimeString.startIndex, offsetBy: 23)
        let timezoneOffset = NSTimeZone.system.secondsFromGMT()/3600
        return dateTimeString.substring(to: index) + "+0\(timezoneOffset):00"
        
    }
    
    
//    static func deleteHistoryDatas(result:DetailHistory) -> Bool {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.mainManagedObjectContext
//        
//        context?.delete(result)
//        do {
//            try context?.save()
//            return true
//        }
//        catch
//        {
//            return false
//        }
//    }
    //
    //    static func cleanHistoryDatas() -> Bool {
    //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        let context = appDelegate.mainManagedObjectContext
    //        let delete = NSBatchDeleteRequest(fetchRequest: DetailHistory.fetchRequest())
    //        do {
    //            try context.execute(delete)
    //            return true
    //        }
    //        catch {
    //         return false
    //        }
    //    }
    //
    //    static func cleanHistoryDatas() -> Bool
    //    {
    //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        let context = appDelegate.mainManagedObjectContext
    //        let delete = NSPersistentStoreRequest()
    //
    //
    //
    //    }
    ////
    
//    static func filterHistoryDatas(search: String) -> [DetailHistory] {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.mainManagedObjectContext
//        
//        let ferchRequest:NSFetchRequest<DetailHistory> = DetailHistory.fetchRequest()
//        var result:[DetailHistory]? =  nil
//        var key = NSPredicate(format: "start contanin[c] %@", search)
//        ferchRequest.predicate = key
//        do {
//            result = try context?.fetch(ferchRequest)
//            return result!
//        }
//        catch {
//            return result!
//        }
//    }
//    
//    static func getHistoryDatas() -> [DetailHistory]
//    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.mainManagedObjectContext
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DetailHistory")
//        request.returnsObjectsAsFaults = false
//        var results:[DetailHistory]? = nil
//        do {
//            results = try context?.fetch(DetailHistory.fetchRequest())
//            return results!
//        }
//        catch   {
//            return results!
//        }
//    }
//    
//    
//    
//    static func setDetailDataHistory(distance: Int, time: Float, diagnos: String, safedrive: String)
//    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        let context = appDelegate.mainManagedObjectContext
//        
//        let newData = NSEntityDescription.insertNewObject(forEntityName: "DetailHistory", into: context!)
//        newData.setValue(distance, forKey: "distance")
//        newData.setValue(time, forKey: "time")
//        newData.setValue(diagnos, forKey: "diagnos")
//        newData.setValue(safedrive, forKey: "safedrive")
//        newData.setValue("2017-11-01 11:22", forKey: "start")
//       
//        do {
//            try context?.save()
//        }
//        catch {
//            
//        }ß
//    }
//    
//    
    static func insertDetailDataLogs(detailsLog: DetailLogsModel)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.mainManagedObjectContext
        
        let newData = NSEntityDescription.insertNewObject(forEntityName: "DetailLogs", into: context!)
        newData.setValue(detailsLog.drivingId, forKey: LogTableFieldName.drivingId)
        newData.setValue(detailsLog.measureTime, forKey: LogTableFieldName.measureTime)
        newData.setValue(detailsLog.dangerousTriggerType, forKey: LogTableFieldName.dangerousTriggerType)
        newData.setValue(detailsLog.dangerousType, forKey: LogTableFieldName.dangerousType)
        newData.setValue(detailsLog.dangerousDrivingId, forKey: LogTableFieldName.dangerousDrivingId)
        
        do {
            try context?.save()
        }
        catch {
            
        }
    }
    
    static func getLogsDatas(groupBy: [String]) -> NSArray?
    {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.mainManagedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DetailLogs")
        let entity = NSEntityDescription.entity(forEntityName: "DetailLogs", in: context!)
        if groupBy.count > 0 {
            var keypathExps = [NSExpression]()
            var groupByDescs = [Any]()
            for value in groupBy
            {
                keypathExps.append(NSExpression(forKeyPath: value))
                groupByDescs.append((entity?.attributesByName[value])!)
            }
            
            let expression = NSExpression(forFunction: "count:", arguments: keypathExps)
            
            let countDesc = NSExpressionDescription()
            countDesc.expression = expression
            countDesc.name = "count"
            countDesc.expressionResultType = .integer64AttributeType
            request.propertiesToGroupBy = groupByDescs
            groupByDescs.append(countDesc)
            request.propertiesToFetch = groupByDescs
        }
        request.resultType = .dictionaryResultType
        
        do {
            var results = try context?.fetch(request) as NSArray?
            // results = try context?.fetch(DetailLogs.fetchRequest())
            return results
        }
        catch   {
            return nil
        }
    }
    
    static func getLogsDatas() -> [DetailLogs]
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.mainManagedObjectContext
        var results:[DetailLogs]? = nil
        do {
            results = try context?.fetch(DetailLogs.fetchRequest())
            return results!
        }
        catch   {
            return []
        }
    }
    
    static func updateLogData(detailLogUpdated: DetailLogsModel)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.mainManagedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DetailLogs")
        let entity = NSEntityDescription.entity(forEntityName: "DetailLogs", in: context!)
        let predicate = NSPredicate(format: "dangerousDrivingId ='\(detailLogUpdated.dangerousDrivingId!)'", argumentArray: nil)
        request.predicate = predicate
        do {
            var results = try context?.fetch(request) as NSArray?
            if (results?.count)! > 0
            {
                let updatedObject = results?[0] as! DetailLogs
                updatedObject.setValue(detailLogUpdated.dangerousType!, forKey: LogTableFieldName.dangerousType)
                do
                {
                    try context?.save()
                }
                catch
                {
                    Log.debugPrint("UPDATE LOG DATA FAILED: \(error)")
                }
                
                
                
            }
            
            // results = try context?.fetch(DetailLogs.fetchRequest())
            
        }
        catch   {
            
        }
        
        
        
    }
    
    static func deleteAllLogData() -> Bool
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.mainManagedObjectContext
        let deletedObjects = Helper.getLogsDatas()
        for deleteObject in deletedObjects
        {
            context?.delete(deleteObject)
        }
        do {
            try context?.save()
            return true
        }
        catch
        {
            return false
        }
    }
    
    static func deleteLogsDatas(result:DetailLogs) -> Bool {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.mainManagedObjectContext
        
        context?.delete(result)
        do {
            try context?.save()
            return true
        }
        catch
        {
            return false
        }
    }
    
    static func deleteLogData(dangerousDrivingId: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.mainManagedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DetailLogs")
        let entity = NSEntityDescription.entity(forEntityName: "DetailLogs", in: context!)
        let predicate = NSPredicate(format: "dangerousDrivingId ='\(dangerousDrivingId)'", argumentArray: nil)
        request.predicate = predicate
        do {
            var results = try context?.fetch(request) as NSArray?
            if (results?.count)! > 0
            {
                let deleteObject = results?[0] as! DetailLogs
                do
                {
                    try context?.delete(deleteObject)
                }
                catch
                {
                    Log.debugPrint("UPDATE LOG DATA FAILED: \(error)")
                }
            }
            
        }
        catch   {
            
        }
        
        
        
    }
}



