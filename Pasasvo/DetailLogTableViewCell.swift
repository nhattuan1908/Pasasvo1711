//
//  DetailLogTableViewCell.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/12/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class DetailLogTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dangerousTitle: UILabel!
    @IBOutlet weak var dangerousCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
