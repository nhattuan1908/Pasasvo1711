//
//  CommonConfig.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/01.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit


struct ConstantColors {
    static var UserEventBorder = UIColor.white
    //cam
    static var UserConfirmBackgroud = UIColor(red: 244/255, green: 177/255, blue: 131/255, alpha: 1)
    static var UserConfirmTitle = UIColor.white
    static var CommonButtonBackGround = UIColor(red: 91/255, green: 155/255, blue: 213/255, alpha: 1)
    static var ComminButtonTitle = UIColor.white
    static var NavigationBar = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)
    static var UserEventBackground = UIColor(red: 158/255, green: 194/255, blue: 135/255, alpha: 1)
    static var UserEventBackgroundSelected = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
    
    static var UserEventTitle = UIColor.black
    static var DialogBackground = UIColor(red: 239, green: 239, blue: 239, alpha: 1)
    static var UserCheckBackground = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
}

struct ConstantFonts
{
    static var CommitFont = UIFont(name: "Avenir-Heavy", size: CGFloat(25))
    static var UserConfirmFont = UIFont(name: "Avenir-Heavy", size: CGFloat(15))
}



open class LogTableFieldName
{
    static let dangerousType = "dangerousType"
    static let dangerousDrivingId = "dangerousDrivingId"
    static let dangerousTriggerType = "dangerousTriggerType"
    static let measureTime = "measureTime"
    static let drivingId = "drivingId"
}


struct AppConfig{
    static var DangerousType: [Int: String] = [1:"Hard Acceleration" , 2:"Hard Acceleration", 3:"Hard braking", 4:"Sudden Steering", 5:"Unsteady Driving",6: "Bump", 98: "Undeciable", 99: "Normal"]
    static var ResultType: [Int: String] = [1: "A", 2: "B", 3: "C", 26: "Z"]
    
    
    static var DEVICE_UUID : String
    {
        get {
            return UIDevice.current.identifierForVendor?.uuidString ?? ""
        }
        
    }
    
    static var APP_VERSION : String
    {
        get {
            return String(1.1)
        }
        
    }
    
    
}

open class DangerousType
{
    var dangerousType: Int?
    var dangerousTitle: String?
    init(dangerousType: Int, dangerousTitle: String) {
        self.dangerousType = dangerousType
        self.dangerousTitle = dangerousTitle
    }
}
