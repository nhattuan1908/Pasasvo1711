//
//  RegisterViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/13/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import PasasvoSDK

class RegisterViewController: UIViewController {
    @IBOutlet weak var SaveBtn: UIButton!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var UserNameContent: UILabel!
    @IBOutlet weak var UserID: UILabel!
    @IBOutlet weak var UserIDValue: UILabel!
    @IBOutlet weak var DeviceID: UILabel!
    @IBOutlet weak var DeviceIDValue: UILabel!
    @IBOutlet weak var CarID: UILabel!
    @IBOutlet weak var CarIDValue: UILabel!
    
    
    @IBAction func SaveData(_ sender: UIButton) {
        //MenuViewController.Index = 0
        PasasvoConfig.setCurrentCar(value: "")
        PasasvoConfig.setCurrentUser(value: "")
        PasasvoConfig.setCurrentDevice(value: "")
        UserDefaults.standard.setCurrentUserName(value: "")
        UserDefaults.standard.setLoginId(value: "")
        UserDefaults.standard.setPassword(value: "")
        self.performSegue(withIdentifier: "show_back_to_menu_view", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UserName.text = "User Name"
        self.UserID.text = "UserID"
        self.DeviceID.text = "DeviceID"
        self.CarID.text = "CarID"
        self.UserNameContent.text = UserDefaults.standard.getCurrentUserName()
        self.UserIDValue.text = PasasvoConfig.userId
        self.CarIDValue.text = PasasvoConfig.carId
        self.DeviceIDValue.text = PasasvoConfig.deviceId
        self.navigationItem.title = NSLocalizedString("Register", comment: "")
        handleView()
        setButtonUser()
        setButtonBack()
        setButtonSave()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    func callMethod()
    {
        self.performSegue(withIdentifier: "show_back_to_main_view", sender: self)
    }
    func setButtonBack() {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(RegisterViewController.callMethod), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    func setButtonUser() {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "user"), for: UIControlState.normal)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    func setButtonSave()
    {
        SaveBtn.layer.cornerRadius = SaveBtn.frame.height/2
        SaveBtn.backgroundColor = ConstantColors.CommonButtonBackGround
        SaveBtn.setTitleColor(UIColor.white, for: .normal)
        SaveBtn.setTitle(NSLocalizedString("Sign Out", comment: ""), for: .normal)
    }
    
    func handleView()
    {
        let viewHeight = self.view.frame.height
        let viewWitdh = self.view.frame.width
        let constantHeight = CGFloat(2210)
        let constantWidth = CGFloat(1240)
        
        self.SaveBtn.titleLabel?.font = ConstantFonts.CommitFont
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint(item: self.SaveBtn, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(100*viewHeight)/constantHeight) ,
                NSLayoutConstraint(item: self.SaveBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(170*viewWitdh)/constantWidth),
                ])
        
    }
    
}
