//
//  MainViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/2/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import Foundation
import PasasvoSDK

class MainViewController: UIViewController {
    
    @IBOutlet weak var carLogoImg: UIImageView!
    @IBOutlet weak var userBtn: UIButton!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var logBtn: UIButton!
    @IBOutlet weak var startBtn: UIButton!
    
    var settingsClass = SettingsViewController()
    var companyTitle: UILabel!
    var appNameTitle: UILabel!
    var versionTitle: UILabel!
    
    var appVersionValue = AppConfig.APP_VERSION
    
    
    @IBOutlet weak var switchModeTitle: LabelWithAdaptiveTextHeight!
    
    @IBAction func unwindToMainView(segue:UIStoryboardSegue) { }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        flag = false
        self.initButtonImagesView()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func StartOperation(_ sender: UIButton) {
        self.showActivityIndicatory()
        Pasasvo.startDataCollect { (responseCode, errorMessage) -> (Void) in
            self.stopActivityIndicatory()
            if (responseCode == 201 || responseCode == 200)
            {
                if(!UserDefaults.standard.getTestModeSettings())
                {
                    self.performSegue(withIdentifier: "show_run_segue", sender: self)
                }
                else
                {
                    self.performSegue(withIdentifier: "show_measure_segue", sender: self)
                }
            }
            else
            {
                if (errorMessage?.range(of: "The Internet connection appears to be offline") != nil)
                {
                    self.showAlert(title: "Error", message: "\(responseCode):  Network error")
                    return
                }
                else if (errorMessage?.range(of: "The request timed out.") != nil)
                {
                    self.showAlert(title: "Error", message: "\(responseCode):  Server time out")
                    return
                }
                else
                {
                self.showAlert(title: "Error", message: "\(responseCode) \(String(describing: errorMessage))")
                return
                }
            }
        }
    }
    
    private func initButtonImagesView() {
        self.handleView()
        self.logBtn.setBackgroundImage(UIImage(named: "start"), for: .normal)
        self.logBtn.setTitle("", for: .normal)
        self.settingsBtn.setBackgroundImage(UIImage(named: "settings"), for: .normal)
        self.settingsBtn.setTitle("", for: .normal)
        self.userBtn.setBackgroundImage(UIImage(named: "user"), for: .normal)
        self.userBtn.setTitle("", for: .normal)
        self.historyBtn.setBackgroundImage(UIImage(named: "history"), for: .normal)
        self.historyBtn.setTitle("", for: .normal)
        self.startBtn.setTitle(NSLocalizedString("start_btn", comment: ""), for: .normal)
        self.startBtn.setTitleColor(ConstantColors.ComminButtonTitle, for: .normal)
        self.startBtn.backgroundColor = ConstantColors.CommonButtonBackGround
        self.startBtn.layer.cornerRadius = CGFloat(self.startBtn.frame.height/4)
        self.startBtn.titleLabel?.font = ConstantFonts.CommitFont
        self.navigationController?.isNavigationBarHidden = true
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func handleView()
    {
        
        let viewHeight = self.view.frame.height
        let viewWitdh = self.view.frame.width
        let constantHeight = CGFloat(2210)
        let constantWidth = CGFloat(1240)
        
        let leftSpace = CGFloat(170)
        self.companyTitle = LabelWithAdaptiveTextHeight(frame: CGRect(x: leftSpace*viewWitdh/constantWidth, y: CGFloat(300)*viewHeight/constantHeight, width: 900*viewWitdh/constantWidth, height: CGFloat(150)*viewHeight/constantHeight))
        let font = UIFont(name: "ArialRoundedMTBold", size: CGFloat(10))
        self.companyTitle.text = NSLocalizedString("company_title", comment: "")
        self.companyTitle.font = font
        self.companyTitle.textAlignment = .center
        self.appNameTitle = LabelWithAdaptiveTextHeight(frame: CGRect(x: leftSpace*viewWitdh/constantWidth, y: CGFloat(430)*viewHeight/constantHeight, width: 900*viewWitdh/constantWidth, height: CGFloat(150)*viewHeight/constantHeight))
        self.appNameTitle.text = NSLocalizedString("appname_title", comment: "")
        self.appNameTitle.font = font
        self.appNameTitle.textAlignment = .center
        self.versionTitle = LabelWithAdaptiveTextHeight(frame: CGRect(x: leftSpace*viewWitdh/constantWidth, y: CGFloat(580)*viewHeight/constantHeight, width: 900*viewWitdh/constantWidth, height: CGFloat(150)*viewHeight/constantHeight))
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            PasasvoConfig.setVersion(value: version)
        }
        self.versionTitle.text = "Version \(PasasvoConfig.version)"
        self.versionTitle.font = UIFont(name: "ArialRoundedMT", size: CGFloat(5))
        self.versionTitle.textAlignment = .center
        
        let bottomSpaceBottom = CGFloat(100*viewHeight)/constantHeight
        let buttonSize = CGFloat(223*viewWitdh)/constantWidth
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint(item: self.carLogoImg, attribute: .top, relatedBy: .equal, toItem:  self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: CGFloat(150*viewHeight)/constantHeight),
                NSLayoutConstraint(item: self.carLogoImg, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(150*viewWitdh)/constantWidth),
                
                NSLayoutConstraint(item: self.logBtn, attribute: .left, relatedBy: .equal, toItem: self.view , attribute: .left, multiplier: 1, constant: CGFloat(100*viewWitdh)/constantWidth),
                NSLayoutConstraint(item: self.logBtn, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(bottomSpaceBottom)),
                NSLayoutConstraint(item: self.logBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: buttonSize),
                
                NSLayoutConstraint(item: self.historyBtn, attribute: .left, relatedBy: .equal, toItem: self.logBtn , attribute: .right, multiplier: 1, constant: CGFloat(50*viewWitdh)/constantWidth),
                NSLayoutConstraint(item: self.historyBtn, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(bottomSpaceBottom)),
                NSLayoutConstraint(item: self.historyBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: buttonSize),
                
                NSLayoutConstraint(item: self.settingsBtn, attribute: .left, relatedBy: .equal, toItem: self.historyBtn , attribute: .right, multiplier: 1, constant: CGFloat(50*viewWitdh)/constantWidth),
                NSLayoutConstraint(item: self.settingsBtn, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(bottomSpaceBottom)),
                NSLayoutConstraint(item: self.settingsBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: buttonSize),
                
                NSLayoutConstraint(item: self.userBtn, attribute: .left, relatedBy: .equal, toItem: self.settingsBtn , attribute: .right, multiplier: 1, constant: CGFloat(50*viewWitdh)/constantWidth),
                NSLayoutConstraint(item: self.userBtn, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(bottomSpaceBottom)),
                NSLayoutConstraint(item: self.userBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: buttonSize),
                
                NSLayoutConstraint(item: self.startBtn, attribute: .bottom, relatedBy: .equal, toItem: self.userBtn, attribute: .top, multiplier: 1, constant: -CGFloat(275*viewHeight)/constantHeight) ,
                NSLayoutConstraint(item: self.startBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(170*viewWitdh)/constantWidth),
                ]
        )
        
        self.view.addSubview(self.companyTitle)
        self.view.addSubview(self.appNameTitle)
        self.view.addSubview(self.versionTitle)
        
    }
    
    
}


