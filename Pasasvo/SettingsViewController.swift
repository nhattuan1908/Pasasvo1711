//
//  SettingsViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/2/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import Eureka
import PasasvoSDK

class SettingsViewController: FormViewController {
    
    var saveSettingsBtn: UIBarButtonItem!
    var backSettingsBtn: UIBarButtonItem!
    var allRulesPassed = true
    var testmode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setButtonBack()
        self.navigationItem.title  = NSLocalizedString("settings", comment: "")
        setButtonSave()
        var rules = RuleSet<String>()
        rules.add(rule: RuleRequired())
        
        form +++
            Section("")
            
            <<< ActionSheetRow<Int>("sensor_data")
            {
                $0.title = NSLocalizedString("Sensor data", comment: "")
                $0.value = 20
                $0.options = [5, 10, 20, 50, 100, 200]
                
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                }).cellSetup({ (cell, row) in
                    if (UserDefaults.standard.getSensorDataSettings() != 0)
                    {
                        row.value = UserDefaults.standard.getSensorDataSettings()
                        
                    }
                })
        
        form +++ Section(NSLocalizedString("Sensor selection", comment: ""))
            
            <<< SwitchRow("gyro_enable") {
                $0.title = "Gyro"
                $0.value = UserDefaults.standard.getGyroEnableSettings()
            }
            <<< SwitchRow("acceleration_enable") {
                $0.title = NSLocalizedString("Acc", comment: "")
                $0.value = UserDefaults.standard.getAccelerationEnableSettings()
            }
            <<< SwitchRow("gps_enable") {
                $0.title = "GPS"
                $0.value = UserDefaults.standard.getGpsEnableSettings()
            }
            <<< ActionSheetRow<Float>("upload_interval")
            {
                $0.title = NSLocalizedString("Upload interval", comment: "")
                $0.value = 0
                $0.options = [0,1,3,5, 10, 20, 30 ,60]
                
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                }).cellSetup({ (cell, row) in
                    if (UserDefaults.standard.getUploadIntervalSettings() != 0)
                    {
                        row.value = UserDefaults.standard.getUploadIntervalSettings()
                    }
                })
            <<< SwitchRow("real_time_log_display") {
                $0.title = NSLocalizedString("Real time log display", comment: "")
                $0.value = UserDefaults.standard.getRealtimeLogSettings()
                
            }
            <<< ActionSheetRow<String>("OBD2Settings")
            {
                $0.title = NSLocalizedString("OBD2 setting", comment: "")
                $0.value = "None"
                $0.options = ["None", "Dummy", "Can"]
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                }).cellSetup({ (cell, row) in
                    if (UserDefaults.standard.getOBD2Settings() != "")
                    {
                        row.value = UserDefaults.standard.getOBD2Settings()
                    }
                    
                })
            <<< SwitchRow("security_settings") {
                $0.title = "Https"
                $0.value = UserDefaults.standard.getSecurityEnableSettings()
            }
            
            <<< IntRow("smartphone_memory_setting")
            {
                $0.title = "Memory Limit(MB)"
                $0.value = UserDefaults.standard.getSmartphoneMemorySettings()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                })
            <<< IntRow("Max time")
            {
                $0.title = "Max time(min)"
                $0.value = Int(UserDefaults.standard.getMaxDrivingTime())
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                })
            <<< SwitchRow("test_mode") {
                $0.title = NSLocalizedString("Test mode", comment: "")
                $0.value = UserDefaults.standard.getTestModeSettings()
                }.onChange{_ in
                    self.testmode = !self.testmode
            }
            
            <<< ActionSheetRow<Int>("speed_data_time")
            {
                $0.title = "Speed calculating interval(s)"
                $0.value = 3
                $0.options = [3, 5, 10, 15, 20,30]
                
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                }).cellSetup({ (cell, row) in
                    if (UserDefaults.standard.getSpeedDataSettings() != 0)
                    {
                        row.value = UserDefaults.standard.getSpeedDataSettings()
                        
                    }
                })
            
            <<< ActionSheetRow<Int>("dangerous_data")
            {
                $0.title = "Server polling intervals(s)"
                $0.value = 3
                $0.options = [3,5, 10, 15, 20, 30]
                
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                }).cellSetup({ (cell, row) in
                    if (UserDefaults.standard.getDangerousDataSettings() != 0)
                    {
                        row.value = UserDefaults.standard.getDangerousDataSettings()
                        
                    }
                })
            
            <<< ActionSheetRow<Int>("driving_data_time")
            {
                $0.title = "Driving data upload inteval(s)"
                $0.value = 3
                $0.options = [3, 5, 10, 15, 20, 30]
                
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = 0
                    }
                }).cellSetup({ (cell, row) in
                    if (UserDefaults.standard.getDrivingDataSettings() != 0)
                    {
                        row.value = UserDefaults.standard.getDrivingDataSettings()
                        
                    }
                })
            
            <<< TextRow("Memo")
            {
                $0.title = "Memo"
                $0.value = UserDefaults.standard.getMemo()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                })
            +++ Section()
            <<< TextRow("service_domain") {
                $0.title = NSLocalizedString("service_domain", comment: "")
                $0.value = PasasvoConfig.serverUrl
                $0.placeholder = "192.168.0.99:8080"
                }
                .cellUpdate { cell, row in
                    cell.textField.keyboardType = .URL
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                }
                .onRowValidationChanged ({ cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                            let labelRow = LabelRow() {
                                $0.title = validationMsg
                                $0.cell.height = { 25 }
                                }.cellUpdate({ (cell, row) in
                                    cell.textLabel?.text = validationMsg
                                    cell.textLabel?.textColor = UIColor.red
                                    cell.textLabel?.font = UIFont(name: "Avenir-BookOblique", size: CGFloat(15))
                                })
                            row.section?.insert(labelRow, at: row.indexPath!.row + index + 1)
                        }
                    }
                })
            +++ Section("CarID")
            <<< TextRow("CarId") {
                $0.value = UserDefaults.standard.getCarId()
                $0.placeholder = UserDefaults.standard.getCarId()
            }
            <<< ButtonRow() {
                $0.title = "Change car"
            }
            
            <<< ButtonRow() {
                $0.title = "API Test"
                }.onCellSelection({ (cell, row) in
                    self.performSegue(withIdentifier: "show_sdktest_view", sender: self)
                })
    }
    
    func setButtonBack() {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(SettingsViewController.backSettings), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
    }
    
    func setButtonSave() {
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action:#selector(SettingsViewController.saveSettings), for:.touchUpInside)
        
        button.setImage(UIImage(named: "save"), for: UIControlState.normal)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        //        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    func saveSettings()
    {
        let error = form.validate()
        if (error.count > 0)
        {
            self.showToast(message: NSLocalizedString("enter_all_settings_required", comment: ""), textColor: UIColor.white)
            return
        }
        
        let OBD2SettingsRow: ActionSheetRow<String> = form.rowBy(tag: "OBD2Settings")!
        let uploadIntervalRow: ActionSheetRow<Float> = form.rowBy(tag: "upload_interval")!
        let sensorDataRow: ActionSheetRow<Int> = form.rowBy(tag: "sensor_data")!
        let gyroRow: SwitchRow = form.rowBy(tag: "gyro_enable")!
        let accelerationEnableRow: SwitchRow = form.rowBy(tag: "acceleration_enable")!
        let gpsEnableRow: SwitchRow = form.rowBy(tag: "gps_enable")!
        let realTimeLogSisplayRow: SwitchRow = form.rowBy(tag: "real_time_log_display")!
        let securitySettingsRow :SwitchRow = form.rowBy(tag: "security_settings")!
        let smartphoneMemorySettingRow : IntRow = form.rowBy(tag: "smartphone_memory_setting")!
        let MaxDrivingTimeRow : IntRow = form.rowBy(tag: "Max time")!
        let testModeRow: SwitchRow = form.rowBy(tag: "test_mode")!
        let serviceRow: TextRow = form.rowBy(tag: "service_domain")!
        let memoRow: TextRow = form.rowBy(tag: "Memo")!
        let dangerousRow: ActionSheetRow<Int> = form.rowBy(tag: "dangerous_data")!
        let speedRow: ActionSheetRow<Int> = form.rowBy(tag: "speed_data_time")!
        let drivingRow: ActionSheetRow<Int> = form.rowBy(tag: "driving_data_time")!
        
        
        UserDefaults.standard.setOBD2Settings(value: OBD2SettingsRow.value!)
        UserDefaults.standard.setSensorDataSettings(value: sensorDataRow.value!)
        UserDefaults.standard.setGyroEnableSettings(value: gyroRow.value!)
        UserDefaults.standard.setAccelerationEnableSettings(value: accelerationEnableRow.value!)
        UserDefaults.standard.setGpsEnableSettings(value: gpsEnableRow.value!)
        UserDefaults.standard.setRealtimeLogSettings(value: realTimeLogSisplayRow.value!)
        UserDefaults.standard.setSecurityEnableSettings(value: securitySettingsRow.value!)
        UserDefaults.standard.setSmartphoneMemorySettings(value: smartphoneMemorySettingRow.value!)
        UserDefaults.standard.setMemo(value: memoRow.value!)
        UserDefaults.standard.setTestModeSettings(value: testModeRow.value!)
        UserDefaults.standard.setUploadIntervalSettings(value: uploadIntervalRow.value!)
        UserDefaults.standard.setDrivingDataSettings(value: drivingRow.value!)
        UserDefaults.standard.setSpeedDataSettings(value: speedRow.value!)
        UserDefaults.standard.setDangerousDataSettings(value: dangerousRow.value!)
        UserDefaults.standard.setServerUrl(value: serviceRow.value!)
        UserDefaults.standard.setMaxDrivingTime(value: MaxDrivingTimeRow.value!)
        
        
        // Setting
        PasasvoConfig.carId = UserDefaults.standard.getCarId()
        PasasvoConfig.deviceId = UserDefaults.standard.getDeviceId()
        PasasvoConfig.userId = UserDefaults.standard.getUserId()
        
        PasasvoConfig.intervalSensor = UserDefaults.standard.getSensorDataSettings()
        PasasvoConfig.intervalDrivingData = UserDefaults.standard.getDrivingDataSettings()
        PasasvoConfig.intervalDataUpload = UserDefaults.standard.getUploadIntervalSettings()
        PasasvoConfig.intervalCalcSpeed = UserDefaults.standard.getSpeedDataSettings()
        PasasvoConfig.testMode = UserDefaults.standard.getTestModeSettings()
        PasasvoConfig.memo = UserDefaults.standard.getMemo()
        PasasvoConfig.serverUrl = UserDefaults.standard.getServerUrl()
        PasasvoConfig.obdOption = CanOBD2Mode.getCanModeByName(name: UserDefaults.standard.getOBD2Settings())!
        PasasvoConfig.sensorOption = []
        PasasvoConfig.serverUrl = UserDefaults.standard.getServerUrl()
        PasasvoConfig.maxDrivingTime = Double(UserDefaults.standard.getMaxDrivingTime())
        
        if UserDefaults.standard.getGyroEnableSettings(){
            PasasvoConfig.sensorOption.append(Int(SensorType.Gyro.rawValue))
        }
        if UserDefaults.standard.getAccelerationEnableSettings(){
            PasasvoConfig.sensorOption.append(Int(SensorType.Acc.rawValue))
        }
        if UserDefaults.standard.getGpsEnableSettings(){
            PasasvoConfig.sensorOption.append(Int(SensorType.GPS.rawValue))
        }
        PasasvoConfig.intervalServerPolling = UserDefaults.standard.getDangerousDataSettings()
        
        
        self.performSegue(withIdentifier: "settings_back_to_main_view_segue", sender: self)
        
        
    }
    func backSettings()
    {
        saveSettings()
        self.performSegue(withIdentifier: "settings_back_to_main_view_segue", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

