//
//  LogConfirmViewController.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/12/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import PasasvoSDK


class LogConfirmViewController: UIViewController {
    
    
    var events = [DangerousType]()
    var optionButtonHeight = CGFloat(60)
    var selectedLog: DetailLogs?
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var measureDatetimeLbl: UILabel!
    @IBOutlet weak var contentUIView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        events.append(DangerousType(dangerousType: 4, dangerousTitle: "Sudden Steering"))
        events.append(DangerousType(dangerousType: 2, dangerousTitle: "Hard Acceleration"))
        events.append(DangerousType(dangerousType: 3, dangerousTitle: "Hard braking"))
        events.append(DangerousType(dangerousType: 5, dangerousTitle: "Unsteady Driving"))
        events.append(DangerousType(dangerousType: 6, dangerousTitle: "Bump"))
        events.append(DangerousType(dangerousType: 98, dangerousTitle: "Undeciable"))
        events.append(DangerousType(dangerousType: 99, dangerousTitle: "Normal"))
        // Do any additional setup after loading the view.
        handleOptionsView(contentView: contentUIView)
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint(item: self.cancelButton, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(20)),
                NSLayoutConstraint(item: self.cancelButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 45),
                NSLayoutConstraint(item: self.cancelButton, attribute: .left , relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 50),
                NSLayoutConstraint(item: self.cancelButton, attribute: .right , relatedBy: .equal, toItem: self.cancelButton, attribute: .right, multiplier: 1, constant: -50),
                ])
        self.cancelButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        self.cancelButton.setTitleColor(ConstantColors.ComminButtonTitle, for: .normal)
        self.cancelButton.backgroundColor = ConstantColors.CommonButtonBackGround
        self.cancelButton.layer.cornerRadius = CGFloat(self.cancelButton.frame.height/3)
        self.cancelButton.titleLabel?.font = ConstantFonts.CommitFont
        self.cancelButton.addTarget(self, action:#selector(self.backToLogView), for: .touchUpInside)
        self.view.addSubview(cancelButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func handleOptionsView(contentView: UIView)
    {
        for view in contentView.subviews
        {
            view.removeFromSuperview()
        }
        self.titleLbl.text = NSLocalizedString("change_dangerous_type", comment: "")
        self.measureDatetimeLbl.text = selectedLog?.measureTime!
        let image_distance: CGFloat =  10
        var x: CGFloat = 10
        var y: CGFloat = 10
        let height : CGFloat = optionButtonHeight
        let item_in_row = 2
        let total = events.count
        let a = total - total%item_in_row
        let b = total%item_in_row
        var current_item_row = item_in_row
        let cornerRadius = CGFloat(7)
        for var i in (1...total){
            if (i > a) {
                current_item_row = b
            }
            let item_width: CGFloat = (self.view.frame.width - image_distance*(CGFloat(current_item_row)+1))/CGFloat(current_item_row)
            let optionButton = UIButton(frame: CGRect(x: x, y: y, width: item_width, height: height))
            optionButton.layer.cornerRadius = 0.2
            optionButton.alpha = CGFloat(0.5)
            optionButton.tag = i-1
            optionButton.layer.cornerRadius = cornerRadius
            optionButton.layer.masksToBounds = true
            optionButton.layer.borderWidth =  2
            optionButton.layer.borderColor = ConstantColors.UserEventBorder.cgColor
            optionButton.setTitle(self.events[i-1].dangerousTitle, for: .normal)
            optionButton.setTitleColor(ConstantColors.UserEventTitle, for: .normal)
            optionButton.setTitleColor(UIColor.white, for: .highlighted)
            optionButton.titleLabel?.font = UIFont(name: "Avenir-Black", size: 15)
            optionButton.backgroundColor = ConstantColors.UserEventBackground
            optionButton.setBackgroundColor(color: UIColor.black, forState: .highlighted)
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(LogConfirmViewController.handleUserEventSelected(_:)))
            optionButton.addGestureRecognizer(recognizer)
            optionButton.isUserInteractionEnabled = true
            
            contentView.addSubview(optionButton)
            contentView.isUserInteractionEnabled = true
            
            let c = i%item_in_row
            x = image_distance +  (image_distance + item_width)*CGFloat(c)
            if (i % Int(item_in_row) == 0)
            {
                x = image_distance
                y = y + height + image_distance
            }
        }
    }
    
    func handleUserEventSelected(_ sender: UITapGestureRecognizer)
    {
        let button = sender.view as! UIButton
        if (button.tag == 7)
        {
            self.sendServerEventAnswer(answerYes: false, dangerousTypeIndexSelected: button.tag)
        }
        else
        {
            self.sendServerEventAnswer(answerYes: true, dangerousTypeIndexSelected: button.tag)
        }
        //  self.handleConfirmView(contentView: self.contentUIView, dangerousTypeIndexSelected: button.tag)
    }
    
    func handleConfirmView(contentView: UIView, dangerousTypeIndexSelected: Int)
    {
        for view in contentView.subviews
        {
            view.removeFromSuperview()
        }
        self.titleLbl.text = NSLocalizedString("change_event_title", comment: "")
        
        self.measureDatetimeLbl.text = selectedLog?.measureTime!
        
        
        let dangerousTitleLbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        dangerousTitleLbl.text = events[dangerousTypeIndexSelected].dangerousTitle
        dangerousTitleLbl.textAlignment = .center
        dangerousTitleLbl.textColor = UIColor.orange
        dangerousTitleLbl.font = UIFont(name: "Arial-BoldMT", size: 24.0)
        dangerousTitleLbl.tintColor = UIColor.orange
        
        
        
        let noButton = UIButton(frame: CGRect(x: 20, y: 120, width: 75, height: 40))
        noButton.tag = dangerousTypeIndexSelected
        noButton.setTitle(NSLocalizedString("no", comment: ""), for: .normal)
        noButton.addTarget(self, action:  #selector(self.noButtonTapped(_:)), for: .touchUpInside)
        noButton.setTitleColor(ConstantColors.UserConfirmTitle, for: .normal)
        noButton.titleLabel?.font = ConstantFonts.UserConfirmFont
        noButton.backgroundColor = ConstantColors.UserConfirmBackgroud
        noButton.setBackgroundColor(color: UIColor.black, forState: .highlighted)
        let noTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.yesButtonTapped(_:)))
        noButton.addGestureRecognizer(noTapGesture)
        noButton.layer.cornerRadius = CGFloat(noButton.frame.height/3)
        
        
        let yesButton = UIButton(frame: CGRect(x: self.view.frame.width - 20 - 70, y: 120, width: 75, height: 40))
        yesButton.titleLabel?.font = ConstantFonts.UserConfirmFont
        yesButton.setBackgroundColor(color: UIColor.black, forState: .highlighted)
        yesButton.setTitleColor(ConstantColors.ComminButtonTitle, for: .normal)
        yesButton.backgroundColor = ConstantColors.UserConfirmBackgroud
        yesButton.addTarget(self, action:  #selector(self.yesButtonTapped(_:)), for: .touchUpInside)
        yesButton.setTitle(NSLocalizedString("yes", comment: ""), for: .normal)
        yesButton.tag = dangerousTypeIndexSelected
        
        
        let yesTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.yesButtonTapped(_:)))
        yesButton.addGestureRecognizer(yesTapGesture)
        yesButton.layer.cornerRadius = CGFloat(yesButton.frame.height/3)
        contentView.addSubview(noButton)
        contentView.addSubview(yesButton)
        contentView.addSubview(dangerousTitleLbl)
        
    }
    func noButtonTapped(_ sender: UITapGestureRecognizer)
    {
        let senderView = sender.view as! UIButton
        
        sendServerEventAnswer(answerYes: false, dangerousTypeIndexSelected: senderView.tag)
    }
    
    func yesButtonTapped(_ sender: UITapGestureRecognizer)
    {
        let senderView = sender.view as! UIButton
        sendServerEventAnswer(answerYes: true, dangerousTypeIndexSelected: senderView.tag)
    }
    
    func sendServerEventAnswer(answerYes: Bool, dangerousTypeIndexSelected: Int)
    {
        if (selectedLog?.dangerousType == Int32(events[dangerousTypeIndexSelected].dangerousType!))
        {
            self.backToLogView()
        }
        
        let feedbacksParams = FeedbacksParam()
        let dataProperties = DataProperties()
        dataProperties.carId = UserDefaults.standard.getCurrentCar()
        dataProperties.deviceId = UserDefaults.standard.getCurrentDevice()
        dataProperties.userId = UserDefaults.standard.getCurrentUser()
        dataProperties.drivingId = selectedLog?.drivingId
        feedbacksParams.dataProperties = dataProperties
        let feedback = Feedbacks()
        if (answerYes)
        {
            if (events[dangerousTypeIndexSelected].dangerousType! == 7)
            {
                feedback.dangerousDrivingType = Int32(99)
            }
            else
            {
                feedback.dangerousDrivingType = Int32(events[dangerousTypeIndexSelected].dangerousType!)
                
            }
        }
        else
        {
            feedback.dangerousDrivingType = Int32(99)
        }
        feedback.dangerousDrivingId = selectedLog?.dangerousDrivingId
        feedback.trigger = selectedLog?.dangerousTriggerType
        feedback.measurementDatetime = selectedLog?.measureTime
        feedbacksParams.feedbacks = []
        feedbacksParams.feedbacks?.append(feedback)
        
        ServerSender.sendFeedback(feedback: feedback) { (responseCode, errorMessage) -> (Void) in
            if (responseCode == 201 || responseCode == 200)
            {
            }
            else if (responseCode == 500)
            {
                self.showAlert(title: "Error", message: "Server Error \(responseCode)")
            }
            else
            {
                self.showAlert(title: "Error", message: "\(String(describing: errorMessage))")
            }
            
        }
        
        //  Update
        let log = DetailLogsModel()
        log.dangerousType = feedback.dangerousDrivingType
        log.drivingId = dataProperties.drivingId
        log.dangerousDrivingId = feedback.dangerousDrivingId
        log.measureTime = feedback.measurementDatetime
        log.dangerousTriggerType = Int32(DangerousTrigger.FromUser)
        
        Helper.updateLogData(detailLogUpdated: log)
        self.backToLogView()
    }
    
    func backToLogView()
    {
        self.performSegue(withIdentifier: "back_to_log_view_segue", sender: self)
    }
    
    
}
