//
//  ConfirmTableViewCell.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/15.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class ConfirmTableViewCell: UITableViewCell {

    @IBOutlet weak var carIconImg: UIImageView!
    @IBOutlet weak var eventMessage: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
