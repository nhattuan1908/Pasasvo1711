//
//  MachineCheckViewController.swift
//  SensorsLog
//
//  Created by Unitec on 11/15/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit


class HumanCheckViewController: UIViewController {
    @IBOutlet weak var Content: UILabel!
    @IBOutlet weak var None: UIButton!
    @IBOutlet weak var AbruptHandle: UIButton!
    @IBOutlet weak var SuddenStart: UIButton!
    @IBOutlet weak var RapidAcceleration: UIButton!
    @IBOutlet weak var SuddenBraking: UIButton!
    @IBOutlet weak var Wobble: UIButton!
    @IBOutlet weak var Bump: UIButton!
    @IBOutlet weak var Cancel: UIButton!
    @IBAction func BackToLogs(_ sender: UIButton) {
        self.performSegue(withIdentifier: "show_back_to_logs_view", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setButton(button: None, title: "None")
        setButton(button: AbruptHandle, title: "AbruptHandle")
        setButton(button: SuddenStart, title: "SuddenStart")
        setButton(button: RapidAcceleration, title: "RapidAcceleration")
        setButton(button: SuddenBraking, title: "SuddenBraking")
        setButton(button: Wobble, title: "Wobble")
        setButton(button: Bump, title: "Bump")
        setButtonCancel()
        Content.text = "2017/11/26 11:11:11"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.title = NSLocalizedString("Logs", comment: "")
    }
    
    func setButton(button: UIButton, title: String)
    {
        button.layer.cornerRadius = 7
        button.backgroundColor  = ConstantColors.UserEventBackground
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle(NSLocalizedString(title, comment: ""), for: .normal)
        
    }
    func setButtonCancel()
    {
        Cancel.layer.cornerRadius = Cancel.frame.height/2
        Cancel.backgroundColor = ConstantColors.CommonButtonBackGround
        Cancel.setTitleColor(UIColor.white, for: .normal)
        Cancel.setTitle(NSLocalizedString("OperationEnd", comment: ""), for: .normal)
    }
}
