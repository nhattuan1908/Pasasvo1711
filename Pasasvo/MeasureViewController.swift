//
//  MeasureViewController.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

import PasasvoSDK

var logsTestMode:String = ""
var sensorsTestMode:String = ""
var feedbacksTestMode:String = ""

class MeasureViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var measureBtn: UIBarButtonItem!
    var testMode: Bool?
    var getDangerousTimer: Timer!
    @IBOutlet weak var stopButton: UIButton!
    var events = [DangerousType]()
    
    var indicatorActivity: UIActivityIndicatorView!
    var currentEvent: DangerousDrivingReturn?
    //var Flag:Bool = true
    var userSelected = false
    var dangerousShowing = false
    
    var optionButtonHeight = CGFloat(60)
    
    var dangerousShowedTime = 0.0
    
    
    @IBAction func StopOperation(_ sender: UIButton) {
        self.showActivityIndicatory()
        Pasasvo.stopDataCollect { (responseCode, errorMessage) -> (Void) in
            self.stopActivityIndicatory()
            if (responseCode == 201 || responseCode == 200)
            {
                self.performSegue(withIdentifier: "show_result_segue", sender: self)
            }
            else
            {
                if (errorMessage?.range(of: "The Internet connection appears to be offline") != nil)
                {
                    self.showAlert(title: "Error", message: "\(responseCode):  Network error")
                    return
                }
                else if (errorMessage?.range(of: "The request timed out.") != nil)
                {
                    self.showAlert(title: "Error", message: "\(responseCode):  Server time out")
                    return
                }
                else
                {
                    self.showAlert(title: "Error", message: "\(responseCode) \(String(describing: errorMessage))")
                    return
                }
            }
            flag = true
            logsTestMode = " "
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dangerousShowing = false
        _ = Helper.deleteAllLogData()
        PasasvoConfig.setSystemCallback { (code, result, error) -> (Void) in
            if (code == 201)
            {
                sensorsTestMode = Helper.getTimeString() + " Sensor: " +  "\(String(describing: result))"
                self.tableView.reloadData()
            }
            else if(code == 101)
            {
                flag = true
                if (result == 200 || result == 201)
                {
                    self.performSegue(withIdentifier: "show_result_segue", sender: self)
                    self.showAlert(title: "Notification", message: "Maxtime time out")
                }
                else
                {
                    if (error?.range(of: "The Internet connection appears to be offline") != nil)
                    {
                        self.showAlert(title: "Error", message: "\(code):  Network error")
                        return
                    }
                    else if (error?.range(of: "The request timed out.") != nil)
                    {
                        self.showAlert(title: "Error", message: "\(code):  Server time out")
                        return
                    }
                    else
                    {
                        self.showAlert(title: "Error", message: "\(code) \(String(describing: error))")
                        return
                    }

                }
            }
        }
        
        PasasvoConfig.setDangerousCallback(){ (code, result, error) in
            if(result != nil)
            {
                self.currentEvent?.dangerousDriving?.append(result!)
                logsTestMode = Helper.getTimeString() + " Dangerous: " + "\(String(describing: code))"
                
                if self.dangerousShowing == false {
                    self.tableView.reloadData()
                }
                
            }
            else
            {
                logsTestMode = Helper.getTimeString() + " Dangerous: " + "\(String(describing: code)))"
            }
            
        }
        
        
        self.tableView.separatorStyle = .none
        currentEvent = DangerousDrivingReturn()
        currentEvent?.dangerousDriving = []
        
        self.tableView.isScrollEnabled = false
        self.tableView.delaysContentTouches = false
        
        
        for case let scrollView as UIScrollView in tableView.subviews {
            scrollView.canCancelContentTouches = false
        }
        
        
        events.append(DangerousType(dangerousType: 2, dangerousTitle: "Hard Acceleration"))
        events.append(DangerousType(dangerousType: 3, dangerousTitle: "Hard braking"))
        events.append(DangerousType(dangerousType: 4, dangerousTitle: "Sudden Steering"))
        events.append(DangerousType(dangerousType: 5, dangerousTitle: "Unsteady Driving"))
        events.append(DangerousType(dangerousType: 6, dangerousTitle: "Bump"))
        self.automaticallyAdjustsScrollViewInsets = false
        
        
        self.navigationItem.title = NSLocalizedString("measure", comment: "")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint(item: self.stopButton, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: -CGFloat(20)),
                NSLayoutConstraint(item: self.stopButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 45),
                NSLayoutConstraint(item: self.stopButton, attribute: .left , relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 50),
                NSLayoutConstraint(item: self.stopButton, attribute: .right , relatedBy: .equal, toItem: self.stopButton, attribute: .right, multiplier: 1, constant: -50),
                ])
        self.stopButton.setTitle(NSLocalizedString("stop_measure", comment: ""), for: .normal)
        self.stopButton.setTitleColor(ConstantColors.ComminButtonTitle, for: .normal)
        self.stopButton.backgroundColor = ConstantColors.CommonButtonBackGround
        self.stopButton.layer.cornerRadius = CGFloat(self.stopButton.frame.height/3)
        self.stopButton.titleLabel?.font = ConstantFonts.CommitFont
        self.view.addSubview(stopButton)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    func loadEvent()
    {
        
    }
    
    func navigateToMeasureResultView()
    {
        
        self.getDangerousTimer.invalidate()
        self.performSegue(withIdentifier: "show_measure_result_segue", sender: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension MeasureViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func noButtonTapped()
    {
        
        sendServerEventAnswer(answerYes: false)
    }
    
    func yesButtonTapped()
    {
        sendServerEventAnswer(answerYes: true)
    }
    
    func buttonNotTapped()
    {
        self.userSelected = true
        
        if (self.currentEvent?.dangerousDriving!.count == 0)
        {
            return
                self.tableView.reloadData()
        }
        let feedback = Feedbacks()
        feedback.dangerousDrivingType = Int32(99)
        feedback.dangerousDrivingId = UUID().uuidString
        feedback.trigger = Int32(DangerousTrigger.FromServer)
        
        feedback.measurementDatetime = Helper.parseDateTime(dateTimeString: (currentEvent?.dangerousDriving?[0].measurementDatetime)!)
       
        let log = DetailLogsModel()
        log.dangerousType = Int32((currentEvent?.dangerousDriving?[0].dangerousDrivingType ?? 0)!)
        log.drivingId = PasasvoConfig.drivingId
        log.dangerousDrivingId = feedback.dangerousDrivingId
        log.measureTime = feedback.measurementDatetime
        log.dangerousTriggerType = Int32( DangerousTrigger.FromServer)
        log.answerType = Int32(AnswerType.no)
        
        Helper.insertDetailDataLogs(detailsLog: log)
        
        if ((self.currentEvent?.dangerousDriving?.count)! > 0)
        {
            self.currentEvent?.dangerousDriving?.remove(at: 0)
        }
        
        self.dangerousShowing = false
        self.tableView.reloadData()
        
    }
    
    func sendServerEventAnswer(answerYes: Bool)
    {
        
        
        self.userSelected = true
        if (self.currentEvent?.dangerousDriving!.count == 0)
        {
            return
                self.tableView.reloadData()
        }
        
        
        let feedback = Feedbacks()
        if (answerYes)
        {
            feedback.dangerousDrivingType = Int32((currentEvent?.dangerousDriving?[0].dangerousDrivingType ?? 0)!)
        }
            
        else
        {
            feedback.dangerousDrivingType = Int32(99)
        }
        
        
        feedback.dangerousDrivingId = UUID().uuidString
        feedback.trigger = Int32(DangerousTrigger.FromServer)
        
        feedback.measurementDatetime = Helper.parseDateTime(dateTimeString: (currentEvent?.dangerousDriving?[0].measurementDatetime)!)
        
        ServerSender.sendFeedback(feedback: feedback){(code, message) in
            feedbacksTestMode = Helper.getTimeString() + " Feedbacks: " + "\(String(describing: code))"
            self.dangerousShowing = false
            self.tableView.reloadData()
        }
        
        
        let log = DetailLogsModel()
        log.dangerousType = Int32((currentEvent?.dangerousDriving?[0].dangerousDrivingType ?? 0)!)
        log.drivingId = PasasvoConfig.drivingId
        log.dangerousDrivingId = feedback.dangerousDrivingId
        log.measureTime = feedback.measurementDatetime
        log.dangerousTriggerType = Int32( DangerousTrigger.FromServer)
        if (feedback.dangerousDrivingType == Int32(99))
        {
            log.answerType = Int32(AnswerType.no)
        }
        else
        {
            log.answerType = Int32(AnswerType.yes)
        }
        
        Helper.insertDetailDataLogs(detailsLog: log)
        
        if ((self.currentEvent?.dangerousDriving?.count)! > 0)
        {
            self.currentEvent?.dangerousDriving?.remove(at: 0)
        }
//        self.tableView.reloadData()
//        self.dangerousShowing = false
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            if (currentEvent?.dangerousDriving?.count == 0)
            {
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                return cell
            }
            else
            {
                if ((currentEvent?.dangerousDriving?.count)! > 0)
                {
                    self.userSelected = false
                    
                    dangerousShowedTime = NSDate().timeIntervalSince1970
                    
                    let cell = Bundle.main.loadNibNamed("ConfirmTableViewCell", owner: self, options: nil)?.first as! ConfirmTableViewCell
                    cell.eventMessage.text = AppConfig.DangerousType[Int((currentEvent?.dangerousDriving?[0].dangerousDrivingType)!)]
                    
                    cell.selectedBackgroundView?.backgroundColor = UIColor.white
                    cell.selectionStyle = .default
                    cell.carIconImg.image = UIImage(named: "car")
                    
                    cell.yesButton.setTitle(NSLocalizedString("yes", comment: "") , for: .normal)
                    cell.yesButton.layer.cornerRadius = CGFloat(10)
                    cell.yesButton.setTitleColor(ConstantColors.UserConfirmTitle, for: .normal)
                    cell.yesButton.titleLabel?.font = ConstantFonts.UserConfirmFont
                    cell.yesButton.backgroundColor = ConstantColors.UserConfirmBackgroud
                    cell.yesButton.setBackgroundColor(color: UIColor.black, forState: .highlighted)
                    
                    cell.yesButton.addTarget(self, action:  #selector(self.yesButtonTapped), for: .touchUpInside)
                    let heightContraintsYes = NSLayoutConstraint(item: cell.yesButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 40)
                    
                    cell.noButton.layer.cornerRadius = CGFloat(10)
                    cell.noButton.setTitle(NSLocalizedString("no", comment: "") , for: .normal)
                    cell.noButton.titleLabel?.font = ConstantFonts.UserConfirmFont
                    cell.noButton.setBackgroundColor(color: UIColor.black, forState: .highlighted)
                    cell.noButton.setTitleColor(ConstantColors.ComminButtonTitle, for: .normal)
                    cell.noButton.backgroundColor = ConstantColors.UserConfirmBackgroud
                    cell.noButton.addTarget(self, action:  #selector(self.noButtonTapped), for: .touchUpInside)
                    let heightContraintsNo = NSLayoutConstraint(item: cell.noButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 40)
                    NSLayoutConstraint.activate([heightContraintsYes,heightContraintsNo])
                    
                    
                    if dangerousShowing == false {
                        dangerousShowing = true
                        startTimer()
                    }
                    
                    
                    
                    
                    cell.selectionStyle = .none
                    return cell
                }
                else
                {
                    return UITableViewCell()
                }
            }
        }
        else if indexPath.row == 1
        {
            let cell = Bundle.main.loadNibNamed("SearchOptionTableViewCell", owner: self, options: nil)?.first as! SearchOptionTableViewCell
            cell.selectedBackgroundView?.backgroundColor = UIColor.white
            cell.selectionStyle = .none
            let image_distance: CGFloat =  10
            var x: CGFloat = 10
            var y: CGFloat = 10
            let height : CGFloat = optionButtonHeight
            let item_in_row = 2
            let total = events.count
            let a = total - total%item_in_row
            let b = total%item_in_row
            var current_item_row = item_in_row
            let cornerRadius = CGFloat(7)
            for i in (1...total){
                if (i > a) {
                    current_item_row = b
                }
                let item_width: CGFloat = (self.tableView.frame.width - image_distance*(CGFloat(current_item_row)+1))/CGFloat(current_item_row)
                let optionButton = UIButton(frame: CGRect(x: x, y: y, width: item_width, height: height))
                optionButton.layer.cornerRadius = 0.2
                optionButton.alpha = CGFloat(0.5)
                optionButton.isUserInteractionEnabled = true
                optionButton.tag = i-1
                optionButton.layer.cornerRadius = cornerRadius
                optionButton.layer.masksToBounds = true
                optionButton.layer.borderWidth =  2
                optionButton.layer.borderColor = ConstantColors.UserEventBorder.cgColor
                optionButton.setTitle(self.events[i-1].dangerousTitle, for: .normal)
                optionButton.setTitleColor(ConstantColors.UserEventTitle, for: .normal)
                optionButton.setTitleColor(UIColor.white, for: .highlighted)
                optionButton.titleLabel?.font = UIFont(name: "Avenir-Black", size: 15)
                optionButton.backgroundColor = ConstantColors.UserEventBackground
                optionButton.setBackgroundColor(color: ConstantColors.UserConfirmBackgroud, forState: .highlighted)
                let recognizer = UITapGestureRecognizer(target: self, action: #selector(MeasureViewController.handleUserEventSelected(_:)))
                optionButton.addGestureRecognizer(recognizer)
                cell.contentView.addSubview(optionButton)
                
                let c = i%item_in_row
                x = image_distance +  (image_distance + item_width)*CGFloat(c)
                if (i % Int(item_in_row) == 0)
                {
                    x = image_distance
                    y = y + height + image_distance
                }
            }
            cell.backgroundColor = ConstantColors.DialogBackground
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "measure_cell", for: indexPath) as! MeasureTableViewCell
            cell.selectionStyle = .none
            if(UserDefaults.standard.getRealtimeLogSettings() == true)
            {
                cell.textViewMeasure.isHidden = false
            }
            else
            {
                cell.textViewMeasure.isHidden = true
            }
            let range = NSMakeRange(cell.textViewMeasure.text.characters.count - 1, 0)
            cell.textViewMeasure.scrollRangeToVisible(range)
            if  (sensorsTestMode != "")
            {
            cell.textViewMeasure.text = cell.textViewMeasure.text + "\n" + sensorsTestMode
                sensorsTestMode = ""
            }
            if  (logsTestMode != "")
            {
                cell.textViewMeasure.text = cell.textViewMeasure.text + "\n" + logsTestMode
                logsTestMode = ""
            }
            if  (feedbacksTestMode != "")
            {
                cell.textViewMeasure.text = cell.textViewMeasure.text + "\n" + feedbacksTestMode
                feedbacksTestMode = ""
            }
            return cell
        }
    }
    
//    func startTimer() {
//        let watingQueue = DispatchQueue(label: "wating_queue")
//        watingQueue.async {
//            var finished = false
//            DispatchQueue.global().async {
//                sleep(UInt32(5))
//                finished = true
//            }
//            while(!self.userSelected && !finished)
//            {}
//            if (!self.userSelected)
//            {
//                self.buttonNotTapped()
//                
//            }
//        }
//    }
    
    func startTimer() {
        Timer.after(5) {
            let now = NSDate().timeIntervalSince1970
            
            if (now - self.dangerousShowedTime) < 4.9 {
                //return
            }
            
            if (!self.userSelected)
            {
                self.buttonNotTapped()
            }
        }
    }
    
    func handleUserEventSelected(_ sender: UITapGestureRecognizer) {
        let viewTapped =  sender.view as! UIButton
        let feedback = Feedbacks()
        feedback.dangerousDrivingType = Int32(events[viewTapped.tag].dangerousType!)
        feedback.dangerousDrivingId = UUID().uuidString
        feedback.trigger = Int32(DangerousTrigger.FromUser)
        feedback.measurementDatetime = Helper.getTodayString()
        
        let log = DetailLogsModel()
        log.dangerousType = feedback.dangerousDrivingType!
        log.drivingId = PasasvoConfig.drivingId
        log.dangerousDrivingId = feedback.dangerousDrivingId
        log.measureTime = feedback.measurementDatetime
        log.dangerousTriggerType = Int32(DangerousTrigger.FromUser)
        log.answerType = Int32(AnswerType.yes)
        Helper.insertDetailDataLogs(detailsLog: log)
        
        ServerSender.sendFeedback(feedback: feedback){(code, message) in
             feedbacksTestMode = Helper.getTimeString() + " Feedbacks: " + "\(String(describing: code))"
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0)
        {
            return CGFloat(200)
        }
        else if (indexPath.row == 1)
        {
            var rowNumber : Int!
            if (self.events.count%2 == 0)
            {
                rowNumber = Int(self.events.count/2)
            }
            else
            {
                rowNumber = (self.events.count/2) + 1
            }
            return CGFloat(optionButtonHeight*CGFloat(rowNumber) + 10*(CGFloat(rowNumber+1)))
        }
        else
        {
            return CGFloat(65)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
}
