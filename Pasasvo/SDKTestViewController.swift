//
//  SDKTestViewController.swift
//  Pasasvo
//
//  Created by unitec-MAC on 12/14/17.
//  Copyright © 2017 unitec-MAC. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import PasasvoSDK

let carTest = Car()
let carUpdate = Car()
let deviceTest = Device()
let deviceUpdate = Device()
let profileTest = Profile()
let profileUpdate = Profile()
let test = UserModel()
let testUpdate = UserModel()
let loginTest = Login()
var userIdReturn:String = ""
class SDKTestViewController: FormViewController {
    
    var saveSettingsBtn: UIBarButtonItem!
    var backSettingsBtn: UIBarButtonItem!
    var allRulesPassed = true
    var testmode: Bool = false
    
    func save() {
        test.loginId = "123456"
        test.nickname = "124567"
        test.password = "12467"
        test.userId = "12345789"
        
        testUpdate.loginId = "1234562"
        testUpdate.nickname = "1245267"
        testUpdate.password = "124627"
        testUpdate.userId = "123457829"
        
        carTest.carEngineSizeId = "123456"
        carTest.carEngineType = "111111"
        carTest.carMakerId = "123456"
        carTest.carModelName = "123456"
        carTest.carType = "123456"
        carTest.carModelYear = 2017
        carTest.memo = "ios"
        
        carUpdate.carEngineSizeId = "1234526"
        carUpdate.carEngineType = "11112"
        carUpdate.carMakerId = "12345697"
        carUpdate.carModelName = "1234526"
        carUpdate.carType = "1234356"
        carUpdate.carModelYear = 2017
        carUpdate.memo = "ios"
        
        deviceTest.deviceId = "123456"
        deviceTest.deviceModelNo = "123456"
        deviceTest.deviceOsType = 1
        deviceTest.deviceOsVer = "123456"
        deviceTest.memo = "ios"
        
        deviceUpdate.deviceId = "1234256"
        deviceUpdate.deviceModelNo = "1234526"
        deviceUpdate.deviceOsType = 1
        deviceUpdate.deviceOsVer = "1234256"
        deviceUpdate.memo = "ios"
        
        //        loginTest.loginId = "unitec"
        //        loginTest.password = "1234"
        //        loginTest.userId = "123"
        
        profileTest.nickname = "123456"
        profileUpdate.nickname = "1232456"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setButtonBack()
        self.navigationItem.title  = NSLocalizedString("API Test", comment: "")
        var rules = RuleSet<String>()
        rules.add(rule: RuleRequired())
        self.save()
        
        form +++
            Section("")
            <<< SegmentedRow<String>("Segment")
            {
                $0.options = ["User", "Car", "Device", "Profile", "History"]
                $0.value = "User"
            }
            <<< TextRow("UserId")
            {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User" || (form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car" || (form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device" || (form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "History")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "UserId"
                $0.value = UserDefaults.standard.getUserId()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                    self.saveSDK()
                })
            <<< TextRow("LoginId")
            {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "LoginId"
                $0.value = UserDefaults.standard.getLoginId()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                    self.saveSDK()
                })
            <<< TextRow("Password")
            {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Password"
                $0.value = UserDefaults.standard.getPassword()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                    self.saveSDK()
                })
            <<< TextRow("CarId")
            {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car" || (form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "History")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "CarId"
                $0.value = UserDefaults.standard.getCarId()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                    self.saveSDK()
                })
            <<< TextRow("DeviceId")
            {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device" || (form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "History")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "DeviceId"
                $0.value = UserDefaults.standard.getDeviceId()
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                    self.saveSDK()
                })
            <<< TextRow("DrivingId")
            {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "History")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "DrivingId"
                $0.value = PasasvoConfig.drivingId
                }.cellUpdate({ (cell, row) in
                    if (row.value == nil)
                    {
                        row.value = ""
                    }
                    self.saveSDK()
                })
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get user"
                }.onCellSelection({ (cell, row) in
                    User.getUser(userId: UserDefaults.standard.getUserId(), callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                            
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: " LoginId: \(String(describing: data?.loginId))\n Nickname: \(String(describing: data?.nickname))\n Password: \(String(describing: data?.password))\n UserId: \(String(describing: (data?.userId)))")
                        }
                    })
                        
                })
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get user list"
                }.onCellSelection({ (cell, row) in
                    User.getUserList(callback: { (response, data, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            var Content:String = ""
                            if((data?.users != nil))
                            {
                                for i in 0...((data?.users?.count)! - 1)
                                {
                                    Content += "LoginId: \(String(describing: data?.users?[i].loginId))\n Nickname: \(String(describing: data?.users?[i].nickname))\n Password: \(String(describing: data?.users?[i].password)))\n UserId: \(String(describing: (data?.users?[i].userId)))"
                                }
                                self.showAlert(title: "Success", message: "\(Content)")
                            }
                            else
                            {
                                self.showAlert(title: "Error", message: " Server return wrong format data")
                            }
                            
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Register user"
                }.onCellSelection({ (cell, row) in
                    User.registerUser(user: test, callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            UserDefaults.standard.setUserId(value: data)
                            self.showAlert(title: "Success", message: "\(String(describing: data))")
                            let UserIdText: TextRow = self.form.rowBy(tag: "UserId")!
                            UserIdText.value = UserDefaults.standard.getUserId()
                            UserIdText.updateCell()
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Update user"
                }.onCellSelection({ (cell, row) in
                    User.updateUser(userId: UserDefaults.standard.getUserId(), user: testUpdate, callback: { (response,error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Delete user"
                }.onCellSelection({ (cell, row) in
                    User.deleteUser(userId: UserDefaults.standard.getUserId(), callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "User")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Login"
                }.onCellSelection({ (cell, row) in
                    loginTest.loginId = UserDefaults.standard.getLoginId()
                    loginTest.password = UserDefaults.standard.getPassword()
                    loginTest.userId = "123"
                    User.login(login: loginTest, callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Notifications", message: "\(String(describing: error))")
                        }
                        else
                        {
                            UserDefaults.standard.setUserId(value: data)
                            self.showAlert(title: "Notifications", message: "\(String(describing: data))")
                            let UserIdText: TextRow = self.form.rowBy(tag: "UserId")!
                            UserIdText.value = UserDefaults.standard.getUserId()
                            UserIdText.updateCell()
                        }
                    })

                    })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get car"
                }.onCellSelection({ (cell, row) in
                    User.getCar(userId: UserDefaults.standard.getUserId(), carId: UserDefaults.standard.getCarId(), callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "Memo: \(String(describing: data?.memo))\n CarMakerId: \(String(describing: data?.carMakerId))\n CarType: \(String(describing: data?.carType))\n CarModelName: \(String(describing: data?.carModelName))\n CarEngineType: \(String(describing: data?.carEngineType))\n CarEngineSizeId: \(String(describing: data?.carEngineSizeId))\n CarModelYear\(String(describing: data?.carModelYear))")
    
                                }
                        })

    
                    })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get car list"
                }.onCellSelection({ (cell, row) in
                    User.getCarList(userId: UserDefaults.standard.getUserId(), callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                            
                        }
                        else
                        {
                            var Content:String = ""
                            if (data?.cars != nil)
                            {
                                for i in 0...((data?.cars?.count)! - 1)
                                {
                                    Content += "Memo: \(String(describing: data?.cars?[i].memo))\n CarMakerId: \(String(describing: data?.cars?[i].carMakerId))\n CarType: \(String(describing: data?.cars?[i].carType))\n CarModelName: \(String(describing: data?.cars?[i].carModelName))\n CarEngineType: \(String(describing: data?.cars?[i].carEngineType))\n CarEngineSizeId: \(String(describing: data?.cars?[i].carEngineSizeId))\n CarModelYear\(String(describing: data?.cars?[i].carModelYear))\n CarId: \(String(describing: data?.cars?[i].carId))"
                                }
                                self.showAlert(title: "Success", message: "\(String(describing: Content))")
                            }
                            else
                            {
                                self.showAlert(title: "Error", message: " Server return wrong format data")
                            }
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Register car"
                }.onCellSelection({ (cell, row) in
                    User.registerCar(userId: UserDefaults.standard.getUserId(), carId: UserDefaults.standard.getCarId(), car: carTest, callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Update car"
                }.onCellSelection({ (cell, row) in
                    User.updateCar(userId: UserDefaults.standard.getUserId(), carId: UserDefaults.standard.getCarId(), car: carUpdate, callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                            print("\(String(describing: carUpdate.carEngineSizeId)),\(String(describing: carUpdate.carEngineType)), \(String(describing: carUpdate.carType)), \(String(describing: carUpdate.carMakerId)), \(String(describing: carUpdate.carModelName)), \(String(describing: carUpdate.carModelName))")
                            
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Delete car"
                }.onCellSelection({ (cell, row) in
                    User.deleteCar(userId: UserDefaults.standard.getUserId(), carId: UserDefaults.standard.getCarId(), callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Car")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Delete car list"
                }.onCellSelection({ (cell, row) in
                    User.deleteCarList(userId: UserDefaults.standard.getUserId(), callback: { (response,error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get device"
                }.onCellSelection({ (cell, row) in
                    User.getDevice(userId: UserDefaults.standard.getUserId(), deviceId: UserDefaults.standard.getDeviceId(), callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "DeviceId: \(String(describing: data?.deviceId))\n DeviceOsType: \(String(describing: data?.deviceOsType))\n DeviceOsVer: \(String(describing: data?.deviceOsVer))\n DeviceModelNo: \(String(describing: data?.deviceModelNo))\n Memo: \(String(describing: data?.memo))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get device list"
                }.onCellSelection({ (cell, row) in
                    User.getDeviceList(userId: UserDefaults.standard.getUserId(), callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            var Content:String = ""
                            if(data?.devices != nil)
                            {
                                for i in 0...((data?.devices?.count)! - 1)
                                {
                                    Content += "DeviceId: \(String(describing: data?.devices?[i].deviceId))\n DeviceOsType: \(String(describing: data?.devices?[i].deviceOsType))\n DeviceOsVer: \(String(describing: data?.devices?[i].deviceOsVer))\n DeviceModelNo: \(String(describing: data?.devices?[i].deviceModelNo))\n Memo: \(String(describing: data?.devices?[i].memo))"
                                }
                                self.showAlert(title: "Success", message: "\(String(describing: Content))")
                            }
                            else
                            {
                                self.showAlert(title: "Error", message: "Server return wrong format data")
                            }
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Register device"
                }.onCellSelection({ (cell, row) in
                    User.registerDevice(userId: UserDefaults.standard.getUserId(), deviceId: UserDefaults.standard.getDeviceId(), device: deviceTest, callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Update device"
                }.onCellSelection({ (cell, row) in
                    User.updateDevice(userId: UserDefaults.standard.getUserId(), deviceId: UserDefaults.standard.getDeviceId(), deviceData: deviceUpdate, callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Delete device"
                }.onCellSelection({ (cell, row) in
                    User.deleteDevice(userId: UserDefaults.standard.getUserId(), deviceId: UserDefaults.standard.getDeviceId(), callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Device")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Delete device list"
                }.onCellSelection({ (cell, row) in
                    User.deleteDeviceList(userId: UserDefaults.standard.getUserId(), callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                        
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Profile")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get profile"
                }.onCellSelection({ (cell, row) in
                    User.getProfile(userId: UserDefaults.standard.getUserId(), callback: { (response, data, error) -> (Void) in
                        if(response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            
                            self.showAlert(title: "Success", message: "Nickname: \(String(describing: data?.nickname))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Profile")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Register profile"
                }.onCellSelection({ (cell, row) in
                    User.registerProfile(userId: UserDefaults.standard.getUserId(), profile: profileTest, callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Profile")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Update profile"
                }.onCellSelection({ (cell, row) in
                    User.updateProfile(userId: UserDefaults.standard.getUserId(), profile: profileUpdate, callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "Profile")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Delete profile"
                }.onCellSelection({ (cell, row) in
                    User.deleteProfile(userId: UserDefaults.standard.getUserId(), callback: { (response, error) -> (Void) in
                        if (response != 200)
                        {
                            self.showAlert(title: "Error", message: "\(String(describing: error))")
                        }
                        else
                        {
                            self.showAlert(title: "Success", message: "\(String(describing: response))")
                        }
                    })
                })
            
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "History")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get history"
                }.onCellSelection({ (cell, row) in
                    ServerGetter.getHistory(drivingId: UserDefaults.standard.getDrivingId(),completeHanlder: { (code, error, data) -> (Void) in
                        if(data == nil)
                        {
                            self.showAlert(title: "Error", message:"\(String(describing: error))" )
                        }
                        else
                        {
                            var Content:String = ""
                            for i in 0...((data?.count)! - 1)
                            {
                               Content += "Average Speed: \(String(describing: data?[i].drivingData?.first?.averageSpeed))\n Driving Distance: \(String(describing: data?[i].drivingData?.first?.drivingDistance))\n Driving Start Time: \(String(describing: data?[i].drivingData?.first?.drivingStartDatetime))\n Driving Id: \(String(describing: data?[i].drivingData?.first?.drivingId))\n Driving Status: \(String(describing: data?[i].drivingData?.first?.drivingStatus))\n Driving Time: \(String(describing: data?[i].drivingData?.first?.drivingTime))\n Memo: \(String(describing: data?[i].drivingData?.first?.memo))\n "
                            }
                            self.showAlert(title: "Success", message: "\(Content)" )
                        }
                    })
                })
            
            
            <<< ButtonRow() {
                $0.hidden = Condition.function(["Segment"], { form in
                    if((form.rowBy(tag: "Segment") as? SegmentedRow)?.value == "History")
                    {
                        return false
                    }
                    return true
                })
                $0.title = "Get history summary"
                }.onCellSelection({ (cell, row) in
                    ServerGetter.getHistoriesSummary(completeHanlder: { (code, error, data) -> (Void) in
                        if(data == nil)
                        {
                            self.showAlert(title: "Error", message:"\(String(describing: error))" )
                        }
                        else
                        {
                            var Content:String = ""
                            for i in 0...((data?.count)! - 1)
                            {
                                Content += "Average Speed: \(String(describing: data?[i].drivingData?.first?.averageSpeed))\n Driving Distance: \(String(describing: data?[i].drivingData?.first?.drivingDistance))\n Driving Start Time: \(String(describing: data?[i].drivingData?.first?.drivingStartDatetime))\n Driving Id: \(String(describing: data?[i].drivingData?.first?.drivingId))\n Driving Status: \(String(describing: data?[i].drivingData?.first?.drivingStatus))\n Driving Time: \(String(describing: data?[i].drivingData?.first?.drivingTime))\n Memo: \(String(describing: data?[i].drivingData?.first?.memo))\n "
                            }
                            self.showAlert(title: "Success", message: "\(Content)" )
                        }
                    })
                })
        
    }
    func saveSDK()
    {
        
        let error = form.validate()
        if (error.count > 0)
        {
            self.showToast(message: NSLocalizedString("enter_all_settings_required", comment: ""), textColor: UIColor.white)
            return
        }
        
        let userIdRow: TextRow? = form.rowBy(tag: "UserId")
        let carIdRow: TextRow? = form.rowBy(tag: "CarId")
        let deviceIdRow: TextRow? = form.rowBy(tag: "DeviceId")
        let drivingIdRow: TextRow? = form.rowBy(tag: "DrivingId")
        let loginIdRow: TextRow? = form.rowBy(tag: "LoginId")
        let passwordRow: TextRow? = form.rowBy(tag: "Password")
        
        UserDefaults.standard.setUserId(value: userIdRow?.value)
        UserDefaults.standard.setCarId(value: carIdRow?.value)
        UserDefaults.standard.setDeviceId(value: deviceIdRow?.value)
        UserDefaults.standard.setDrivingId(value: drivingIdRow?.value)
        UserDefaults.standard.setLoginId(value: loginIdRow?.value)
        UserDefaults.standard.setPassword(value: passwordRow?.value)
        
        PasasvoConfig.userId = UserDefaults.standard.getUserId()
        PasasvoConfig.carId = UserDefaults.standard.getCarId()
        PasasvoConfig.deviceId = UserDefaults.standard.getDeviceId()
        PasasvoConfig.drivingId = UserDefaults.standard.getDrivingId()
        
    }
    
    func setButtonBack() {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(SDKTestViewController.backSettings), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
    }
    
    func backSettings()
    {
        self.performSegue(withIdentifier: "show_back_to_setting_view", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
