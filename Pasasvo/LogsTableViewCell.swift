//
//  LogsTableViewCell.swift
//  SensorsLog
//
//  Created by Unitec on 11/7/17.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class LogsTableViewCell: UITableViewCell {

    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var eventLogs: UILabel!
    @IBOutlet weak var checkResult: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        setButtonCheck()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setButtonCheck()
    {
        checkResult.layer.cornerRadius = checkResult.frame.height/2
        checkResult.backgroundColor = UIColor.green
        checkResult.setTitleColor(UIColor.black, for: .normal)
        checkResult.setTitle(NSLocalizedString("checkResult", comment:""), for: .normal)
    }

}
