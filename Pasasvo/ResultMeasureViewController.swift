//
//  ResultMeasureViewController.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/11/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class ResultMeasureViewController: UIViewController {

    var backToMain: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("measure_result", comment: "")
        backToMain = UIBarButtonItem(title: NSLocalizedString("home", comment: ""), style: .done, target: self, action: #selector(self.backToMainView))
        self.navigationItem.leftBarButtonItem = backToMain
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backToMainView()
    {
        self.performSegue(withIdentifier: "result_back_to_main_segue", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
