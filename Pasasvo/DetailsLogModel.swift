//
//  DetailsLogModel.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/12/03.
//  Copyright © 2017 unitec. All rights reserved.
//


open class DetailLogsModel
{
    
    public var dangerousDrivingId: String?
    public var dangerousTriggerType: Int32?
    public var dangerousType: Int32?
    public var drivingId: String?
    public var measureTime: String?
    public var answerType: Int32?
    public init() {}


}
