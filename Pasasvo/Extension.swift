//
//  Extension.swift
//  SensorsLog
//
//  Created by Unitec on 2017/07/28.
//  Copyright © 2017 unitec. All rights reserved.
//

import Foundation
import UIKit
import PasasvoSDK

extension UserDefaults
{

    func setCurrentDate(dateStr: String)
    {
        UserDefaults.standard.set(dateStr, forKey: "current_date_str")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentDate() -> String?
    {
        return UserDefaults.standard.string(forKey: "current_date_str")
    }

        func setCurrentIndexFile(index: Int)
    {
        UserDefaults.standard.set(index, forKey: "current_file_index")
        UserDefaults.standard.synchronize()
    
    }
    
    func getCurrentIndexFile() -> Int {
        return UserDefaults.standard.integer(forKey: "current_file_index")
    }
    
    // Get - Set Car model 
    func getCarModelSettings() -> String
    {
        return UserDefaults.standard.string(forKey: "car_model_settings") ?? ""
    
    }
    
    func setCarModelSettings(value: String)
    {
        UserDefaults.standard.set(value, forKey: "car_model_settings")
        
    }
    
    
    
    // Set - Get Distance Calculator Interval
    func getDistanceCalculatorIntervalSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "distance_calculator_interval_settings")
        
    }
    func setDistanceCalculatorIntervalSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "distance_calculator_interval_settings")
    }
    

    // Set - Get Distance Calculator Interval
    func getSampleIntervalSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "sample_interval_settings")
        
    }
    func setSampleIntervalSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "sample_interval_settings")
    }
  

    
    // Get Port settings 
    func getPortSettings() -> Int
    {
       return UserDefaults.standard.integer(forKey: "port_settings")
    }
    
    func setPortSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "port_settings")
    }
    
    
    // Sensor data 
    func getSensorDataSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "sensor_data") ?? PasasvoConfig.intervalSensor
        
    }
    
    func setSensorDataSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "sensor_data")
    }
    
    // Dangerous Data Time
    func getDangerousDataSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "speed_data_time") ?? PasasvoConfig.intervalServerPolling
        
    }
    
    func setDangerousDataSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "speed_data_time")
    }
    
    // Speed Data Time
    func getSpeedDataSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "dangerous_data") ?? PasasvoConfig.intervalCalcSpeed
        
    }
    
    func setSpeedDataSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "dangerous_data")
    }
    
    // Driving Data Time
    func getDrivingDataSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "driving_data_time") ?? PasasvoConfig.intervalDrivingData
        
    }
    
    func setDrivingDataSettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "driving_data_time")
    }
    
    // Gyro settings
    func getGyroEnableSettings() -> Bool
    {

        return UserDefaults.standard.bool(forKey: "gyro_enable") ?? true
    }
    
    func setGyroEnableSettings(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "gyro_enable")
    }
    
    // Gyro accerator settings
    func getAccelerationEnableSettings() -> Bool
    {
        return UserDefaults.standard.bool(forKey: "acceleration_enable") ?? true
    }
    
    func setAccelerationEnableSettings(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "acceleration_enable")
    }
    
    
    // Gyro accerator settings
    func getGpsEnableSettings() -> Bool
    {
        return UserDefaults.standard.bool(forKey: "gps_enable") ?? true
    }
    
    func setGpsEnableSettings(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "gps_enable")
    }
    

    
    // Upload interval data
    func getUploadIntervalSettings() -> Float
    {

        return UserDefaults.standard.float(forKey: "upload_interval_settings") ?? PasasvoConfig.intervalDataUpload
    }
    
    func setUploadIntervalSettings(value: Float)
    {
        UserDefaults.standard.set(value, forKey: "upload_interval_settings")
    }
    
    
    // Real time display
    func getRealtimeLogSettings() -> Bool
    {
        return UserDefaults.standard.bool(forKey: "real_time_log_display_settings")
    }
    
    func setRealtimeLogSettings(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "real_time_log_display_settings")
    }
    
    // Real obd2 display
    func getOBD2Settings() -> String
    {
        return UserDefaults.standard.string(forKey: "obd2_settings") ?? CanOBD2Mode.getCanModeByCode(code: PasasvoConfig.obdOption)!
    }
    
    func setOBD2Settings(value: String)
    {
        UserDefaults.standard.set(value, forKey: "obd2_settings")
    }
    
    // Security Settings display
    func getSecurityEnableSettings() -> Bool
    {
        return UserDefaults.standard.bool(forKey: "security_enable_settings")
    }
    
    func setSecurityEnableSettings(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "security_enable_settings")
    }
    
    
    // Real Smartphone memory display
    func getSmartphoneMemorySettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "smartphone_memory_settings") ?? 0
    }
    
    func setSmartphoneMemorySettings(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "smartphone_memory_settings")
    }
    
    // Real Max Time Driving
    func getMaxDrivingTime() -> Int
    {
        return UserDefaults.standard.integer(forKey: "Max time") ?? 0
    }
    
    func setMaxDrivingTime(value: Int)
    {
        UserDefaults.standard.set(value, forKey: "Max time")
    }
    
    //Real Memo
    func getMemo() -> String
    {
        return UserDefaults.standard.string(forKey: "Memo") ?? PasasvoConfig.memo
    }
    
    func setMemo(value: String)
    {
        UserDefaults.standard.set(value, forKey: "Memo")
    }
    
    //Real CarId
    func getCarId() -> String
    {
        return UserDefaults.standard.string(forKey: "CarId") ?? PasasvoConfig.carId
    }
    
    func setCarId(value: String?)
    {
        UserDefaults.standard.set(value, forKey: "CarId")
    }
    
    //Real UserId
    func getUserId() -> String
    {
        return UserDefaults.standard.string(forKey: "UserId") ?? PasasvoConfig.userId
    }
    
    func setUserId(value: String?)
    {
        UserDefaults.standard.set(value, forKey: "UserId")
    }
    
    //Real DeviceId
    func getDeviceId() -> String
    {
        return UserDefaults.standard.string(forKey: "DeviceId") ?? PasasvoConfig.deviceId
    }
    
    func setDeviceId(value: String?)
    {
        UserDefaults.standard.set(value, forKey: "DeviceId")
    }
    
    //Real DrivingId
    func getDrivingId() -> String
    {
        return UserDefaults.standard.string(forKey: "DrivingId") ?? PasasvoConfig.drivingId
    }
    
    func setDrivingId(value: String?)
    {
        UserDefaults.standard.set(value, forKey: "DrivingId")
    }
    
    //Real LoginId
    func getLoginId() -> String
    {
        return UserDefaults.standard.string(forKey: "LoginId") ?? " "//PasasvoConfig.drivingId
    }
    
    func setLoginId(value: String?)
    {
        UserDefaults.standard.set(value, forKey: "LoginId")
    }
    
    //Real Password
    func getPassword() -> String
    {
        return UserDefaults.standard.string(forKey: "Password") ?? " "//PasasvoConfig.drivingId
    }
    
    func setPassword(value: String?)
    {
        UserDefaults.standard.set(value, forKey: "Password")
    }
    
    // Real time display
    func getTestModeSettings() -> Bool
    {
        return UserDefaults.standard.bool(forKey: "test_mode_settings") ?? PasasvoConfig.testMode
    }
    
    func setTestModeSettings(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "test_mode_settings")
    }
    
    // Real Flag
    func getFlag() -> Bool
    {
        return UserDefaults.standard.bool(forKey: "Flag")
    }
    
    func setFlag(value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "Flag")
    }
    
    //Current User
    func setCurrentUser(value: String)
    {
        UserDefaults.standard.set(value, forKey: "UserID")
    }
    func getCurrentUser() -> String
    {
        return UserDefaults.standard.string(forKey: "UserID") ?? PasasvoConfig.userId
    }
    
    //Current User Name
    func setCurrentUserName(value: String)
    {
        UserDefaults.standard.set(value, forKey: "UserName")
    }
    func getCurrentUserName() -> String
    {
        return UserDefaults.standard.string(forKey: "UserName") ?? ""
    }
    
    //Current CarID
    func setCurrentCar(value: String)
    {
        UserDefaults.standard.set(value, forKey: "CarID")
    }
    func getCurrentCar() -> String
    {
        return UserDefaults.standard.string(forKey: "CarID") ?? PasasvoConfig.carId
    }
    
    //Current DeviceID
    
    func setCurrentDevice(value: String)
    {
        UserDefaults.standard.set(value, forKey: "DeviceID")
    }
    func getCurrentDevice() -> String
    {
        return UserDefaults.standard.string(forKey: "DeviceID") ?? PasasvoConfig.deviceId
    }
    //
    func setServerUrl(value: String)
    {
        UserDefaults.standard.set(value, forKey: "ServerUrl")
    }
    func getServerUrl() -> String
    {
        return UserDefaults.standard.string(forKey: "ServerUrl") ?? PasasvoConfig.serverUrl
    }
    
    //
    func setDrivingDiagnosisValue(value: String)
    {
        UserDefaults.standard.set(value, forKey: "DrivingDiagnosisValue")
    }
    func getDrivingDiagnosisValue() -> String
    {
        return UserDefaults.standard.string(forKey: "DrivingDiagnosisValue") ?? ""
    }
    
    //
    func setDateTime(value: String)
    {
        UserDefaults.standard.set(value, forKey: "DateTime")
    }
    func getDateTime() -> String
    {
        return UserDefaults.standard.string(forKey: "DateTime") ?? ""
    }
    
    //
    func setTotalMilesValue(value: String)
    {
        UserDefaults.standard.set(value, forKey: "TotalMilesValue")
    }
    func getTotalMilesValue() -> String
    {
        return UserDefaults.standard.string(forKey: "TotalMilesValue") ?? ""
    }
    
    //
    func setMileageEarnedValue(value: String)
    {
        UserDefaults.standard.set(value, forKey: "MileageEarnedValue")
    }
    func getMileageEarnedValue() -> String
    {
        return UserDefaults.standard.string(forKey: "MileageEarnedValue") ?? ""
    }
    
    //
    func setAverageSpeedValue(value: String)
    {
        UserDefaults.standard.set(value, forKey: "AverageSpeedValue")
    }
    func getAverageSpeedValue() -> String
    {
        return UserDefaults.standard.string(forKey: "AverageSpeedValue") ?? ""
    }
}



extension UIColor
{
    

}

class LabelWithAdaptiveTextHeight: UILabel {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        font = fontToFitHeight()
    }
    
    // Returns an UIFont that fits the new label's height.
    private func fontToFitHeight() -> UIFont {
        
        var minFontSize: CGFloat = 20
        var maxFontSize: CGFloat = 250
        var fontSizeAverage: CGFloat = 0
        var textAndLabelHeightDiff: CGFloat = 0
        
        while (minFontSize <= maxFontSize) {
            fontSizeAverage = minFontSize + (maxFontSize - minFontSize) / 2
            
            if let labelText: String = text {
                let labelHeight = frame.size.height
                
                let testStringHeight = labelText.size(attributes: [NSFontAttributeName: font.withSize(fontSizeAverage)]).height
                
                textAndLabelHeightDiff = labelHeight - testStringHeight
                
                if (fontSizeAverage == minFontSize || fontSizeAverage == maxFontSize) {
                    if (textAndLabelHeightDiff < 0) {
                        return font.withSize(fontSizeAverage - 1)
                    }
                    return font.withSize(fontSizeAverage)
                }
                
                if (textAndLabelHeightDiff < 0) {
                    maxFontSize = fontSizeAverage - 1
                    
                } else if (textAndLabelHeightDiff > 0) {
                    minFontSize = fontSizeAverage + 1
                    
                } else {
                    return font.withSize(fontSizeAverage)
                }
            }
        }
        return font.withSize(fontSizeAverage)
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
}




public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
