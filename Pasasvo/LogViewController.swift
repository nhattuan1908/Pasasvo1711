//
//  LogViewController.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/12/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit
import PasasvoSDK

class LogViewController: UIViewController {
    
    @IBAction func unwindToLogView(segue:UIStoryboardSegue) { }
    
    @IBOutlet weak var tableView: UITableView!
    var selectedLog: DetailLogs?
    
    var logDatas = [DetailLogs]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets.zero
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        logDatas = Helper.getLogsDatas()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension LogViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogViewTableViewCell") as! LogViewTableViewCell
            cell.dangerousTitle.text = AppConfig.DangerousType[Int(logDatas[indexPath.row].dangerousType)]
            cell.datetime.text = logDatas[indexPath.row].measureTime
            let triggerType = logDatas[indexPath.row].dangerousTriggerType
            cell.changeDangerousTypeBtn.layer.cornerRadius = CGFloat(5)
            cell.changeDangerousTypeBtn.layer.borderColor = UIColor.black.cgColor
            cell.changeDangerousTypeBtn.layer.borderWidth = CGFloat(1)
            if (triggerType == Int32(DangerousTrigger.FromServer))
            {
                cell.changeDangerousTypeBtn.setBackgroundColor(color: UIColor.red, forState: .normal)
                cell.changeDangerousTypeBtn.setBackgroundColor(color: UIColor.black, forState: .highlighted)
                cell.changeDangerousTypeBtn.setTitleColor(UIColor.white, for: .normal)
                cell.changeDangerousTypeBtn.setTitle(NSLocalizedString("event", comment: ""), for: .normal)
                
            }
            else
            {
                cell.changeDangerousTypeBtn.setBackgroundColor(color: ConstantColors.UserEventBackground, forState: .normal)
                cell.changeDangerousTypeBtn.setBackgroundColor(color: ConstantColors.UserEventBackgroundSelected, forState: .highlighted)
                cell.changeDangerousTypeBtn.setTitleColor(UIColor.black, for: .normal)
                cell.changeDangerousTypeBtn.setTitle(NSLocalizedString("checkResult", comment: ""), for: .normal)
                cell.changeDangerousTypeBtn.titleLabel?.font = UIFont(name: "Avenir-Black", size: 10)
            }
            
            
            cell.changeDangerousTypeBtn.tag = indexPath.row
            
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.editDangerousLog(_:)))
            cell.changeDangerousTypeBtn.addGestureRecognizer(recognizer)
            //return cell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.logDatas.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? LogConfirmViewController
        {
            controller.selectedLog = self.selectedLog
        }
    }
    
    func editDangerousLog(_ sender: UITapGestureRecognizer) {
        let button = sender.view as! UIButton
        selectedLog = logDatas[button.tag]
        self.performSegue(withIdentifier: "show_confirm_dangerous_event", sender: self)
    }
    
}
