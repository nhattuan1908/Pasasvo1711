//
//  LogViewTableViewCell.swift
//  SensorsLog
//
//  Created by Tran Anh on 2017/12/03.
//  Copyright © 2017 unitec. All rights reserved.
//

import UIKit

class LogViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var datetime: UILabel!
    @IBOutlet weak var dangerousTitle: UILabel!
    @IBOutlet weak var changeDangerousTypeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
